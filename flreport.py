import arrayfire as af
import numpy as np
from AF_Analysis import Histogram as hst
from AF_Analysis import afRANSAC as afr
from AF_Analysis import afFFFL as af3fl
from converter import unitConvert
from matplotlib import collections  as mc
from matplotlib.backends.backend_pdf import PdfPages
from scipy.ndimage import filters
import file_read
import json
import matplotlib.pyplot as plt
import png
import time
import datetime
import math
import sys
import io

if __name__ == "__main__":

    def escape(string):
        return string.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace("'", '&#39;').replace('"', '&quot;').replace(':', '*').replace(' ', '_')
    
    now = datetime.datetime.now()
    safeDate = "FFFL_UTC" + escape(now.strftime("%Y-%m-%d %H:%M:%S"))


    parsedInfo = json.loads(sys.argv[1])
    
    googleID = parsedInfo["googleID"]
    resultID = parsedInfo["resultID"]
    bounds = parsedInfo["bounds"]
    sections = parsedInfo["region"]
    int_args = ["minff", "minfl", "specff", "specfl", "spacing"]
    user = parsedInfo["user"]
    design = {}
    errThr = parsedInfo["errThr"]
    plnThr = parsedInfo["plnThr"]
    plnHt = parsedInfo["plnHt"]
    if "design" in parsedInfo and parsedInfo["design"] != "none":
        sys.stdout.flush()
        design = file_read.getCADJSON(parsedInfo["design"], googleID)
    sys.stdout.flush()

    img = file_read.loadNPYData(resultID + "_npimg", googleID)

    report = af3fl.flreport(
            plnHt, plnThr, errThr, sections, img, bounds, bounds["units"],
            design=design, filename=parsedInfo["filename"], user=user,
            **{k:int(v) for k,v in parsedInfo.items() if k in int_args and v != ''})

    safeName = safeDate + "_" + parsedInfo["filename"]
    local_fname = "../uploaded_files/" + safeName + ".pdf"
    with PdfPages(local_fname) as pdf:
        report.plotSections(pdf)

    pdflink = file_read.upload_pdf(local_fname, googleID, safeName)

    print(pdflink)
    sys.stdout.flush()
