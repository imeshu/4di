# afRANSAC_test.py
# Created by Mica Lewis
# 4Di

import numpy as np
import arrayfire as af
import time
from AF_Analysis import afRANSAC as afr
import matplotlib.pyplot as plt
from AF_Analysis import afFFFL
from AF_Analysis import Histogram as hst
from AF_Analysis import afDebug as dbg

data = np.array([[1, 1, 0], [0, 1, 0], [1, 0, 0], [0, 0, 1]],
                order='F',
                dtype='f')

print("Max points for af_get_inliers: " + str(dbg.max_points(lambda a: afr.af_get_inliers(a, af.Array([0,0,1,0]), .5))))
print("Max points for af_avg_grid: " + str(dbg.max_points(lambda a: hst.af_avg_grid(a, 100, 100, 0, 0, 1, 1))))

print("Random dataset:")
print(data)
start = time.time()
result = afr.ground_plane(data, .5, 10)
end = time.time()
print("RANSAC runtime: " + str(end - start) + "s")
print("Result:")


'''
print(str(result.inliers) + " inliners in plane:")
print(result.coeffs)

print("Input organized csv file to load (skips first row)")
#f = input("> ")
start = time.time()
data = np.load("connorerr.npy").astype(np.float32)
end = time.time()
print("Load time: " + str(end - start) + "s")
af.random.set_seed(int(time.time()))
print()

print("First 10 elements of file dataset:")
print(data[:10])
print()

print("Bounds:")
print(hst.get_bounds(data))
print()

print("Single Plane:\n")
start = time.time()
result = afr.ground_plane(data, .04, 100, culling=100)
end = time.time()
print("RANSAC runtime: " + str(end - start) + "s")
print("Result:\n")
print(str(result.inliers) + " inliners in plane")
print(str(abs(result.coeffs[3])) + " meters off the ground")

print("\nFirst 10 elements of file dataset:")
print(result.inlier_data[:10])
np.save("result.npy", result.inlier_data)
# '''
