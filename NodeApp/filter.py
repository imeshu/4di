# Filters output from 4dilogs
# Counts events per user

import re
from datetime import datetime as dt
import sys

exclude_list = ["matt", "Peter", "Mica", "Rich", "Sean"];

def parseDate(dateStr):
    parse = re.split("/", dateStr)
    return dt(int(parse[2]), int(parse[0]), int(parse[1]))

if __name__ == "__main__":
    with open('/home/ubuntu/v3/NodeApp/4dilogs.txt','r') as f:
        
        in_bracket = False
        in_error = False
        errors = {}
        reports = {}
        loggins = {}
        uploads = {}
        pdfs = {}
        fffls = {}

        if len(sys.argv) == 1:
            cutoff = dt(1,1,1)
        else:
            cutoff = parseDate(sys.argv[1])

        for line in f:

            if line != "\n" or in_error:

                if "During handling of the above exception" in line or "Traceback (most recent call last)" in line or "OSError" in line:
                    in_error = True
                
                if not in_bracket and not in_error:
                    user = re.split("\t", line)[1]
                    date = parseDate(re.split("\t", line)[0][:line.index(',')])
                    included = not any(name == user for name in exclude_list) and date>=cutoff
                    if not user in errors and included:
                        errors[user] = 0
                        reports[user] = 0
                        loggins[user] = 0
                        uploads[user] = 0
                        pdfs[user] = 0
                        fffls[user] = 0

                if "}" in line and not in_error:
                    in_bracket = False


                if included:
                    print(line, end = "")
                    if "DONE," in line:
                        reports[user] += 1
                    if "loaded homepage" in line:
                        loggins[user] += 1
                    if "sent pdf:" in line:
                        pdfs[user] += 1
                    if "sent files from box" in line or "uploaded file" in line:
                        uploads[user] += 1
                    if "fffl complete" in line:
                        fffls[user] += 1

                if "{" in line and not "}" in line and not in_error:
                    in_bracket = True
                if "generate request failed" in line:
                    if included:
                        errors[user] += 1
                    in_error = True
                if line == "\n":
                    in_error = False



        print("\n\n")
        print("==========  STATS ==========")
        print("Errors:\t\t\t\t" + str(errors))
        print("Completed reports:\t\t" + str(reports))
        print("Log ins or page refreshes):\t" + str(loggins))
        print("Sent or saved pdfs:\t\t" + str(pdfs))
        print("Uploads:\t\t\t" + str(uploads))
        print("FF/FL reports:\t\t\t" + str(fffls))

