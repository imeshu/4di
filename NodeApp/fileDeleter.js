const fs = require('fs');
const path = require('path');


module.exports={

 //Deletes the specified file in the specified folder
singleDel: function(directory, file){
    fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
        console.log("Deleted " + file + " from " + directory);
    });
},


 //Deletes all files in a given folder
folderEmpty: function(directory){
    fs.readdir(directory, (err, files) => {
        if (err) throw err;

         for (const file of files) {
            fs.unlink(path.join(directory, file), err => {
            if (err) throw err;

             console.log("Deleted " + file + " from " + directory);
            });
        };
    });
}


};
