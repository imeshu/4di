var fs = require("fs");
var file_url = "";
var downloadDir = "../uploaded_files/";
const url = require("url");
var http = require("follow-redirects").http;
var https = require("follow-redirects").https;
var exec = require("child_process").exec;
var progress = require("progress-stream");
var log = require("single-line-log").stdout;
var numeral = require("numeral");
var crypto = require("crypto");
var AWS = require("aws-sdk");
var s3 = new AWS.S3();
var path = require("path");
var boxDownloadBuffer = {};
const { spawn } = require('child_process');
var fileDeleter = require("./fileDeleter.js")

module.exports = {


fileUploader: function(files, googleid, token){

    //var linkArray = req.body.strArr.split(",");
    //console.log(linkArray);

    var totalFileSize = 0;
    var downloadID = Date.now();
    var totalProg = 0;
    var lastProg = 0;
	
    boxDownloadBuffer[downloadID] = [];
    // calculates the total size of all files sent
    for (var i = 0; i < files.length; i++) {
        //var subarr = linkArray[i].split("--");
        totalFileSize += files[i].bytes;
    }
    console.log("Total file size:" + totalFileSize);

    // downloads a file for each link sent
    files.forEach(function(element) {
        //var subarray = element.split("--");
        var fileName = element.name;
        file_url = element.link;

        var str = progress({
            drain: true,
            speed: 20
        });

        str.on("progress", function(progress) {
            totalProg = totalProg + progress.delta;
            var prog = Math.round((totalProg / totalFileSize) * 100) + "%";
            boxDownloadBuffer[downloadID].push(prog);
        });

	// retrieves links from box
	var download_file_httpget = function(file_url) {
            if(token != 0){
                var options = {
                    host: url.parse(file_url).host,
                    port: 443,
                    path: url.parse(file_url).pathname,
                    headers: { Authorization: "Bearer " + token }
                };
            }else{
                var options = {
                    host: url.parse(file_url).host,
                    port: 443,
                    path: url.parse(file_url).pathname
                };
            }
            var file = fs.createWriteStream(downloadDir + fileName, { flags: "w" });
            file.on("error", function(e) {
                console.error(e);
            });

            https.get(options, function(response) {
                response.pipe(str);
                response
                    .on("data", function(data) {
                        file.write(data);
                    })
                    .on("end", function() {
                        file.end();
                        console.log(fileName + " downloaded to " + downloadDir);
                        response = uploads3(fileName, googleid);
                        console.log(response);
                    })
                    .on("error", function() {
                        console.log("error!");
                    });
            });
        };

        download_file_httpget(file_url);
    });
    
},
	
	
// retrives a files from box given a list of links
boxUploader: function(req, res, token, googleid){

    var linkArray = req.body.strArr.split(",");
    console.log(linkArray);

    var totalFileSize = 0;
    var downloadID = Date.now();
    var totalProg = 0;
    var lastProg = 0;
    res.send("" + downloadID);
    boxDownloadBuffer[downloadID] = [];
    // calculates the total size of all files sent
    for (var i = 0; i < linkArray.length; i++) {
        var subarr = linkArray[i].split("--");
        totalFileSize = parseInt(totalFileSize, 10) + parseInt(subarr[2], 10);
    }
    console.log("Total file size:" + totalFileSize);

    // downloads a a file for each link sent
    linkArray.forEach(function(element) {
        var subarray = element.split("--");
        var fileName = subarray[0];
        file_url = subarray[1];

        var str = progress({
            drain: true,
            speed: 20
        });

        str.on("progress", function(progress) {
            totalProg = totalProg + progress.delta;
            var prog = Math.round((totalProg / totalFileSize) * 100) + "%";
            boxDownloadBuffer[downloadID].push(prog);
        });

	// retrieves links from box
	var download_file_httpget = function(file_url) {
            var options = {
                host: url.parse(file_url).host,
                port: 443,
                path: url.parse(file_url).pathname,
                headers: { Authorization: "Bearer " + token }
            };

            var file = fs.createWriteStream(downloadDir + fileName, { flags: "w" });
            file.on("error", function(e) {
                console.error(e);
            });

            https.get(options, function(response) {
                response.pipe(str);
                response
                    .on("data", function(data) {
                        file.write(data);
                    })
                    .on("end", function() {
                        file.end();
                        console.log(fileName + " downloaded to " + downloadDir);
                        response = uploads3(fileName, googleid);
                        console.log(response);
                    })
                    .on("error", function() {
                        console.log("error!");
                    });
            });
        };

        download_file_httpget(file_url);
    });
    
},
boxDownloadBuffer: boxDownloadBuffer,
uploadS3: function(filename, id){
    uploads3(filename, id);
}

}

// Manages uploads to S3 for point cloud and design files
var uploads3 = function(filename, id) {
    console.log("Uploading to S3...");
    var filepath = "../uploaded_files/" + filename;
    oldfilename = filename
    uploadext = filename.slice(-3);
    // Case where file uploaded is zipped/rard
    if (uploadext == "rar" || uploadext == "zip") {
        newfilename = ""
        console.log("RAR uploaded!");
	// Launches unp - special handling is in place to make sure we properly extract the filenames
	// of the files contained in the rar/zip
        var rarextract = spawn("unp", [filename], { cwd: '/home/ubuntu/v3/uploaded_files/' });
        rarextract.stdout.on("data", function(data) {
            textchunk = data.toString('utf-8');
            if (textchunk.includes('Extracting  ')) {
                console.log(textchunk);
                tempind = textchunk.search('Extracting  ') + 12;
                newfilename = textchunk.slice(tempind).trim();
                console.log(newfilename);
                if (newfilename.includes('%')) {
                    tempind2 = newfilename.search('%');
                    newfilename = newfilename.slice(0, tempind2 - 7).trim();
                    console.log(newfilename + "<- newfilename");
                }
            }
        });
	// After finishing extraction, run as normal for the unzipped file
        rarextract.on('close', function(code) {
            //fileDeleter.singleDel(downloadDir, oldfilename);
            console.log(code);
            filepath = '../uploaded_files/' + newfilename;
            uploadext = filepath.slice(-3);
            filename = newfilename;
            if (uploadext == "e57" || uploadext == "pts" || uploadext == "las" || uploadext == "xyz") {
                uploadext = "pcl";
            }

            console.log("Upload ext: " + uploadext);
            console.log("Filepath: " + filepath);
	    // Spawn file_read process to handle the unzipped file
            var extract = spawn("python3", ["../file_read.py", filename, id]);
            extract.stdout.on("data", function(data) {
            var textChunk = data.toString('utf8');
            console.log(textChunk);
            if (textChunk.includes('Done')) {
                console.log("Id: "+id);
		// Once done transforming file to NPY, upload it
                var params = {
                    Bucket: "4diuploads",
                    Body: fs.createReadStream(filepath + ".npy"),
                    Key: id + "/" + uploadext + "/" + path.basename(filepath) + ".npy"
                };
                s3.upload(params, function(err, data) {
		    // After upload, remove files from directory
                    //handle error
                    if (err) {
                        console.log("Error", err);
                        fileDeleter.singleDel(downloadDir, filename);
                        return err;
                    }

                    //success
                    if (data) {
                        console.log("Uploaded in:", data.Location);
                        fileDeleter.singleDel(downloadDir, filename);
                        fileDeleter.singleDel(downloadDir, filename + ".npy");
                        return data;
                    }
                });
            }
            });
            extract.stderr.on("data", function(data) {
                console.log(data.toString('utf8'));
                fileDeleter.singleDel(downloadDir, filename);
            });

        });
    // Uploading dxf case
    }else if(uploadext == "dxf"){
	// Transofrm data to json, and upload (all done in python process)
        var extract = spawn("python3", ["../file_read.py", filename, id]); 
        extract.stdout.on("data", function(data) {
            var textChunk = data.toString('utf8');
            console.log(textChunk); 
            if (textChunk.includes('Done')) {
                fileDeleter.singleDel(downloadDir, filename);
                fileDeleter.singleDel(downloadDir, filename + ".json");
            }
        });
        
        extract.stderr.on("data", function(data) {
            var error = data.toString("utf8");
            console.log("ERR," + error);
        });   
    // Uploading point cloud case
    }else {
    // Transform data to NPY
    var extract = spawn("python3", ["../file_read.py", filename, id]);
    extract.stdout.on("data", function(data) {
        var textChunk = data.toString('utf8');
        console.log(textChunk);
        if (uploadext == "e57" || uploadext == "pts" || uploadext == "las" || uploadext == "xyz") {
            uploadext = "pcl";
        }
        if (textChunk.includes('Done')) {
            console.log("Id: "+id);
            var params = {
                Bucket: "4diuploads",
                Body: fs.createReadStream(filepath + ".npy"),
                Key: id + "/" + uploadext + "/" +  path.basename(filepath) + ".npy"
            };
            s3.upload(params, function(err, data) {
                //handle error
                if (err) {
                    console.log("Error", err);
                    fileDeleter.singleDel(downloadDir, filename);
                    fileDeleter.singleDel(downloadDir, filename + ".npy");
                    return err;
                }

                //success
                if (data) {
                    console.log("Uploaded in:", data.Location);
                    fileDeleter.singleDel(downloadDir, filename);
                    fileDeleter.singleDel(downloadDir, filename + ".npy");
                    return data;
                }
            });
        }
    });
    extract.stderr.on("data", function(data) {
        console.log(data.toString('utf8'));
        fileDeleter.singleDel(downloadDir, filename);
    });
    }
}    
