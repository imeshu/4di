const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();
const bodyParser = require("body-parser");
const spawn = require("child_process").spawn;
const passport = require("passport");
var Strategy = require("passport-google-oauth20").Strategy;
var cookieParser = require("cookie-parser");
var request = require('request');
var AWS = require("aws-sdk");
var uuid = require("uuid");
var fs = require("fs");
var path = require("path");
var crypto = require("crypto");
var s3 = new AWS.S3();
var mysql = require("mysql");
const boxSDK = require("box-node-sdk");
const appConfig = require("../config.js"); // Auth keys and redirect
const querystring = require("querystring"); // Querystring stringifier
var BoxStrategy = require("passport-box").Strategy;
var util = require("util");
var fs = require("fs");
const url = require("url");
var http = require("follow-redirects").http;
var https = require("follow-redirects").https;
var exec = require("child_process").exec;
var progress = require("progress-stream");
var log = require("single-line-log").stdout;
var numeral = require("numeral");
//var file_url = "";
var downloadDir = "../uploaded_files/";
var uploader = require("./uploadManager.js");
const session = require('express-session');
const FileStore = require('session-file-store')(session);
var fileDeleter = require("./fileDeleter.js")
var jsforce = require('jsforce');

if (appConfig.localEnv) {
	app.use(express.static('../static_files/app'));
}

// session settings
app.use(session({
    genid: (req) => {
        console.log('Inside the session middleware')
        console.log(req.sessionID)
        return uuid() // use UUIDs for session IDs
    },
    store: new FileStore(),
    cookie: { maxAge : 3600000 * 24 * 14 }, // 2 weeks
    secret: 'lovelylaszlo',
    resave: true,
    saveUninitialized: true
}));

app.use(cookieParser());

// for files contained within post request
app.use(
    bodyParser.urlencoded({
        limit: '50mb',
        extended: true
    })
);

app.use(bodyParser.json({limit: '50mb', extended: true}));

// Sends information into log file with associated user and timestamp
function log4di (action, req) {
	connection.query(
		'SELECT name FROM users WHERE googleid = ?',
		[req.session.passport.user],
		function(err1, res1, fields) {
			line = 
				(new Date()).toLocaleString() + // in UTC time
				"\t" + res1[0]["name"] + "\t" + action + "\n"
		        fs.appendFile("4dilogs.txt", line, function(err){
                		if (err) throw err;
        		});

		}
	)
}

app.get("/auth/box/callback", function(req, res) {
    // Create a new Box SDK instance
    console.log("ClientID:"+appConfig.oauthClientId);
    console.log("ClientSecret:"+appConfig.oauthClientSecret);
    const sdk = new boxSDK({
        clientID: appConfig.oauthClientId,
        clientSecret: appConfig.oauthClientSecret
    });
    console.log("Get Successful");
    // Extract auth code
    const code = req.query.code;

    console.log(code);

    // Exchange code for access token
    sdk.getTokensAuthorizationCodeGrant(code, null, function(err, tokenInfo) {
        console.log("Error:"+err);
	const client = sdk.getPersistentClient(tokenInfo);
        // PERFORM API ACTIONS WITH CLIENT
        //Extract access token from client object
        var clientStr = util.inspect(client);
        var tokenStart = clientStr.search("accessToken:") + 14;
        var clientToken = clientStr.slice(tokenStart, tokenStart + 32);
        var cookies = cookieParser.JSONCookies(req.cookies);
        var refreshStart = clientStr.search("refreshToken:") + 15;
        var refreshToken = clientStr.slice(refreshStart, refreshStart + 64);
        
	connection.query('UPDATE users SET boxtoken = ?, boxrefresh = ? WHERE googleid = ?;', [clientToken, refreshToken, req.session.passport.user], function (err1, res1, fields) {
            if (err1) throw err1;
            res.redirect('/');
        });
    });
});

//Sends access token to client as a cookie
app.get("/token", function(req, res) {
    
    var refreshTok = 0
    connection.query('SELECT `boxrefresh` FROM `users` WHERE `googleid` = ?', [req.session.passport.user], function (error, results, fields) {
        if (error) throw error;
        refreshTok = results[0];
    });

    function updater(access, reference){
        connection.query('UPDATE `users` SET `boxtoken` = ?, `boxrefresh` = ? WHERE `googleid` = ?', [access,reference,req.session.passport.user], function (error, results, fields) {
            if (error) throw error;
            console.log(results);
        });
    }

    var dataString = 'grant_type=refresh_token&refresh_token=' + refreshTok + '&client_id=' + appConfig.oauthClientId + '&client_secret=' + appConfig.oauthClientSecret;

    var options = {
        url: 'https://api.box.com/oauth2/token',
        method: 'POST',
        body: dataString
    };

    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            var tokenjson = JSON.stringify(body, null, 2);
            extractor(tokenjson);
        }else{
            console.log(response.statusCode + ' ' + error)       
        }
    }

    request(options, callback);

    function extractor(json){
        var atStart = json.search("access_token") + 17;
        var accToken = json.slice(atStart, atStart + 32);

        var rtStart = json.search("refresh_token") + 18;
        var refToken = json.slice(rtStart, rtStart + 64);
        console.log(refToken);
        updater(accToken, refToken);
    }
    connection.query('SELECT boxtoken FROM users WHERE googleid = ?', [req.session.passport.user], function (err1, res1, fields) {
        if (err1) throw err1;
        res.send(res1[0].boxtoken);
    })
});

//Downloads file from Box to 4Di
app.post("/download", function(req, res) {

    console.log("Box download user: " + req.session.passport.user);
    connection.query('SELECT boxtoken FROM users WHERE googleid = ?', [req.session.passport.user], function(err1, res1, fields) {
        if (err1) throw err1;
        console.log('Box token: ' + res1[0]);
        uploader.boxUploader(req, res, res1[0].boxtoken, req.session.passport.user);
    	log4di("sent files from box",req)
    });
});

// creates the connection with the rds database
var connection = mysql.createConnection({
  host     : 'database-1.cluster-crjzr9hijgkz.us-east-1.rds.amazonaws.com',
  user     : 'admin',
  password : 'laszlo13',
  database : '4didata'
});

connection.connect(function(err) {
  if (err) {
    return console.error('error: ' + err.message);
  }
 
  console.log('Connected to the SQL server.');
});


// This is our Passport strategy for google OAuth. Here we can view the profile and potentially extract data
passport.use(
    new Strategy(
        {
            clientID: "878697669718-776f6eovk39n826ff7qln90enafkpjs2.apps.googleusercontent.com",
            clientSecret: "ypKnOlqATAIGrBJwVnIQN1kU",
            callbackURL: appConfig.redirectGoogleURI,
            userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo'
        },
        function(accessToken, refreshToken, profile, cb) {
            if (profile) {
		 connection.query('SELECT * FROM users WHERE googleid = ?', [profile.id], function (err1, res1, fields) {
		     if (err1) throw err1;
		     console.log("Select res: " + res1)
		     console.log("Fields: " + fields)
		     // If user already has account
		     if (res1.length < 1) {
			 console.log('Response length too short - creating new entry');
			 connection.query('SELECT * FROM registration WHERE email = ?', [profile._json.email], function(err2, res2, fields2) {
				console.log(res2[0]);
				if (res2.length == 1) {
					connection.query(
						'INSERT INTO users (googleid,name,zuoraid,email) VALUES(?,?,?,?)',
						[profile.id, profile.name.givenName, res2[0]["zuoraid"], profile._json.email],
						function (err3, res3, fields3) {
							if (err3) throw err3;
							console.log(res3);
							return cb(null, { googleid: profile.id, sid: null, boxToken: null, boxRefresh: null, name: profile.name.givenName });
					});
				} else {
					return cb(null, false);
				}
			 });
		     } else {
			 console.log('Found existing entry');
			 console.log(res1[0]);
			 return cb(null, res1[0]);
		     }
		 });
             } else {
                 return cb(null, false);
             }
         }
));

app.post("/createuser", function(req, res){
	email = req.body.email;
	zuoraid = req.body.zuoraid;

	console.log("recieved createuser");
	console.log(req.body);

	
	var options = { method: 'GET',
		url: 'https://rest.zuora.com/v1/accounts/' + zuoraid + '/summary',
  		headers:
   { 'cache-control': 'no-cache',
     'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' }
	}

	if (email && zuoraid) {
		request(options, function(error, response, body){
			body = JSON.parse(body);
			console.log(body.subscriptions);
			if ( body["subscriptions"] && body["subscriptions"].some(function(e){console.log(e["status"]); return e["status"] == "Active"}) ) {
				connection.query('INSERT INTO registration (email,zuoraid) VALUES(?,?)', [email, zuoraid], function (err2, res2, fields2) {
					console.log(err2);
					if (err2) throw err2;
					console.log(res2);
					res.send("success");
				});
			} else {
				console.log("no active subscription found");
				res.send("no active subscription found");
			}
		});
	} else {
		console.log("bad arguments on create user");
		res.send("bad arguments");
	}

})

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  In a
// production-quality application, this would typically be as simple as
// supplying the user ID when serializing, and querying the user record by ID
// from the database when deserializing.  However, due to the fact that this
// example does not have a database, the complete Facebook profile is serialized
// and deserialized.
passport.serializeUser(function(user, cb) {
    cb(null, user.googleid);
});

passport.deserializeUser(function(obj, cb) {
    connection.query('SELECT * FROM users WHERE googleid = ?', [obj], function(err, res, fields) {
        if (err) throw err;
        if (res.length > 0) {
            //console.log("Deserializing - here is user");
            //console.log(res[0])
            cb(null, res[0]);
        } else {
            cb(null, false);
        }
    });
});

// Allows Node to parse request bodies
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

// File uploader - used for saving files to server
app.use(
    fileUpload({
        limits: { fileSize: 50 * 1024 * 1024 * 1024 },
        useTempFiles: true,
        tempFileDir: "../uploaded_files/",
        preservePath: true
    })
);

// sets exposed headers
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization, cache-control"
    );
    next();
});

// Used to maintain session persistence and authentication
app.use(require("cookie-parser")());
app.use(require("express-session")({ secret: "best dog laz", resave: true, saveUninitialized: true }));

// Init Passport
app.use(passport.initialize());
app.use(passport.session());

var consoleBuffer = {};
// This is the endpoint that must be hit by a user trying to login
app.get("/auth/google", passport.authenticate("google", { scope: ["profile", "email"] }));

// Callback endpoint after logging in to google
app.get("/auth/google/callback", 
	passport.authenticate("google", { successRedirect: "/", failureRedirect: "/login", failureFlash: true }),
);

// Uploads files from client to 4Di
app.post("/node/upload", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    console.log("Uploaded files:");
    console.log(req.files.file.name);
    log4di("uploaded file: " + req.files.file.name, req);

    let datafile = req.files.file;
    console.log(req.files.file)

    datafile.mv("../uploaded_files/" + req.files.file.name, function(err) {
	    console.log("renaming file")
        if (err) {
            console.log(err);
            res.send("failure");
            return res.status(500).send(err);
        } else {
		console.log(req.files.file)
	    res.send("success");
	}
    });
});


// Allows client side access to unit conversions
app.post("/node/convert", function(req, res) {
    var vals = req.body.valArr.split('--');
    var converter = spawn("python", [
        "../converter.py",
        parseFloat(vals[0]),
        vals[1],
        vals[2],
        vals[3],
        vals[4]
      ]);
    
    converter.stdout.on("data", function(data){
        console.log(data.toString("utf8"));
        res.send(data.toString());
    });
    
    converter.stderr.on("data", function(data) {
        var error = data.toString("utf8");
        console.log("ERR," + error);
	res.send("ERR," + error)
    });   
});


// Run Python generation script (plotCloud.py)
app.post("/node/generate", function(req, res) {
    console.log("recieved generate request");

    log4di("began report on " + JSON.stringify(req.body, null, 4), req)

    var d = new Date();
    var resultID = Date.now();

    res.send("" + resultID);

    consoleBuffer[resultID] = [];

    var bodyjson = req.body;
    bodyjson.resultID = resultID;
    bodyjson.googleID = req.user.googleid;
    console.log(bodyjson);

    // send form data JSON in 
    var generator = spawn("python3", [
        "../plotCloud.py",
        JSON.stringify(bodyjson)
    ]);

    generator.stdout.on("data", function(data) {
        var textChunk = data.toString("utf8");
        console.log(textChunk);

        consoleBuffer[resultID].push(textChunk);
	log4di(textChunk, req)
    });

    generator.stderr.on("data", function(data) {
        var error = data.toString("utf8");

        if (error.includes("GLFW") || error.toLowerCase().includes("warning")) {
            console.log("Ignore for now: " + error);
        } else {
            console.log("ERR," + error);
            consoleBuffer[resultID].push("ERR," + error);
	    log4di("generate request failed: " + error, req)
        }
    });
});

// Progress poller - keeps request alive while uploading
app.get("/node/pollprogress/*", function(req, res) {
    var resultID = req._parsedUrl.query;
    var result = consoleBuffer[resultID].pop();
    res.send(result);
});

// Progress poller - keeps request alive while uploading
app.get("/node/pollboxprogress/*", function(req, res) {
    var resultID = req._parsedUrl.query;
    var result = uploader.boxDownloadBuffer[resultID].pop();
    res.send(result);
});

// Redirect endpoint used to private host output files on the server
app.get("/out/*", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    filename = req.path.substring(5);
    console.log(filename);
    res.set("X-Accel-Redirect", "/app/outputs/" + filename);
    res.send();
});

//Sends the requested values from the config file to the client
app.get("/getboxlink", function(req, res) {
    var configVals = { "client" : [], "upload" : [] }
    configVals["client"].push(appConfig.oauthClientId);
    configVals["upload"].push(appConfig.uploadURI);
    res.send(configVals);
});

// retrieve the names of all files (pointclouds and design files) on S3 for a given user
app.get("/lists3", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    var params = {
        Bucket: '4diuploads',
        Prefix: req.user.googleid + "/pcl/",
    };
    var filenames = { "pcl" : [], "dxf" : [] }
    s3.listObjects(params, function(err, data) {
        if (err) console.log(err);
        if (data) {
            for (var i = 0; i < data["Contents"].length; i++) {
                filekey = data["Contents"][i]["Key"];
                filenames["pcl"].push(filekey.split("/")[2].slice(0,-4));
            }
        }
    }).on("success", function (res2){
    	params.Prefix = req.user.googleid + "/json/";
	s3.listObjects(params, function(err, data) {
	    if (err) console.log(err);
            if (data) {
                for (var i = 0; i < data["Contents"].length; i++) {
                    filekey = data["Contents"][i]["Key"];
                    filenames["dxf"].push(filekey.split("/")[2].slice(0,-5));
                }
            }
	    res.send(filenames);
	});
    });
});

// list all pdfs saved in a user's account
app.get("/listpdf", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    console.log("listpdf");
    var params = {
        Bucket: '4dipdfs',
        Prefix: req.session.passport.user + "/pdf/",
    };
    var flParams = {
	Bucket: '4dipdfs',
	Prefix: req.session.passport.user + "/flreports",
    };
  // getPdfs(params);
  // getPdfs(flParams)
   //function getPdfs(params){
   
   var filenames = { "pdf" : [], "user" : [] }
      s3.listObjects(flParams, function(err, data) {
        filenames["user"].push(req.session.passport.user);
        if (err) console.log(err);
        if (data) {
            for (var i = 0; i < data["Contents"].length; i++) {
                filekey = data["Contents"][i]["Key"];
                fileext = filekey.slice(-7, -4);
		altext = filekey.slice(-8);
		//console.log("Altext is: " + altext);
        filenames["pdf"].push(filekey.slice(32, -4));
            }         
        //res.send(filenames);
	console.log(filenames.pdf.length);
        }
    });
   //};
      s3.listObjects(params, function(err, data) {
        filenames["user"].push(req.session.passport.user);
        if (err) console.log(err);
        if (data) {
            for (var i = 0; i < data["Contents"].length; i++) {
                filekey = data["Contents"][i]["Key"];
                fileext = filekey.slice(-7, -4);
		altext = filekey.slice(-8);
		//console.log("Altext is: " + altext);
        filenames["pdf"].push(filekey.slice(26, -4));
            }         
        res.send(filenames);
        }
    }); 	
});

// Saves a pdf from the client's browser to s3
app.post("/sendpdf", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
   var str = req.body.pdfStr;
   var fname = req.body.pdfName + '.pdf';
   var pdf = str.split(';base64,').pop();
   const bckt = '4dipdfs';
    fs.writeFile(downloadDir + fname, pdf, {encoding: 'base64'}, function(err) {
    console.log('File created');
   });
   var filepath = downloadDir + fname
    var params = {
     Bucket: bckt,
     Body: fs.createReadStream(filepath),
     Key: req.user.googleid + "/pdf/" +  path.basename(filepath),
     ContentDisposition: 'inline',
     ContentType: 'application/pdf',
     ACL: 'public-read'
    };
    //This solves the 0B problem
    req.pause();
    s3.upload(params, function(err, data) {
    //handle error
        if (err) {
            console.log("Error", err);
            //fileDeleter.singleDel(downloadDir, filename);
	    log4di("PDF failed to send: " + err, req)
            return err;
        }

    //success
        if (data) {
            console.log("Uploaded in:", data.Location);
	    log4di("sent pdf: " + data.Location, req);
            fileDeleter.singleDel(downloadDir, fname);
	    res.send(data.Location);
        }
    });
   
});

//Endpoint for fffl data
app.post("/genfffl", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
   var flData = req.body;
    
   connection.query(
      'SELECT name FROM users WHERE googleid = ?',
      [req.session.passport.user],
      function(err1, res1, fields) {
	  
	   flData["user"] = res1[0]["name"]
	   flData["googleID"] = req.session.passport.user
	   console.log(flData);
	   log4di("recieved fffl request", req);
	   var generator = spawn("python3", [
	       "../flreport.py",
	       JSON.stringify(flData)
	   ]);

	   generator.stdout.on("data", function(data){
		let result = data.toString("utf-8");
		console.log("FFFL report complete");
		console.log(result);
		log4di("fffl complete", req);
		res.send(result);
	   })

	   generator.stderr.on("data", function(data){
		console.log(data.toString("utf-8"));
		log4di("fffl failed", req);
	   })
   });
});

// Endpoint for uploading an existing file to S3
app.get("/uploads3/*", function(req, res) {
    console.log("Made it past auth to uploads3");
    console.log(req.user.googleid);
    console.log(typeof req.user.googleid);
    data = uploader.uploadS3(req.query.filename, req.user.googleid);
    return res.send(data);
});

// Redirect requests for private JS assets to the private app folder for logged in users
app.get("/js/*", require("connect-ensure-login").ensureLoggedIn(), function(req, res) {
    res.set("X-Accel-Redirect", "/app"+req.originalUrl);
    res.send();
});

// Redirect requests for private CSS asseets to the private app folder for logged in users
app.get("/css/*", require("connect-ensure-login").ensureLoggedIn(), function(req, res) {
    res.set("X-Accel-Redirect", "/app"+req.originalUrl);
    res.send();
});

// Redirect requests for private CSS asseets to the private app folder for logged in users
app.get("/images/*", require("connect-ensure-login").ensureLoggedIn(), function(req, res) {
    res.set("X-Accel-Redirect", "/app"+req.originalUrl);
    res.send();
});

// Redirect requests for private HTML assets to the private app folder for logged in users
app.get("/html/*", require("connect-ensure-login").ensureLoggedIn(), function(req, res) {
    res.set("X-Accel-Redirect", "/app"+req.originalUrl);
    res.send();
});

app.get("/canvas", require("connect-ensure-login").ensureLoggedIn(), function(req, res) {
    res.set("X-Accel-Redirect", "/app/canvas/");
    res.send();
});

// Redirect requests for the home page to the private app folder for logged in users
app.get("/", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    res.set("X-Accel-Redirect", "/app/");
    console.log("Home page requested");
    log4di("loaded homepage", req);
    res.send();
});

// retrieves JSON encoded design files from s3
app.get("/canvas/getJSON/*", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    console.log("Session ID: " + req.session.passport.user);
    console.log("Filename: " + req.query.filename);
    var getParams = {
        Bucket: '4diuploads', // your bucket name,
        Key: req.session.passport.user + "/json/" + req.query.filename + ".json"// path to the object you're looking for
    }
    s3.getObject(getParams, function(err, data) {
        if (data) {
            res.send(data.Body);
	}
	if (err) {
	    console.log(err);
	}
    });
});


app.post("/ffflpurch", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
	var link = req.body.link;
	var fnIndex = link.indexOf("FFFL_UTC");
	var fileName = link.substring(fnIndex, link.indexOf("?AWS"));
	var keyIndex = link.indexOf("aws.com") + 8;
	var key = link.substring(keyIndex, link.indexOf("?AWS"));
	console.log(decodeURI(key));

	var params = {
	ACL: 'public-read',
  	CopySource: '4dipdfs' + '/' + key,
  	Bucket: '4dipdfs',
  	Key: req.session.passport.user + "/flreports/" + decodeURI(fileName)
	};

	s3.copyObject(params, function(err, data) {
  	if (err){ console.log(err, err.stack); // an error occurred
		res.status(400).send({message: "FFFL Purchase failed"});
	}else{     console.log("FFFL Report moved sucessfully: " + data);
		res.send();
	}
	});

});


app.post("/node/createzaccount", function(req, res) {
	var accInfo = req.body.accInfo;

var now = new Date();
var currDate = now.toISOString();
var fullDate = currDate.substring(0,10);

var options = { method: 'POST',
  url: 'https://rest.zuora.com/v1/orders',
  //url: 'https://rest.zuora.com/v1/orders',
  headers: 
   { 'cache-control': 'no-cache',
     'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'zuora-version': '223.0',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' },
  body: 
   { newAccount: 
      { billCycleDay: '0',
        autoPay: 'true',
	billToContact: 
         { address1: accInfo.creditCardAddress1,
	   address2: accInfo.creditCardAddress2,
           city: accInfo.creditCardCity,
           country: accInfo.creditCardCountry,
           firstName: accInfo.firstName,
           lastName: accInfo.lastName,
           state: accInfo.creditCardState,
           workEmail: accInfo.email },
        currency: 'USD',
        hpmCreditCardPaymentMethodId: accInfo.refId,
        soldToContact: 
         { address1: accInfo.creditCardAddress1,
	   address2: accInfo.creditCardAddress2,
           city: accInfo.creditCardCity,
           country: accInfo.creditCardCountry,
           firstName: accInfo.firstName,
           lastName: accInfo.lastName,
           state: accInfo.creditCardState, },
	crmId: accInfo.crmId,
        name: accInfo.firstName + ' '+ accInfo.lastName },
     orderDate: fullDate,
     subscriptions: 
      [ { orderActions: 
           [ { createSubscription: 
                { subscribeToRatePlans: [ { productRatePlanId: accInfo.prpid } ],
                  terms: 
                   { initialTerm: 
                      { termType: 'TERMED',
                        period: '12',
                        periodType: 'Month',
                        startDate: fullDate },
                     autoRenew: 'true',
                     renewalSetting: 'RENEW_WITH_SPECIFIC_TERM',
                     renewalTerms: [ { period: '12', periodType: 'Month' } ] } },
               triggerDates: [ { name: 'ContractEffective', triggerDate: fullDate } ],
               type: 'CreateSubscription' } ] } ],
   		processingOptions: {
     	        runBilling: 'true',
        	collectPayment: 'true'
    }
   
   		},
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
  	getZuoraAccId(body.accountNumber, accInfo.reports)
	res.send(body);
  	console.log("Zuora Account submitted");
});
});


//Using the user's googleid, this returns their Zuora account number
function getZuoraAccNum(req, callback) {
	connection.query(
		'SELECT zuoraid FROM users WHERE googleid = ?',
		[req.session.passport.user],
		function(err1, res1, fields) {
			if(err1){
				callback(err, null);
			}else{
			console.log("Zuora Account: " + res1[0]["zuoraid"]);
            		callback(null, res1[0]["zuoraid"])
			//zuoNum = res1[0]["zuoraid"];
			//return zuoNum;
			}
		}
	)
}

//This uses the Zuora account number to get the Zuora account ID
function getZuoraAccId(accNum, repQuant){
  var options = { method: 'GET',
  url: 'https://rest.zuora.com/v1/accounts/' + accNum,
  headers: 
   { 'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'zuora-version': '223.0',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' },
  body: 
   { 
   },
  json: true };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    var zid = body.basicInfo.id;
  	console.log("Zuora ID: " + zid);
    buyRep(zid, repQuant);
  });
}


//This sends the desired number of reports to Zuora to be billed
function buyRep(accountId, quantity){
  var now = new Date();
  var currDate = now.toISOString();
  var options = { method: 'POST',
  url: 'https://rest.zuora.com/v1/object/usage',
  headers: 
   { 'cache-control': 'no-cache',
     'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'zuora-version': '223.0',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' },
  body: 
   { 
    "AccountId": accountId,
    "Description": "test",
    "Quantity": quantity,
    "StartDateTime": currDate,
    "EndDateTime": currDate,
    //"SubscriptionNumber": "A-S00000022",
    "UOM": "Reports"
   },
  json: true };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
  	//res.send(body);
	updateRepDb(accountId, quantity);
  	console.log("Zuora Usage Submitted");
	
  });

}

//updateRepDb('2c92c0fa6fd657a5016fd97d57971a68', 10);
//updateRepDb('2c92c0fb6fc766f3016fcb03fa587b79', 5);
var zuoNum;

function updateRepDb (zid, quantity) {
	//Check if user exists in the table
     connection.query(
		'SELECT COUNT(*) FROM reports WHERE zid = ?',
		[zid],
		function(err1, res1, fields) {
			console.log("Reports: " + res1[0]["COUNT(*)"]);
			
            if(res1[0]["COUNT(*)"]==0){
                connection.query(
                    'INSERT INTO reports (zid,googleid,reports) VALUES(?,?,?)',
                    [zid,null,quantity],
                    function(err2, res2, fields2) {
                        console.log("Insert results: " + res2);;
                    }
                )
            }else{
	    connection.query('SELECT * FROM reports WHERE zid = ?',[zid],
	    function(err3, res3, fields3){

	    var rep = res3[0]["reports"];
            var newRep = parseInt(rep) + parseInt(quantity);
            console.log("New Rep: " + newRep);
	    connection.query(
                'UPDATE reports SET reports = ? WHERE zid = ?;',
                [newRep, zid],
                function(err4, res4, fields4) {
                    if (err4) throw err4;
		    console.log("Update results: " + util.inspect(res4, {depth: null}));
                }
            )
	    })
            }
        }
    )
}


app.get("/repsleft", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
updateReps();
//For report counter
function updateReps(){
connection.query(
		'SELECT COUNT(*) FROM reports WHERE googleid = ?',
		[req.session.passport.user],
		function(err1, res1, fields) {
            if(res1[0]["COUNT(*)"] != 0){
                connection.query(
                'SELECT reports FROM reports WHERE googleid = ?',
                [req.session.passport.user],
                function(err2, res2, fields2) {
                    //Update report counter here
                    res.send("Reports Remaining: " + (res2[0]["reports"]).toString());
                })
            }else{
                
		getZuoraAccNum(req, function(error,data){
        		if (error) {
            		// error handling code goes here
            		console.log("ERROR : ",error);            
        		} else {            
            		// code to execute on data retrieval
            		zuoNum = data;
			//console.log("result from db is : ",data);   
        		    
		//});    
		    
		//var zNum = getZuoraAccNum(req);
                console.log("Znum: " + zuoNum);
		var options = { method: 'GET',
                    url: 'https://rest.zuora.com/v1/accounts/' + zuoNum,
                    headers: 
                    { 'Connection': 'keep-alive',
                         'Accept-Encoding': 'gzip, deflate',
                         'Cache-Control': 'no-cache',
                         'zuora-version': '223.0',
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         'apiSecretAccessKey': 'Laszlo96',
                         'apiAccessKeyId': 'support@4di.ai' },
                      body: 
                       { 
                       },
                      json: true };

                      request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                        console.log(body);
			var zuoraId = body.basicInfo.id;
                        //console.log("Zuora ID: " + zuoraId);
                        connection.query(
                            'SELECT COUNT(*) FROM reports WHERE zid = ?',
                            [zuoraId],
                            function(err3, res3, fields3) {
                                if(res3[0]["COUNT(*)"] != 0){
                                    connection.query(
                                        'UPDATE reports SET googleid = ? WHERE zid = ?;',
                                        [req.session.passport.user, zuoraId],
                                        function(err4, res4, fields4) {
                                            if(err4) throw err4;
                                            if(res4.affectedRows > 0){updateReps();}
                                        }
                                    )
                                }else{
                                    connection.query(
                                        'INSERT INTO reports (zid,googleid,reports) VALUES(?,?,?)',
                                        [zuoraId, req.session.passport.user, 0],
                                        function(err5, res5, fields5) {
                                            console.log("Insert Step: " + req.session.passport.user);
					    if(err5) throw err5;
                                            res.send('Reports Remaining: 0');
                                        }
                                    )
                                }
                            });
                      });
		}}) }
        })
}
})



app.post("/purchreps", function(req, res){
    var quant = req.body.reps;
    connection.query(
	'SELECT * FROM reports WHERE googleid = ?',
	[req.session.passport.user],
	function(err1, res1, fields) {
	    var zuoraId = res1[0]["zid"];
	    buyRep(zuoraId, quant);
	    res.send();
	 })
});


app.get("/usereport", require("connect-ensure-login").ensureLoggedIn("/login"), function(req, res) {
    connection.query(
        'SELECT * FROM reports WHERE googleid = ?',
        [req.session.passport.user],
        function(err2, res2, fields2) {
            var currRep = res2[0]["reports"];
            var newRep = currRep - 1;
            if(currRep <= 0){
		console.log("User is out of reports");
                res.status(400).send({
                message: 'User is out of reports.'
                });
            }else{
                connection.query(
                    'UPDATE reports SET reports = ? WHERE zid = ?;',
                    [newRep, res2[0]["zid"]],
                    function(err4, res4, fields4) {
                        if(err4) throw err4;
                        res.send();
                    }
                )
            }
        }
    )
});


app.get("/node/getzparams", function(req, res){
	var options = { method: 'POST',
  //Fix sandbox links
  url: 'https://rest.zuora.com/v1/rsa-signatures',
  headers: 
   { 'cache-control': 'no-cache',
     'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' },
  body: 
   { uri: 'https://www.zuora.com/apps/PublicHostedPageLite.do',
     //uri:"https://apisandbox.zuora.com/apps/PublicHostedPageLite.do",
     method: 'POST',
     //pageId: '2c92c0f86ef94c70016efbef47e92e11'},
     pageId: '2c92a0086db4fff2016dbb7ba1d65fec' },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  res.send(body);
});
});


app.get("/node/getzparamsSuff", function(req, res){
	var options = { method: 'POST',
  url: 'https://rest.zuora.com/v1/rsa-signatures',
  headers: 
   { 'cache-control': 'no-cache',
     'Connection': 'keep-alive',
     'Accept-Encoding': 'gzip, deflate',
     'Host': 'rest.zuora.com',
     'Cache-Control': 'no-cache',
     'Content-Type': 'application/json',
     'Accept': 'application/json',
     'apiSecretAccessKey': 'Laszlo96',
     'apiAccessKeyId': 'support@4di.ai' },
  body: 
   { uri: 'https://www.zuora.com/apps/PublicHostedPageLite.do',
     method: 'POST',
     pageId: '2c92a0fd6ef89f2a016efb930a187e13' },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  res.send(body);
});
});



var sft;
//var sfconn = new jsforce.Connection({
  //		loginUrl: 'https://test.salesforce.com',
//		instanceUrl: 'https://cs9.lightning.force.com',
//		accessToken: sft
//	});


app.get("/node/getcompany/", function(req, res){	
	var sfconn = new jsforce.Connection({
  		//loginUrl: 'https://test.salesforce.com',
		instanceUrl: 'https://na54.salesforce.com'
	});
	sfconn.login('rich.feimer@4di.ai', 'forcee4di0q27A3N5gex1hhSJJBbS26ACz', function(err, userInfo) {
  	if (err) { return console.error(err); }
  	// Now you can get the access token and instance URL information.
  	// Save them to establish connection next time.
  	console.log(sfconn.accessToken);
  	console.log(sfconn.instanceUrl);
	});
	var records = [];
	sfconn.query("SELECT Id, Name FROM Account", function(err, result) {
  	  if (err) { return console.error(err); }
  	  console.log("total : " + result.totalSize);
  	  //console.log("fetched : " + result.records[0].Name);
	  res.send(result);
	});
});


app.post("/node/sfcreation", function(req, res){
	var sfconn = new jsforce.Connection({
  		//loginUrl: 'https://test.salesforce.com',
		instanceUrl: 'https://na54.salesforce.com'
	});
sfconn.login('rich.feimer@4di.ai', 'forcee4di0q27A3N5gex1hhSJJBbS26ACz', function(err, userInfo) {
  if (err) { return console.error(err); }
 // console.log(sfconn.accessToken);
 // console.log(sfconn.instanceUrl);
  
});
    
	var data = req.body.info;
	
	
	var query1 = "SELECT Id FROM Account WHERE Name = '" + data.accName + "'"
	sfconn.query(query1, function(err, result) {
  	  if (err) { return console.error(err); }
  	  if(result.totalSize > 0){
	  console.log("Found " + result.totalSize + " matching existing account");
  	  data.accId = result.records[0].Id
	  console.log("SF Acc Id: " + data.accId);
	  searchContacts();
	  }else{
	    createAcc();
	  }
	});
	
	data.cid = "";
	function createAcc(){	
	//console.log("New Company: " + data.newComp);	
	  sfconn.sobject("Account").create({ Name : data.accName }, function(err, ret) {
  	    if (err || !ret.success) { return console.error(err, ret); }
  	      data.accId = ret.id;
	      console.log("Company id: " + data.accId);
	      searchContacts();  
	    });
	
	}
    
	function searchContacts(){	
	var records = [];
	sfconn.query("SELECT Id FROM Contact WHERE Email = '" + data.wemail+ "'" , function(err, result) {
  	  if (err) { return console.error(err); }
  	  if(result.totalSize > 0){
	    data.cid = result.records[0].Id;
	    console.log("Email matches existing Salesforce account")
	    console.log("Matching accounts: " + result.totalSize);
	    //res.send(data.cid);
	    res.send(data.accId);
	  }else{createContact();}
	  });
    }

    function createContact(){	
	if(data.cid == ""){
	  sfconn.sobject("Contact").create({ 
	  FirstName : data.firstName,
	  LastName: data.lastName,
	  Email: data.wemail,
	  AccountId: data.accId
	  }, function(err, ret) {
  	  if (err || !ret.success) { return console.error(err, ret); }
  	    console.log("Created record id : " + ret.id);
  	    res.send(data.accId);
	  });
	}		
    }


});

app.post("/dbupload", function(req, res){
	//var dbName = req.body.name;
	//var dbUrl = req.body.url;
	var user = req.user.googleid;
	/*var dbSize = req.body.size;
	var files = [];
	var dbFile={
	    "name": dbName,
	    "url": dbUrl,
	    "size": dbSize
	}
	files.push(dbFile);*/
	var files = JSON.parse(req.body.uploads)
	uploader.fileUploader(files, user, 0);
	res.send();
	/*
	var ext = dbUrl.split('.').pop()
	if(ext=="e57" || ext=="npy"){
	    ext="pcl";
	}
	var dbUploader = spawn("python",[
	    "../dropbox.py",
	    dbUrl,
	    user,
	    dbName,
	    ext
	   ]);

    dbUploader.stdout.on("data", function(data){
        console.log(data.toString("utf8"));
        res.send(data.toString());
    });
    
    dbUploader.stderr.on("data", function(data) {
        var error = data.toString("utf8");
        console.log("ERR," + error);
	res.send("ERR," + error)
    });   
    */
});


var server = app.listen(appConfig.listenPort, "localhost");
console.log("App started, listening on port " + String(appConfig.listenPort))
