# plotCloud.py
# Created by Sean Sullivan
# 4Di

# The entry point of the python generate process. Handles all report generation from a .npy pointcloud on s3,
# to .png outputs on S3 and a json returned to the site for indexing the report

import arrayfire as af
import file_read
import mathutils as ma
import matplotlib
import matplotlib.pyplot as plt 
import matplotlib.tri as tri
import numpy as np
import os
import time
import sys
import datetime
from AF_Analysis import afRANSAC as afr
from AF_Analysis import afFFFL
from AF_Analysis import Histogram as hst
from sys import argv
import json
from converter import unitConvert
from converter import unitVerbose

# total steps to be printed to the site
STEP_TOTAL = 4

# prints steps to the site as they are completed
def printToSite(start, step, output):
    time = datetime.datetime.now().timestamp() - start
    print("{:.9f}".format(time) + "s," + step + "/" + str(STEP_TOTAL) + "," + output)
    sys.stdout.flush()

#builds a string with the company name, address, and name of the reviewer
def get_reviewer_info(parsedInfo):
    reviewer = ""
    if not parsedInfo['address'] == '':
        reviewer += 'Address: ' + parsedInfo['address'] + '\n'
    if not parsedInfo['company'] == '':
        reviewer += 'Company: ' + parsedInfo['company'] + ', '
    if not parsedInfo['reviewer'] == '':
        reviewer += 'Reviewer: ' + parsedInfo['reviewer']
    return reviewer

if __name__ == "__main__":

    # The function is called with an extensible JSON encoded object that is parsed into all values required for a report
    parsedInfo = json.loads(argv[1])

    now = str(datetime.datetime.now()).replace(' ','_')
    startTime = datetime.datetime.now().timestamp()
    resultID = str(parsedInfo["resultID"])
    my_path = os.path.abspath(__file__) # Figures out the absolute path for you in case your working directory moves around.
    pointFile = parsedInfo["e57drop"]
    maxDist = float(parsedInfo["threshold"])
    samples = int(parsedInfo["samples"])
    units = parsedInfo["outUnits"]
    generatePTS = 0
    if ("genpts" in parsedInfo):
        generatePTS = int(parsedInfo["genpts"])

    subDivisions = 14
    cull = int(parsedInfo["culling"])
    autoDetect = parsedInfo["detect"] == "auto"
    height = 0
    if not autoDetect:
        height = float(parsedInfo["height"])
    
    reviewer = get_reviewer_info(parsedInfo)
    savee57 = True
    borderVals = []
    cadFile = ""
    cadFile = parsedInfo["dxfdrop"]
    alpha_val = 1
    INF = sys.maxsize
    ignore = float(parsedInfo["ignore"])

    googleid = parsedInfo["googleID"]
    if units != 'm':
        maxDist = unitConvert(maxDist, 'm', units)
        ignore = unitConvert(ignore, 'm', units)
        height = unitConvert(height, 'm', units)

    printToSite(startTime, "1", "Opening points file:" + pointFile)
    cloud = file_read.loadE57Data(pointFile, googleid, uses3=True)
    printToSite(startTime, "2", "Fitting plane to points")

    try:
        if autoDetect:
            plane = afr.ground_plane(
                cloud,
                maxDist,
                samples,
                culling=cull)

        else:
            plane = afr.user_input_plane(
                cloud,
                maxDist,
                height,
                culling=cull)
    except RuntimeError:
        print("ERR, Server overloaded. Please try again in a few minutes")
        sys.stdout.flush()
        quit()

    empty = np.full((1, 3), 0)
    if plane.inlier_data.size == 0 or plane.inlier_data.shape == empty.shape:
        # "ERR," is caught by the website and displayed as an error in red
        print("ERR, No points found at that height. Please enter another height or select automatic plane detection.")
        sys.stdout.flush()
    else:
        height = unitConvert(-1*plane.coeffs[3]/plane.coeffs[2], units)
        if ignore > 0:
           plane = plane._replace(inlier_data=afr.get_outliers(plane.inlier_data, plane.coeffs, ignore))

        bounds = hst.get_bounds(plane.inlier_data, int(parsedInfo["resolution"]))
        #y, m, d, h, m, s = now.year, now.month, now.day, now,hour, now.minute, now.second
        printToSite(startTime, "3", "Saving outputs")
        pts_url = ""
        if (generatePTS):
            pts_url = file_read.write_scan(plane.inlier_data, googleid, resultID)

        height_map = hst.avg_grid(plane.inlier_data, b=bounds)
        enc_url = file_read.upload_image(
            hst.encodeRGB(height_map, bounds["minz"], bounds["maxz"]).to_ndarray(),
            googleid,
            str(resultID)+"_encoded"
        )
        color_url = file_read.upload_image(
            hst.colorRGB(height_map, bounds["minz"], bounds["maxz"]).to_ndarray(),
            googleid,
            str(resultID)+"_colored"
        )
        npimg_url = file_read.upload_np(
            height_map,
            googleid,
            str(resultID)+"_npimg" 
        )
        
        cad_url = "none"
        if cadFile != "":
            cad_url = file_read.s3.meta.client.generate_presigned_url(
                    'get_object',
                    Params={
                        'Bucket': '4diuploads', 
                        'Key': str(googleid) + "/json/" + cadFile + ".json"
                        },
                    ExpiresIn=10800) 

        for key in bounds:
            if key != "sizex" and key != "sizey":
                bounds[key] = unitConvert(bounds[key], units)
        bounds["units"] = units
        
        outJSON = {
                "filename":pointFile,
                "image_enc":enc_url,
                "image_color":color_url,
                "npimg_url":npimg_url,
                "pts":pts_url,
                "design":cadFile,
                "reportID": resultID,
                "samples":samples,
                "culling":cull,
                "reviewer": {
                    "name": parsedInfo["reviewer"],
                    "company": parsedInfo["company"],
                    "address": parsedInfo["address"]
                    },
                "bounds": bounds,
                "planeThreshold" : maxDist,
                "planeHeight" : height,
                "errorThreshold" : ignore,
                "relative" : "scale" in parsedInfo
                }

        printToSite(startTime, "4", "Finished!")
        sys.stdout.flush()
        time.sleep(5)
        
        print("DONE," + json.dumps(outJSON))
        sys.stdout.flush()




