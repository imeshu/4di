import sys
from sys import argv


#This function assumes all inputs are in meters unless input units are specified
def unitConvert(val, outUnit, inUnit=''):
    ratio = {
        "m": 1,
        "in": 39.37007874,
        "ft": 3.28084,
        "16th": 629.92125984,
        "8th": 314.96062992,
        "surft": (3937/1200)
        }
    if inUnit=='':
        return(val*ratio[outUnit])
    else:
        mVal = (val/ratio[inUnit])
        return(mVal*ratio[outUnit])
    

def unitVerbose(abbr):
    uDict = {
        "m": "Meters",
        "in": "Inches",
        "ft": "Feet",
        "16th": "16ths of an Inch",
        "8th": "8ths of an Inch",
        "surft": "U.S. Survey Feet"
        }
    return(uDict[abbr])


if __name__ == "__main__":
    thresh = unitConvert(float(argv[1]), argv[2], argv[3])
    ignore = unitConvert(float(argv[4]), argv[2], argv[3])
    height = unitConvert(float(argv[5]), argv[2], argv[3])
    print(str(thresh)+'--'+str(ignore)+'--'+str(height))

