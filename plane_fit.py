# plane_fit.py
# Created by Sean Sullivan
# 4Di
import arrayfire as af
from AF_Analysis import afRANSAC as afr
import numpy as np
from sys import argv
import sys
import time
import file_read
import mathutils



def planeFit(cloud, borderVals, maxDist, numIters, sampleSize, fix, culling, autoDetect, height):
    #print("Plane fitting...")
    # Transform np array to af array
    afPoints = af.from_ndarray(cloud)
    # Ground plane fit - get coeffs and inliers
    if autoDetect:
        output = afr.ground_plane(afPoints, maxDist, numIters, min_inliers=-1, culling=culling)
    else:
        output = afr.user_input_plane(afPoints, maxDist, height, culling) 
    # Transfer cioeffs and inliers back to arrays we can work with
    coeffs = output.coeffs.to_list()
    
 
    inliers = output.inlier_data.to_ndarray()
	#    if (borderVals != []):
	#        xvals, yvals, zvals = mathutils.filterBoundingDWG(inliers, borderVals)

    return coeffs, inliers, output

if __name__ == "__main__":
    pointFile = argv[1]
    cadFile = argv[2]
    start = time.time()
    INF = sys.maxsize
    borderVals = file_read.getCadBorder(cadFile)
    print("Opening points file..")
    cloud = file_read.handleInput(pointFile)
    plane, plane_points = planeFit(cloud, borderVals, .08, 200, 3, af.Array([0, 0, 1]))
    print("Outputting plane points to surface.csv")
    np.savetxt("surface.csv", plane_points, delimiter=',')
    print(start - time.time())
