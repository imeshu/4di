import smart_open
import requests
import io
import boto3
import sys
from sys import argv

def dropboxUp(url, gid, name, ext):
    r = requests.get(url, allow_redirects=True)
    with io.BytesIO() as buf:
        buf.write(r.content)
        buf.seek(0)
        print("Get done")

        key = 's3://4diuploads/'+ gid + '/' + ext + '/' + name
        with smart_open.open(key, 'wb') as bufout:
            bufout.write(buf.getvalue())
            print("Done: " + key)
            buf.close()

if __name__ == "__main__":
    dropboxUp(argv[1], argv[2], argv[3], argv[4])
    

