# afFFFL_test.py
# Created by Mica Lewis
# 4Di
# a test bed for histogram functions

import arrayfire as af
import numpy as np
from AF_Analysis import Histogram as hst
from AF_Analysis import afRANSAC as afr
from AF_Analysis import afFFFL as af3fl
from matplotlib import collections  as mc
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.figure import Figure
from scipy.ndimage import filters
import json
import matplotlib.pyplot as plt
import png
import time
import math

bounds = json.load(open("bounds.json"))
img = np.load("resultimg.npy")

lines = [
    {"start":(-100,10), "end":(8,10)},
    {"start":(0,9), "end":(8,9)},
    {"start":(0,8), "end":(8,8)},
    {"start":(0,7), "end":(8,7)},
    ]

region = {
    "origin": (-2, 2),
    "wh": (20, 8),
    "angle": 0
}

report = af3fl.flreport([lines, region], img, bounds, 'm', filename="test_data.e57", minff=7, specff=12, specfl=7)

with PdfPages("testpdf.pdf") as pdf:
    report.plotSections(pdf)
