#include <cstring> // memcpy
#include <cstdlib> //realloc
#include <iostream>
#include "E57Format.h"
#include "E57Header.h"
#include <vector>
#include <array>
#include <stdio.h>
#include <math.h>
#include <exception>
#include "cnpy.h"
//using namespace std;
using namespace e57;

struct PoseMat
{
	float rotMat[3][3];
	float translation[3];
};

//Freely inspired from "E57 Simple API" by Stan Coleby
//This function uses the prototype subnode of a scan to store
//vital information about the scan
static void DecodePrototype(const e57::StructureNode& scan, const e57::StructureNode& proto, E57ScanHeader& header)
{

	// Get a prototype of datatypes that will be stored in points record.
	header.pointFields.cartesianXField = proto.isDefined("cartesianX");
	header.pointFields.cartesianYField = proto.isDefined("cartesianY");
	header.pointFields.cartesianZField = proto.isDefined("cartesianZ");
	header.pointFields.cartesianInvalidStateField = proto.isDefined("cartesianInvalidState");

	header.pointFields.pointRangeScaledInteger = 0; //FloatNode
	header.pointFields.pointRangeMinimum = 0;
	header.pointFields.pointRangeMaximum = 0;

	// Check if coordinates are spherical or cartesian
	if (proto.isDefined("cartesianX"))
	{
		if (proto.get("cartesianX").type() == e57::E57_SCALED_INTEGER)
		{
			// Check if the node containing binary data is based on an integer and scale
			double scale = e57::ScaledIntegerNode(proto.get("cartesianX")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("cartesianX")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("cartesianX")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("cartesianX")).maximum();
			header.pointFields.pointRangeMinimum = minimum * scale + offset;
			header.pointFields.pointRangeMaximum = maximum * scale + offset;
			header.pointFields.pointRangeScaledInteger = scale;

		}
		else if (proto.get("cartesianX").type() == e57::E57_FLOAT)
		{
			// check if the node containing binary data is based on floats
			header.pointFields.pointRangeMinimum = e57::FloatNode(proto.get("cartesianX")).minimum();
			header.pointFields.pointRangeMaximum = e57::FloatNode(proto.get("cartesianX")).maximum();
		}
	}
	else if (proto.isDefined("sphericalRange"))
	{
		// mirrored from cartesian
		if (proto.get("sphericalRange").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("sphericalRange")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("sphericalRange")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("sphericalRange")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("sphericalRange")).maximum();
			header.pointFields.pointRangeMinimum = minimum * scale + offset;
			header.pointFields.pointRangeMaximum = maximum * scale + offset;
			header.pointFields.pointRangeScaledInteger = scale;

		}
		// mirroed from cartesian
		else if (proto.get("sphericalRange").type() == e57::E57_FLOAT)
		{
			header.pointFields.pointRangeMinimum = e57::FloatNode(proto.get("sphericalRange")).minimum();
			header.pointFields.pointRangeMaximum = e57::FloatNode(proto.get("sphericalRange")).maximum();
		}
	}

	// the three coordinates of spherical coordinates
	header.pointFields.sphericalRangeField = proto.isDefined("sphericalRange");
	header.pointFields.sphericalAzimuthField = proto.isDefined("sphericalAzimuth");
	header.pointFields.sphericalElevationField = proto.isDefined("sphericalElevation");
	header.pointFields.sphericalInvalidStateField = proto.isDefined("sphericalInvalidState");

	header.pointFields.angleScaledInteger = 0.; //FloatNode
	header.pointFields.angleMinimum = 0.;
	header.pointFields.angleMaximum = 0.;

	if (proto.isDefined("sphericalAzimuth"))
	{
		if (proto.get("sphericalAzimuth").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("sphericalAzimuth")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("sphericalAzimuth")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("sphericalAzimuth")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("sphericalAzimuth")).maximum();
			header.pointFields.angleMinimum = minimum * scale + offset;
			header.pointFields.angleMaximum = maximum * scale + offset;
			header.pointFields.angleScaledInteger = scale;

		}
		else if (proto.get("sphericalAzimuth").type() == e57::E57_FLOAT)
		{
			header.pointFields.angleMinimum = e57::FloatNode(proto.get("sphericalAzimuth")).minimum();
			header.pointFields.angleMaximum = e57::FloatNode(proto.get("sphericalAzimuth")).maximum();
		}
	}

	// values set beyond this point are not used in the generation of numpy array

	header.pointFields.rowIndexField = proto.isDefined("rowIndex");
	header.pointFields.columnIndexField = proto.isDefined("columnIndex");
	header.pointFields.rowIndexMaximum = 0;
	header.pointFields.columnIndexMaximum = 0;

	if (proto.isDefined("rowIndex"))
	{
		header.pointFields.rowIndexMaximum = static_cast<uint32_t>(e57::IntegerNode(proto.get("rowIndex")).maximum());
	}

	if (proto.isDefined("columnIndex"))
	{
		header.pointFields.columnIndexMaximum = static_cast<uint32_t>(e57::IntegerNode(proto.get("columnIndex")).maximum());
	}

	header.pointFields.returnIndexField = proto.isDefined("returnIndex");
	header.pointFields.returnCountField = proto.isDefined("returnCount");
	header.pointFields.returnMaximum = 0;

	if (proto.isDefined("returnIndex"))
	{
		header.pointFields.returnMaximum = static_cast<uint8_t>(e57::IntegerNode(proto.get("returnIndex")).maximum());
	}

	header.pointFields.timeStampField = proto.isDefined("timeStamp");
	header.pointFields.isTimeStampInvalidField = proto.isDefined("isTimeStampInvalid");
	header.pointFields.timeMaximum = 0.;

	if (proto.isDefined("timeStamp"))
	{
		if (proto.get("timeStamp").type() == e57::E57_INTEGER)
			header.pointFields.timeMaximum = static_cast<double>(e57::IntegerNode(proto.get("timeStamp")).maximum());
		else if (proto.get("timeStamp").type() == e57::E57_FLOAT)
			header.pointFields.timeMaximum = static_cast<double>(e57::FloatNode(proto.get("timeStamp")).maximum());
	}

	header.pointFields.intensityField = proto.isDefined("intensity");
	header.pointFields.isIntensityInvalidField = proto.isDefined("isIntensityInvalid");
	header.pointFields.intensityScaledInteger = 0.;

	header.intensityLimits.intensityMinimum = 0.;
	header.intensityLimits.intensityMaximum = 0.;

	if (scan.isDefined("intensityLimits"))
	{
		e57::StructureNode intbox(scan.get("intensityLimits"));
		if (intbox.get("intensityMaximum").type() == e57::E57_SCALED_INTEGER)
		{
			header.intensityLimits.intensityMaximum = e57::ScaledIntegerNode(intbox.get("intensityMaximum")).scaledValue();
			header.intensityLimits.intensityMinimum = e57::ScaledIntegerNode(intbox.get("intensityMinimum")).scaledValue();
		}
		else if (intbox.get("intensityMaximum").type() == e57::E57_FLOAT)
		{
			header.intensityLimits.intensityMaximum = e57::FloatNode(intbox.get("intensityMaximum")).value();
			header.intensityLimits.intensityMinimum = e57::FloatNode(intbox.get("intensityMinimum")).value();
		}
		else if (intbox.get("intensityMaximum").type() == e57::E57_INTEGER)
		{
			header.intensityLimits.intensityMaximum = static_cast<double>(e57::IntegerNode(intbox.get("intensityMaximum")).value());
			header.intensityLimits.intensityMinimum = static_cast<double>(e57::IntegerNode(intbox.get("intensityMinimum")).value());
		}
	}

	if (proto.isDefined("intensity"))
	{
		if (proto.get("intensity").type() == e57::E57_INTEGER)
		{
			if (header.intensityLimits.intensityMaximum == 0.)
			{
				header.intensityLimits.intensityMinimum = static_cast<double>(e57::IntegerNode(proto.get("intensity")).minimum());
				header.intensityLimits.intensityMaximum = static_cast<double>(e57::IntegerNode(proto.get("intensity")).maximum());
			}
			header.pointFields.intensityScaledInteger = -1.;

		}
		else if (proto.get("intensity").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("intensity")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("intensity")).offset();

			if (header.intensityLimits.intensityMaximum == 0.)
			{
				int64_t minimum = e57::ScaledIntegerNode(proto.get("intensity")).minimum();
				int64_t maximum = e57::ScaledIntegerNode(proto.get("intensity")).maximum();
				header.intensityLimits.intensityMinimum = minimum * scale + offset;
				header.intensityLimits.intensityMaximum = maximum * scale + offset;
			}
			header.pointFields.intensityScaledInteger = scale;
		}
		else if (proto.get("intensity").type() == e57::E57_FLOAT)
		{
			if (header.intensityLimits.intensityMaximum == 0.)
			{
				header.intensityLimits.intensityMinimum = e57::FloatNode(proto.get("intensity")).minimum();
				header.intensityLimits.intensityMaximum = e57::FloatNode(proto.get("intensity")).maximum();
			}
		}
	}

	header.pointFields.colorRedField = proto.isDefined("colorRed");
	header.pointFields.colorGreenField = proto.isDefined("colorGreen");
	header.pointFields.colorBlueField = proto.isDefined("colorBlue");
	header.pointFields.isColorInvalidField = proto.isDefined("isColorInvalid");

	header.colorLimits.colorRedMinimum = 0.;
	header.colorLimits.colorRedMaximum = 0.;
	header.colorLimits.colorGreenMinimum = 0.;
	header.colorLimits.colorGreenMaximum = 0.;
	header.colorLimits.colorBlueMinimum = 0.;
	header.colorLimits.colorBlueMaximum = 0.;

	if (scan.isDefined("colorLimits"))
	{
		e57::StructureNode colorbox(scan.get("colorLimits"));
		if (colorbox.get("colorRedMaximum").type() == e57::E57_SCALED_INTEGER)
		{
			header.colorLimits.colorRedMaximum = e57::ScaledIntegerNode(colorbox.get("colorRedMaximum")).scaledValue();
			header.colorLimits.colorRedMinimum = e57::ScaledIntegerNode(colorbox.get("colorRedMinimum")).scaledValue();
			header.colorLimits.colorGreenMaximum = e57::ScaledIntegerNode(colorbox.get("colorGreenMaximum")).scaledValue();
			header.colorLimits.colorGreenMinimum = e57::ScaledIntegerNode(colorbox.get("colorGreenMinimum")).scaledValue();
			header.colorLimits.colorBlueMaximum = e57::ScaledIntegerNode(colorbox.get("colorBlueMaximum")).scaledValue();
			header.colorLimits.colorBlueMinimum = e57::ScaledIntegerNode(colorbox.get("colorBlueMinimum")).scaledValue();
		}
		else if (colorbox.get("colorRedMaximum").type() == e57::E57_FLOAT)
		{
			header.colorLimits.colorRedMaximum = e57::FloatNode(colorbox.get("colorRedMaximum")).value();
			header.colorLimits.colorRedMinimum = e57::FloatNode(colorbox.get("colorRedMinimum")).value();
			header.colorLimits.colorGreenMaximum = e57::FloatNode(colorbox.get("colorGreenMaximum")).value();
			header.colorLimits.colorGreenMinimum = e57::FloatNode(colorbox.get("colorGreenMinimum")).value();
			header.colorLimits.colorBlueMaximum = e57::FloatNode(colorbox.get("colorBlueMaximum")).value();
			header.colorLimits.colorBlueMinimum = e57::FloatNode(colorbox.get("colorBlueMinimum")).value();
		}
		else if (colorbox.get("colorRedMaximum").type() == e57::E57_INTEGER)
		{
			header.colorLimits.colorRedMaximum = static_cast<double>(e57::IntegerNode(colorbox.get("colorRedMaximum")).value());
			header.colorLimits.colorRedMinimum = static_cast<double>(e57::IntegerNode(colorbox.get("colorRedMinimum")).value());
			header.colorLimits.colorGreenMaximum = static_cast<double>(e57::IntegerNode(colorbox.get("colorGreenMaximum")).value());
			header.colorLimits.colorGreenMinimum = static_cast<double>(e57::IntegerNode(colorbox.get("colorGreenMinimum")).value());
			header.colorLimits.colorBlueMaximum = static_cast<double>(e57::IntegerNode(colorbox.get("colorBlueMaximum")).value());
			header.colorLimits.colorBlueMinimum = static_cast<double>(e57::IntegerNode(colorbox.get("colorBlueMinimum")).value());
		}
	}

	if ((header.colorLimits.colorRedMaximum == 0.) && proto.isDefined("colorRed"))
	{
		if (proto.get("colorRed").type() == e57::E57_INTEGER)
		{
			header.colorLimits.colorRedMinimum = static_cast<double>(e57::IntegerNode(proto.get("colorRed")).minimum());
			header.colorLimits.colorRedMaximum = static_cast<double>(e57::IntegerNode(proto.get("colorRed")).maximum());
		}
		else if (proto.get("colorRed").type() == e57::E57_FLOAT)
		{
			header.colorLimits.colorRedMinimum = e57::FloatNode(proto.get("colorRed")).minimum();
			header.colorLimits.colorRedMaximum = e57::FloatNode(proto.get("colorRed")).maximum();
		}
		else if (proto.get("colorRed").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("colorRed")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("colorRed")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("colorRed")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("colorRed")).maximum();
			header.colorLimits.colorRedMinimum = minimum * scale + offset;
			header.colorLimits.colorRedMaximum = maximum * scale + offset;
		}
	}

	if ((header.colorLimits.colorGreenMaximum == 0.) && proto.isDefined("colorGreen"))
	{
		if (proto.get("colorGreen").type() == e57::E57_INTEGER)
		{
			header.colorLimits.colorGreenMinimum = static_cast<double>(e57::IntegerNode(proto.get("colorGreen")).minimum());
			header.colorLimits.colorGreenMaximum = static_cast<double>(e57::IntegerNode(proto.get("colorGreen")).maximum());
		}
		else if (proto.get("colorGreen").type() == e57::E57_FLOAT)
		{
			header.colorLimits.colorGreenMinimum = e57::FloatNode(proto.get("colorGreen")).minimum();
			header.colorLimits.colorGreenMaximum = e57::FloatNode(proto.get("colorGreen")).maximum();
		}
		else if (proto.get("colorGreen").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("colorGreen")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("colorGreen")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("colorGreen")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("colorGreen")).maximum();
			header.colorLimits.colorGreenMinimum = minimum * scale + offset;
			header.colorLimits.colorGreenMaximum = maximum * scale + offset;
		}
	}
	if ((header.colorLimits.colorBlueMaximum == 0.) && proto.isDefined("colorBlue"))
	{
		if (proto.get("colorBlue").type() == e57::E57_INTEGER)
		{
			header.colorLimits.colorBlueMinimum = static_cast<double>(e57::IntegerNode(proto.get("colorBlue")).minimum());
			header.colorLimits.colorBlueMaximum = static_cast<double>(e57::IntegerNode(proto.get("colorBlue")).maximum());
		}
		else if (proto.get("colorBlue").type() == e57::E57_FLOAT)
		{
			header.colorLimits.colorBlueMinimum = e57::FloatNode(proto.get("colorBlue")).minimum();
			header.colorLimits.colorBlueMaximum = e57::FloatNode(proto.get("colorBlue")).maximum();
		}
		else if (proto.get("colorBlue").type() == e57::E57_SCALED_INTEGER)
		{
			double scale = e57::ScaledIntegerNode(proto.get("colorBlue")).scale();
			double offset = e57::ScaledIntegerNode(proto.get("colorBlue")).offset();
			int64_t minimum = e57::ScaledIntegerNode(proto.get("colorBlue")).minimum();
			int64_t maximum = e57::ScaledIntegerNode(proto.get("colorBlue")).maximum();
			header.colorLimits.colorRedMinimum = minimum * scale + offset;
			header.colorLimits.colorRedMaximum = maximum * scale + offset;
		}
	}
}

//Gets a scans rotation and translation
static bool GetPoseInformation(const e57::StructureNode& node, PoseMat* poseMat)
{
	bool validPoseMat = false;
	if (node.isDefined("pose"))
	{
		e57::StructureNode pose(node.get("pose"));
		if (pose.isDefined("rotation"))
		{
			// rotates data based on rotation field
			e57::StructureNode rotNode(pose.get("rotation"));
			float wquat = e57::FloatNode(rotNode.get("w")).value();
			float xquat = e57::FloatNode(rotNode.get("x")).value();
			float yquat = e57::FloatNode(rotNode.get("y")).value();
			float zquat = e57::FloatNode(rotNode.get("z")).value();
			(*poseMat).rotMat[0][0] = (1 - (2 * yquat*yquat) - (2 * zquat*zquat));
			(*poseMat).rotMat[0][1] = (2 * xquat*yquat) - (2 * zquat*wquat);
			(*poseMat).rotMat[0][2] = (2 * xquat*zquat) + (2 * yquat*wquat);
			(*poseMat).rotMat[1][0] = (2 * xquat*yquat) + (2 * zquat*wquat);
			(*poseMat).rotMat[1][1] = (1 - (2 * xquat*xquat) - (2 * zquat*zquat));
			(*poseMat).rotMat[1][2] = (2 * zquat*yquat) - (2 * xquat*wquat);
			(*poseMat).rotMat[2][0] = (2 * xquat*zquat) - (2 * yquat*wquat);
			(*poseMat).rotMat[2][1] = (2 * zquat*yquat) + (2 * xquat*wquat);
			(*poseMat).rotMat[2][2] = (1 - (2 * xquat*xquat) - (2 * yquat*yquat));
			validPoseMat = true;
		}

		if (pose.isDefined("translation"))
		{
			// translates data based on translation field
			e57::StructureNode transNode(pose.get("translation"));
			(*poseMat).translation[0] = (FloatNode(transNode.get("x"))).value();
			(*poseMat).translation[1] = (FloatNode(transNode.get("y"))).value();
			(*poseMat).translation[2] = (FloatNode(transNode.get("z"))).value();
			std::cout << "Translation: " << (*poseMat).translation[0] << "," << (*poseMat).translation[1] << "," << (*poseMat).translation[2];
			validPoseMat = true;
		}
	}

	return validPoseMat;
}

int main(int argc, char** argv) {
	try {
		// name of the input e57 file and the output npy file
		char* filename = argv[1];
		char* outfile = argv[2];
		
		// loads e57
		ImageFile imf(filename, "r");
		// finds root node
		StructureNode root = imf.root();
		// find all data in a vector of scans
		VectorNode data3D = static_cast<VectorNode> (root.get("data3D"));
		bool sphericalMode = false;

		for (int i=0; i<data3D.childCount(); i++) {
			
			StructureNode scani(data3D.get(i));
			CompressedVectorNode points = static_cast<CompressedVectorNode> (scani.get("points"));
			const unsigned pointCount = points.childCount();

			// Raw point buffer and invalidData buffer
			float* cloudpoints = (float*)malloc(pointCount * 3 * sizeof(float));
			int* invalidData = (int*)malloc(pointCount * sizeof(int));

			// Use prototype to get scan meta-data
			e57::StructureNode prototype(points.prototype());
			E57ScanHeader header;
			DecodePrototype(scani, prototype, header);

			//no cartesian fields?
			if (!header.pointFields.cartesianXField &&
				!header.pointFields.cartesianYField &&
				!header.pointFields.cartesianZField)
			{
				//let's look for spherical ones
				if (!header.pointFields.sphericalRangeField &&
					!header.pointFields.sphericalAzimuthField &&
					!header.pointFields.sphericalElevationField)
				{
					return -1;
				}
				sphericalMode = true;
			}
			// Object to hold rotation and translation
			PoseMat poseMat;
			const bool validPoseMat = GetPoseInformation(scani, &poseMat);
			// Buffer to contain transformed point cloud
			float* finalpoints = (float*)malloc(pointCount * 3 * sizeof(float));
			// E57 Buffer object for reading
			std::vector<e57::SourceDestBuffer> dbufs;

			// Fill SourceDestBuffers and instruct to read into cloudpoints and invaliddata buffers
			if (sphericalMode) {
                        	dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "sphericalRange", &cloudpoints[0], pointCount, true, true, 3*sizeof(float)));
				dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "sphericalAzimuth", &cloudpoints[1], pointCount, true, true, 3*sizeof(float)));
				dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "sphericalElevation", &cloudpoints[2], pointCount, true, true, 3*sizeof(float)));
				if (header.pointFields.sphericalInvalidStateField) {
					std::cout << "Spherical Invalid field exists";
                                        dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "sphericalInvalidState", &invalidData[0], pointCount, true, true, sizeof(int)));
                                }
			} else {
				dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "cartesianX", &cloudpoints[0], pointCount, true, true, 3*sizeof(float)));
				dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "cartesianY", &cloudpoints[1], pointCount, true, true, 3*sizeof(float)));
				dbufs.push_back(SourceDestBuffer(scani.destImageFile(), "cartesianZ", &cloudpoints[2], pointCount, true, true, 3*sizeof(float)));
				if (header.pointFields.cartesianInvalidStateField) {
					dbufs.push_back(SourceDestBuffer(imf, "cartesianInvalidState", &invalidData[0], pointCount, true, true, sizeof(int)));
				}
			}
		
			// Check which nodes are defined in the scan
			bool hasPose = false;
			bool hasTrans = false;
			bool hasRot = false;
			if (scani.isDefined("pose"))
			{
				hasPose = true;
				e57::StructureNode pose(scani.get("pose"));
				hasTrans = pose.isDefined("translation");
				hasRot = pose.isDefined("rotation");
			}

			// Create reader object
			e57::CompressedVectorReader dataReader = points.reader(dbufs);
			
			// How many valid points we actually save to buffer
			int realCount = 0;
			
			unsigned size = 0;
			// Read loop
			while ((size = dataReader.read()))
			{
				for (unsigned i = 0; i < size; ++i)
				{
					//we skip invalid points!
					if ((invalidData[(i)] != 0) || ((abs(cloudpoints[(i*3)]) < 0.000001) && (abs(cloudpoints[(i*3)+1]) < 0.000001 )))
					{
						continue;
					}

					float Pd[3][1] = { {0}, {0}, {0} };
					if (sphericalMode)
					{
						float r = cloudpoints[i*3];
						float theta = cloudpoints[(i*3)+1];	//Azimuth
						float phi = cloudpoints[(i*3)+2];		//Elevation

						float cos_phi = cos(phi);
						Pd[0][0] = r * cos_phi * cos(theta);
						Pd[1][0] = r * cos_phi * sin(theta);
						Pd[2][0] = r * sin(phi);
					}
					//DGM TODO: not handled yet (-->what are the standard cylindrical field names?)
					/*else if (cylindricalMode)
					{

					}*/
					
					else //cartesian
					{
						Pd[0][0] = cloudpoints[(i*3)];
						Pd[1][0] = cloudpoints[(i*3)+1];
						Pd[2][0] = cloudpoints[(i*3)+2];
					}
				        
					if (hasPose)
					{
                 		        	float translatedPoint[3][1] = { {0}, {0}, {0} };
						
						for(int i2=0;i2<3;i2++)
        					{
            						for(int j2=0;j2<1;j2++)
        	    					{
	                					if(hasTrans)
									translatedPoint[i2][j2] = poseMat.translation[i2];
                						for(int k2=0;k2<3;k2++)
                						{
									if(hasRot)
                  								translatedPoint[i2][j2]+=poseMat.rotMat[i2][k2]*Pd[k2][j2];
                						}
            						}
        					}
					
						float finalPoint[3] = { translatedPoint[0][0],translatedPoint[1][0],translatedPoint[2][0] };
						memcpy(&finalpoints[realCount*3], &finalPoint[0], 3*sizeof(float));
					} else {
						memcpy(&finalpoints[realCount*3], &Pd[0], 3*sizeof(float));
					}
					realCount++;
				}
				
			}
			
			free(invalidData);
			free(cloudpoints);
			std::vector<long unsigned int> arrshape = {realCount,3};
			
			if (i > 0) {
				cnpy::npy_save(outfile, finalpoints, arrshape, "a");
			} else {
				cnpy::npy_save(outfile, finalpoints, arrshape, "w");
			}
			free(finalpoints);
			dataReader.close();
		}
		imf.close(); // don't forget to explicitly close the ImageFile
	}
	catch (E57Exception& ex) {
		ex.report(__FILE__, __LINE__, __FUNCTION__);
		return(-1);
	}
	catch (std::exception& e) {
		std::cout << "\nEXCEPTION " << e.what();
	}
	return(0);
}
