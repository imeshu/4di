# file-read.py
# Created by Sean Sullivan
# 4Di

import numpy as np
import pye57
import mathutils
import ezdxf
import sys
import png
from sys import argv
from laspy.file import File
import boto3
import botocore
import io
import json
import os
import subprocess
s3 = boto3.resource('s3')
bucket = s3.Bucket('4diuploads')
pdfbucket = s3.Bucket('4dipdfs')
ups3 = boto3.client('s3')

# Writes a .pts file from an array of points and uploads that to S3
# points: a numpy array of points (python arrays *may* work as well, this is untested)
# googleid: the google id of the user to which the point cloud is uploaded
# outputid: the name used for the pts file
def write_scan(points, googleid, outputid):
    local_fname = "../uploaded_files/" + str(outputid) + ".pts"
    key = str(googleid) + "/outputs/" + str(outputid) + ".pts"
    with open(local_fname, "w") as f:
        f.write("%s\n" % str(len(points)))
        for point in points:
            f.write("%s %s %s 0 0 0 0\n" % (str(point[0]), str(point[1]), str(point[2])))
        s3.meta.client.upload_file(
                local_fname,
                '4diuploads',
                key)
        os.remove(local_fname)
        return s3.meta.client.generate_presigned_url(
                'get_object',
                Params={
                    'Bucket': '4diuploads',
                    'Key': key
                    }, 
                ExpiresIn=10800)

# Writes a .png based on a numpy array and uploads that to S3
# points: a numpy array of RGB values
# googleid: the google id of the user to which the image is uploaded
# outputid: the name used for the png file
def upload_image(image, googleid, outputid):
    local_fname = "../uploaded_files/" + str(outputid) + ".png"
    png.from_array(
            image,
            mode="RGB;8" ).save(local_fname)
    return upload_output(local_fname, googleid, outputid + ".png")

def upload_np(arr, googleid, outputid):
    local_fname = "../uploaded_files/" + str(outputid) + ".npy"
    np.save(local_fname, arr)
    return upload_output(local_fname, googleid, outputid + ".npy")

def upload_output(local_fname, googleid, outputid):
    key = str(googleid) + "/outputs/" + str(outputid)
    s3.meta.client.upload_file(
            local_fname,
            '4diuploads', 
            key)
    os.remove(local_fname)
    return s3.meta.client.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': '4diuploads',
                'Key': key
                },
            ExpiresIn=10800)

def upload_pdf(local_fname, googleid, outputid):
    key = str(googleid) + "/flpreview/" + str(outputid) + ".pdf"
    s3.meta.client.upload_file(
            local_fname,
            '4dipdfs', 
            key,
            ExtraArgs={
                'ContentType': 'application/pdf',
                'ContentDisposition': 'inline',
                'ACL':'public-read'})
    os.remove(local_fname)
    return s3.meta.client.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': '4dipdfs',
                'Key': key
                },
            ExpiresIn=10800)

# if uses3 = True: returns numpy array from s3
# if uses3 = False: saves a given .e57 file in uploaded files
def loadE57Data(filename, googleid=0, uses3 = False):
    if uses3:
        with io.BytesIO() as f:
            obj = bucket.Object(googleid + "/pcl/" + filename + ".npy")
            filedata = obj.get()['Body'].read()
            data = np.load(io.BytesIO(filedata), allow_pickle=False)
            return data

    subprocess.call([
        "./../a.out",
        "../uploaded_files/" + filename,
        "../uploaded_files/" + filename + ".npy"])

    return 0

def loadNPYData(filename, googleid):
    with io.BytesIO() as f:
        obj = bucket.Object(googleid + "/outputs/" + filename + ".npy")
        filedata = obj.get()['Body'].read()
        data = np.load(io.BytesIO(filedata), allow_pickle=False)
        return data

# saves a given las file to uploaded_files directory
def loadLASData(fname):
    filename = "../uploaded_files/" + fname
    infile = File(filename, mode='r')
    print(dir(infile))
    I = infile.Classification == 2
    points = infile.points
    print(points)
    return np.array(points)

# takes in filename and outputs an equivelent numpy to uploaded_files
def loadPTSData(fname):
    filename = "../uploaded_files/" + fname
    with open(filename, 'r') as f:
        numpoints = int(next(f))
	
        outputarray = np.ndarray(shape=(numpoints, 3), dtype=np.float32)
        i = 0
        for line in f:
            if i%100000 == 0:
                pass
            isa = line.split(' ')
            if len(isa) > 1:
                infoarray = np.ndarray(shape=(3), buffer=np.array([np.float32(isa[0]), np.float32(isa[1]), np.float32(isa[2])]), dtype=np.float32)
                outputarray[i] = infoarray
                i = i + 1
    return outputarray
    
def handleInput(filename, gid):
    ext = filename[-4:]
    if ext == ".e57":
        return loadE57Data(filename)
    elif ext == ".dxf":
        return loadCadData(filename, gid)
    elif ext == ".pts":
        return loadPTSData(filename)
    #elif ext == ".csv":
        #csv = open(filename)
        #return pd.read_csv(csv).drop_duplicates()

def loadCadData(fname, gid):
    filename = "../uploaded_files/"+fname
    doc = ezdxf.readfile(filename)
    jsonify(filename, doc, gid)

DXF_UNITS = {
    1:"in",
    2:"ft",
    6:"m",
    21:"surft"
}
def jsonify(fn, doc, gid):

    msp = doc.modelspace()
    jsonl = {}
    jsonl["LINES"] = []
    if doc.header['$INSUNITS'] in DXF_UNITS:
        jsonl["UNITS"] = DXF_UNITS[doc.header['$INSUNITS']]
    if doc.header['$INSUNITS'] == 0:
        jsonl["UNITS"] = 'ft'

    for e in msp.query('LINE'):
        startC = (str(e.dxf.start).translate({ord(i): None for i in '()'}))
        endC = (str(e.dxf.end).translate({ord(i): None for i in '()'}))
        jsonl["LINES"].append({
            "start": startC.split(', '),
            "end": endC.split(', ')
            })

    for face in msp.query('3DFACE'):
        jsonl["LINES"].append({"start": face.dxf.vtx0, "end":face.dxf.vtx1})
        jsonl["LINES"].append({"start": face.dxf.vtx1, "end":face.dxf.vtx2})
        jsonl["LINES"].append({"start": face.dxf.vtx2, "end":face.dxf.vtx3})
        jsonl["LINES"].append({"start": face.dxf.vtx3, "end":face.dxf.vtx0})
    
    for mesh in msp.query('MESH'):
        for edge in mesh.edges:
            jsonl["LINES"].append({"start": mesh.vertices[edge[0]], "end": mesh.vertices[edge[1]]})

    final_list = []
    for line in jsonl["LINES"]: 
        if line not in final_list: 
            final_list.append(line) 
    jsonl["LINES"] = final_list 

    with open(fn + '.json', 'w') as outfile:
        json.dump(jsonl, outfile)
    
    ups3.upload_file((fn + '.json'), '4diuploads', gid + '/json/' + os.path.basename(fn) + '.json')
    ups3.upload_file(fn, '4diuploads', gid + '/dxf/' + os.path.basename(fn))
    print("DXF and JSON uploaded to S3")
    sys.stdout.flush()
    return 0

def getCadBorder(cFile, alpha_val, googleid=0, uses3=False):
    cadFile = "../uploaded_files/" + cFile
    if cadFile == "../uploaded_files/none" or cadFile == "../uploaded_files/undefined" or cadFile == "none":
    	return np.empty((1,1))
    if uses3:
        try:
            bucket.download_file(googleid + '/dxf/' + cadfile, "../uploaded_files/" + cadfile)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise
        
        dxf= ezdxf.readfile("../uploaded_files/" + cadfile)
        msp = dxf.modelspace()
        polyVals = []
        outlines = []
    
        for e in msp.query('LINE'):
            polyVals.append([e.dxf.start[0], e.dxf.start[1]])
            polyVals.append([e.dxf.end[0], e.dxf.end[1]])
	
        polyVals = np.array(polyVals)
        if (polyVals.shape[0] > 4):
            outlines = mathutils.alpha_shape(polyVals, alpha_val)
        borderVals = []
        for outline in outlines:
            for point in outline:
                borderVals.append(polyVals[point])

        return np.array(borderVals)


    dxf = ezdxf.readfile(cadFile)
    msp = dxf.modelspace()
    polyVals = []
    outlines = []
    
    for e in msp.query('LINE'):
        polyVals.append([e.dxf.start[0], e.dxf.start[1]])
        polyVals.append([e.dxf.end[0], e.dxf.end[1]])
	
    polyVals = np.array(polyVals)
    if (polyVals.shape[0] > 4):
        outlines = mathutils.alpha_shape(polyVals, alpha_val)
    borderVals = []
    for outline in outlines:
        for point in outline:
            borderVals.append(polyVals[point])
            
    return np.array(borderVals)

def getCADJSON(filename, googleid):
    uri = '{0}/json/{1}.json'.format(googleid, filename)
    s3object = bucket.Object(uri)
    file_stream = io.BytesIO()
    s3object.download_fileobj(file_stream)
    parsedDesign = json.loads(file_stream.getvalue())
    
    for i in range(len(parsedDesign["LINES"])):
        for k, v in parsedDesign["LINES"][i].items():
            for j, coord in enumerate(v):
                parsedDesign["LINES"][i][k][j] = float(coord)

    return parsedDesign

if __name__ == "__main__":
    filename = argv[1]
    googleid = argv[2]
    print("Opening " + str(filename))
    sys.stdout.flush()
    points = handleInput(filename, googleid)
    if (filename[-4:] != '.dxf') and (filename[-4:] != '.e57'):
        np.save('../uploaded_files/'+filename, points)
    print("Done")
