/**
 * annotationHelper.js
 * 4Di
 * Created by Peter Hart
 */
var flTool;
var flAnnot = [];
var flInfo={};
var lnGrp = [];
var rawLn = [];
var rawReg = [];
var flpdf;
var undoStck = [];
var undone = [];
//axisY = 85;
//axisX = 35;
/*
 * Draws over the scan based on the mouseEvent, 
 * annotation tool, color, and size the user selected
 */
function annotate(event, down){
	console.log(annotations);

	var rect = canvas.getBoundingClientRect();
    let xMouse = event.clientX - rect.left;
    let yMouse = event.clientY - rect.top + 0.5;

    if(!inBounds(xMouse, yMouse)){
    	return;
    }

	let tool  = document.getElementById("tool" ).value;
    let color = document.getElementById("color").value;
    let size  = document.getElementById("size" ).value;

    ctx.beginPath();
    ctx.lineWidth = size;
    ctx.strokeStyle = "rgb(" + color + ")";
	ctx.fillStyle = "black";
    let figure = new Figure(tool, size, color);

    switch(tool) {
      case "point":
      	if(down){
        	figure.text = pointIndex.toString();
            pointIndex ++;

        ctx.fillText(figure.text, xMouse + 3, yMouse - 2);
           	ctx.arc(xMouse, yMouse, 2, 0, 2 * Math.PI);
        	figure.shape = [xMouse, yMouse]
          }
          else return;
      break;
      case "rect":
      		if(down){
            	startX = xMouse;
                startY = yMouse;
                return;
            }
      		else{
                ctx.rect(startX, startY, xMouse - startX, yMouse - startY);
                figure.shape = [startX, startY, xMouse - startX, yMouse - startY];
            }

    break;
    case "line":
      		if(down){
            	startX = xMouse;
                startY = yMouse;
                return;
            }
      		else{
            	ctx.moveTo(startX, startY);
                ctx.lineTo(xMouse, yMouse);
		
                figure.text =  getDistance(xMouse, yMouse, startX, startY).toFixed(3);
                ctx.fillText(figure.text, (startX + xMouse)/2, (startY + yMouse)/2 );
                figure.shape = [startX, startY, xMouse, yMouse];
            }
    break;
    case "pen":
      	if(down){
            linePath.push(xMouse, yMouse);
        	toolState = "pen";
        	ctx.beginPath();
            ctx.moveTo(xMouse, yMouse);
    		ctx.strokeStyle = color;
            return;
        }
        else {
        	toolState = "none";
            figure.shape = linePath;
        }
      break;
      case "closed":
      	if(down){
            linePath.push(xMouse, yMouse);
        	toolState = "pen";
            ctx.beginPath();
            ctx.moveTo(xMouse, yMouse);
    		ctx.strokeStyle = color;
            return;
        }
        else {
        	ctx.moveTo(xMouse, yMouse);
        	ctx.lineTo(linePath[0], linePath[1]);
        	toolState = "none";
            linePath.push(linePath[0], linePath[1]);
            figure.type = "pen";
            figure.shape = linePath;
        }
      break;
    }
    ctx.stroke();
    annotations.push(figure);
    linePath = [];
    ctx.stroke();
}
/**
 * Removes one or all of the annotations from the list, 
 * clears the scan window and redraws any remaining annotations (if any exist)
 */
function undo(clear){
	ctx.beginPath();
	ctx.fillStyle = "white";

	ctx.fillRect(0, titleHeight, width, height - titleHeight - subtitleHeight);
	ctx.drawImage(reportColored, axisX, axisY,axisWidth, axisHeight);
ctx.lineWidth = 1;
		
	ctx.strokeStyle = "black";
	ctx.rect(axisX,axisY,axisWidth,axisHeight);
	ctx.fillStyle = "black";
	drawColorBar(ctx);
	drawAxis(reportList[reportIndex]['bounds']["units"], ctx);
	ctx.stroke();
    if(clear){
	pointIndex = 1;
    	annotations = []
    }
    else{
    	if(annotations.pop().type == "point"){
		pointIndex -= 1;
	}
        reAnotate();
    }
}

function flUndo(rotate){
	flctx.beginPath();
	flctx.fillStyle = "white";
	console.log(axisY);
	flctx.fillRect(0, titleHeight, width, height - titleHeight - subtitleHeight);
	flctx.drawImage(reportColored, axisX, axisY, axisWidth, axisHeight);
	flctx.lineWidth = 1;
		
	flctx.strokeStyle = "black";
	flctx.rect(axisX,axisY,axisWidth,axisHeight);
	flctx.fillStyle = "black";
	drawColorBar(flctx);
	drawAxis(reportList[reportIndex]['bounds']["units"], flctx);
	flctx.stroke();
	console.log(rawReg.length);	
	
	if(!rotate && undoStck[(undoStck.length-1)] == 'ln'){
            //lnGrp.pop();
            //rawLn.pop();
            //undoStck.pop();
	    undone.push([lnGrp.pop(), rawLn.pop(), undoStck.pop()]);
          }else if(!rotate && undoStck[(undoStck.length-1)] == 'reg'){
            //flAnnot.pop();
            //rawReg.pop();
            //undoStck.pop();
	    undone.push([flAnnot.pop(), rawReg.pop(), undoStck.pop()]);
           }
           console.log(undoStck);
					 
	if (!rotate && lnGrp.length > 0) {
		$("#startlnx").val(lnGrp[lnGrp.length-1]["start"][0]);
		$("#startlny").val(lnGrp[lnGrp.length-1]["start"][1]);
		$("#endlnx").val(lnGrp[lnGrp.length-1]["end"][0]);
		$("#endlny").val(lnGrp[lnGrp.length-1]["end"][1]);
	}
					 
	 if(rawLn.length > 0){
	  for(var i=0;i<rawLn.length;i++){
		//console.log("Drawing region ", i);
		flctx.moveTo(rawLn[i][0], rawLn[i][1]);
		console.log(lnGrp[i].start);
		console.log(lnGrp[i].end);
		flctx.lineTo(rawLn[i][2], rawLn[i][3]);
		flctx.stroke();
	    }
	}
	if(rawReg.length > 0){
	     for(var i=0;i<rawReg.length;i++){
	      if(rawReg[i][4] == 0){	
		flctx.rect(rawReg[i][0], rawReg[i][1], rawReg[i][2], rawReg[i][3]);
		flctx.stroke();
	      }else{
		drawRotatedRect(rawReg[i][0], rawReg[i][1], rawReg[i][2], rawReg[i][3], parseInt(rawReg[i][4]));
	      }
	    }
	}
    
}


function drawRotatedRect(x, y, width, height, degrees){
    console.log("In rotation function");
    //Save the unrotated context
    flctx.save();

    //flctx.beginPath();
    // move the rotation point to the center of the rect
    //flctx.translate(x + width / 2, y + height / 2);
    flctx.translate(x, y);
    // rotate the rect
    flctx.rotate(degrees * Math.PI / 180);

    // draw the rect on the transformed context
    // Note: after transforming [0,0] is visually [x,y]
    //       so the rect needs to be offset accordingly when drawn
    //flctx.rect(-width / 2, -height / 2, width, height);
    flctx.rect(0, 0, width, height);
    flctx.stroke();
    // restore the context to its untranslated/unrotated state
    flctx.restore();
    //flctx.rotate((360-degrees) * Math.PI/ 180);
}



/**
 * called from index.html, this adds to the pen path if the user is drawing.
 * Also updates the displayed coordinates with call to showCoords(event)
 */

function mouseMove(event, id, canv, canvOff){
    let color = document.getElementById("color").value;
    var rect = id.getBoundingClientRect();
    let xMouse = event.clientX - rect.left;
    let yMouse = event.clientY - rect.top + 0.5;
    if(inBounds(xMouse, yMouse)){
	if(toolState == "pen"){
    	linePath.push(xMouse, yMouse);
    	canv.lineTo(xMouse, yMouse);
        canv.stroke();
		}
    }
	showCoords(event, id, canvOff);
}
/**
 * Called from undo(), this redraws all of the annotations (Object 'figure') 
 * in the list 'annotations'
 */
function reAnotate(){
	for(let i = 0; i < annotations.length; i++){
    	let figure = annotations[i];
        ctx.beginPath();

        ctx.strokeStyle = "rgb(" + figure.color + ")";
        ctx.lineWidth = figure.size;
	ctx.fillStyle = "black";
        let text = figure.text;
        let shape = figure.shape;

    	switch(figure.type){
          case "point":
              ctx.arc(shape[0], shape[1], 2, 0, 2 * Math.PI);
              ctx.fillText(text, shape[0] + 3, shape[1] - 2);
          break;
          case "rect":
              ctx.rect(shape[0], shape[1], shape[2], shape[3]);
          break;
          case "line":
              ctx.moveTo(shape[0], shape[1]);
              ctx.fillText(text, (shape[0] + shape[2])/2, (shape[1] + shape[3])/2 );

              ctx.lineTo(shape[2], shape[3]);
          break;
		case "pen":
              ctx.moveTo(shape[0], shape[1]);
              for(let j = 2; j < shape.length; j += 2){
                  ctx.lineTo(shape[j], shape[j + 1]);
              }
          break;

        }
        ctx.stroke();
        ctx.stroke();
        ctx.stroke();
    }

}
/**
 * structure for storing the fields that describe each type of annotation and its parameters
 */
function Figure(type, size, color) {
   this.type  = type; //"point", "rect", "pen"...
   this.size  = size; //StrokeStyle, based on slider value at time of creation
   this.color = color;
   this.text  = ""; //Point index, line distance
   this.shape = []; //Coords for point, path of pen... Describes shape
}

function annotatefl(event, down){
	console.log(annotations);

	var rect = flCanv.getBoundingClientRect();
    let xMouse = event.clientX - rect.left;
    let yMouse = event.clientY - rect.top + 0.5;

    if(!inBounds(xMouse, yMouse)){
    	return;
    }

	let tool  = flTool
    //let color = document.getElementById("color").value;
    //let size  = document.getElementById("size" ).value;

    flctx.beginPath();
    //ctx.lineWidth = size;
    //ctx.strokeStyle = "rgb(" + color + ")";
	flctx.fillStyle = "black";
    let figure = new Figure(tool);

    switch(tool) {
      case "rect":
      		if(down){
            	startX = xMouse;
                startY = yMouse;
                return;
            }
      		else{
                var trueX;
		var trueY;
		var ht= yMouse-startY
		document.getElementById("angleIn").value = 0;	
		flctx.rect(startX, startY, xMouse - startX, yMouse - startY);
		figure.shape = [startX, startY, xMouse - startX, yMouse - startY];
                if(xMouse < startX){trueX=xMouse}else{trueX=startX};
		if(yMouse > startY){trueY=yMouse; ht=ht*-1}else{trueY=startY}
		rawReg.push([trueX, trueY, Math.abs(xMouse-startX), ht, 0]);
		let rectOrigin = getCoordsStr(trueX, trueY, flctxOff);
                let ori = rectOrigin.split(', ').map(Number);
		if(ori.length > 2){ori.pop()};
		let rectW = getDistance(startX, startY, xMouse, startY);
                let rectL = getDistance(startX, startY, startX, yMouse);
                //if(xMouse < startX){rectW = (rectW * -1)};
                //if(yMouse > startY){rectL = (rectL * -1)};
		var obj={
                    origin: [ori[1],ori[0]],
                    wh: [rectW,rectL],
                    angle: 0
                }
                flAnnot.push(obj);
		undoStck.push('reg');
                console.log(JSON.stringify(flAnnot));
            }

    break;
    case "line":
      		if(down){
            	startX = xMouse;
                startY = yMouse;
                return;
            }
      		else{
            	flctx.moveTo(startX, startY);
                flctx.lineTo(xMouse, yMouse);
                rawLn.push([startX, startY, xMouse, yMouse]);
		let lnStart = getCoordsStr(startX, startY, flctxOff);
                let lns = lnStart.split(', ').map(Number);
		if(lns.length > 2){lns.pop()};
		console.log(lnStart);
		console.log(lns);	
			
		let lnEnd = getCoordsStr(xMouse, yMouse, flctxOff)
                let lne = lnEnd.split(', ').map(Number);
		if(lne.length > 2){lne.pop()};
		//figure.text =  getDistance(xMouse, yMouse, startX, startY).toFixed(3);
                //flctx.fillText(figure.text, (startX + xMouse)/2, (startY + yMouse)/2 );
                figure.shape = [startX, startY, xMouse, yMouse];
                var obj = {
			start:[lns[1],lns[0]],
			end:[lne[1],lne[0]]
		};
                //obj.start[0]= lns[0];
		//obj.start[1]= lns[1];
                 //   end: "[" + lnEnd.slice(0,(lnEnd.lastIndexOf(","))) + "]"
               // }
								$("#startlnx").val(lns[1]);
								$("#startlny").val(lns[0]);
								$("#endlnx").val(lne[1]);
								$("#endlny").val(lne[0]);
                lnGrp.push(obj);
                undoStck.push('ln');
		console.log(JSON.stringify(lnGrp));
            }
    break;
    }
    flctx.stroke();
    //annotations.push(figure);
    linePath = [];
    flctx.stroke();
}

function changeCurrentLine(startX, startY, endX, endY) {
	
	if (rawLn.length==0) return false;
	if (
		startX < xMin || startX > xMax ||
		startY < yMin || startY > yMax ||
		endX < xMin || endX > xMax ||
		endY < yMin || endY > yMax
	) return false;
	
	lnGrp[lnGrp.length-1] = {
		start: [startX, startY],
		end: [endX, endY]
	}
	let lns = fromCoords(startX, startY)
	let lne = fromCoords(endX, endY)
	rawLn[rawLn.length-1] = [lns[0], lns[1], lne[0], lne[1]]
	
	flctx.beginPath();
	flctx.fillStyle = "white";
	console.log(axisY);
	flctx.fillRect(0, titleHeight, width, height - titleHeight - subtitleHeight);
	flctx.drawImage(reportColored, axisX, axisY, axisWidth, axisHeight);
	flctx.lineWidth = 1;
	
	flctx.strokeStyle = "black";
	flctx.rect(axisX,axisY,axisWidth,axisHeight);
	flctx.fillStyle = "black";
	drawColorBar(flctx);
	drawAxis(reportList[reportIndex]['bounds']["units"], flctx);
	flctx.stroke();
	
	if(rawLn.length > 0){
	  for(var i=0;i<rawLn.length;i++){
			flctx.moveTo(rawLn[i][0], rawLn[i][1]);
			console.log(lnGrp[i].start);
			console.log(lnGrp[i].end);
			flctx.lineTo(rawLn[i][2], rawLn[i][3]);
			flctx.stroke();
		}
	}
	if(rawReg.length > 0){
		for(var i=0;i<rawReg.length;i++){
		if(rawReg[i][4] == 0){
			flctx.rect(rawReg[i][0], rawReg[i][1], rawReg[i][2], rawReg[i][3]);
			flctx.stroke();
		}else{
			drawRotatedRect(rawReg[i][0], rawReg[i][1], rawReg[i][2], rawReg[i][3], parseInt(rawReg[i][4]));
		}
		}
	}
}

function updateCurrentLine() {
	changeCurrentLine(
		parseFloat($("#startlnx").val()), parseFloat($("#startlny").val()),
		parseFloat($("#endlnx").val()), parseFloat($("#endlny").val()),
	)
}

defLine.addEventListener("click", function(){
	flTool = "line";
});

defReg.addEventListener("click", function(){
	flTool = "rect";
});

undofl.addEventListener("click", function(){
	flUndo(false);
});

redofl.addEventListener("click", function(){
	var redo = [];
	if(undone.length > 0){
	    redo = undone.pop();
	    if(redo[2] == 'reg'){
		flAnnot.push(redo[0]);
		rawReg.push(redo[1]);
		undoStck.push(redo[2]);
	    }else{
		lnGrp.push(redo[0]);
		rawLn.push(redo[1]);
		undoStck.push(redo[2]);
	    }
	    flUndo(true);
	}
});

rotReg.addEventListener("click", function(){
	var angVal = parseInt(document.getElementById("angleIn").value);
	var arrLen = rawReg.length - 1;
	rawReg[arrLen][4] = angVal;
	flAnnot[arrLen].angle = angVal * -1;
	console.log(angVal);
	flUndo(true);
	//drawRotatedRect(rawReg[arrLen][0], rawReg[arrLen][1], rawReg[arrLen][2], rawReg[arrLen][3], parseInt(rawReg[arrLen][4]));
});


shareEml.addEventListener("click", function(){
	useReport();
	window.open("mailto:?subject=FFFL Report " + reportList[reportIndex]['filename'] + "&body=" + encodeURIComponent(flpdf));
});

/*cpyLnk.addEventListener("click", function(){
	var dummy = $('<input>').val(flpdf).appendTo('body').select();
	document.execCommand('copy');
	alert("URL copied to the clipboard");
});*/

genfffl.addEventListener("click", function(){
	var lnsAdded = false;
	/*
	var arr = [];
   	var obj = {
        maxz: zMax,
        minz: zMin,
        maxx: xMax,
        minx: xMin,
        maxy: yMax,
        miny: yMin,
        sizex: reportList[reportIndex]['bounds']['sizex'],
        sizey: reportList[reportIndex]['bounds']['sizey'],
        units: units
    }
    arr.push(obj);
    var regStr;
    if(flAnnot.length ==1){
        regStr = JSON.stringify(flAnnot).slice(1,(JSON.stringify(flAnnot).lastIndexOf("]")));
    }else{
        regStr = JSON.stringify(flAnnot);
    }
    let annotStr = '{"googleID":"' + userID + '","resultID":"' + reportList[reportIndex]['reportID'] + '","bounds":' + JSON.stringify(arr).slice(1,(JSON.stringify(arr).lastIndexOf("]"))) + ',"region":' + regStr + '}';
   */
	flInfo = {
        googleID: userID,
        resultID: reportList[reportIndex]['reportID'],
	filename: reportList[reportIndex]['filename'],
	design: reportList[reportIndex]['design'],
	minff: $("#minff").val(),
	minfl: $("#minfl").val(),
	specff: $("#specff").val(),
	specfl: $("#specfl").val(),
	spacing: $("#spacing").val(),
	errThr: reportList[reportIndex]['errorThreshold'],
	plnHt: reportList[reportIndex]['planeHeight'],
	plnThr: reportList[reportIndex]['planeThreshold']
    };
    flInfo.bounds = {
        maxz: zMax,
        minz: zMin,
        maxx: xMax,
        minx: xMin,
        maxy: yMax,
        miny: yMin,
        sizex: reportList[reportIndex]['bounds']['sizex'],
        sizey: reportList[reportIndex]['bounds']['sizey'],
        units: units
    }
    if(relative == true){
       flInfo.bounds.minz += planeHeight;
        flInfo.bounds.maxz += planeHeight;
   }
    if(lnGrp.length){
	flAnnot.push(lnGrp);
	lnsAdded = true;
    }
    flInfo.region = flAnnot;
     console.log(JSON.stringify(flInfo));
     console.log(JSON.stringify(rawLn));

    $.ajax({
        type: "POST",
        url: "/genfffl",
	contentType: "application/json",
        data: JSON.stringify(flInfo),
        success: function (data) {
            //Display returned pdf
	    PDFObject.embed(data, "#flpdf-div");
	    flpdf = data;
	    document.getElementById("save_flreport").disabled=false;
        },
        error: function (e) {
            console.log("ERROR : ", e);
            			
        }
    });

    //If the array of lines was added to the annotation array, remove it to prevent muultiple line arrays
    if(lnsAdded){
        flAnnot.pop();
        lnsAdded = false;
    }

});



