/**
 * Annotator.js
 * 4Di
 * Created by Mica Lewis
 */
 
 class Annotator {
	 
	constructor (reportDisplay) {
		this.reportDisplay = reportDisplay;
		this.reportDisplay.onReset(function (){
			savedPoints = [];
			pointIndex  = 1;
			toolState   = "none";
			linePath    = [];
			annotations = [];
		});
	}
	
	
 }