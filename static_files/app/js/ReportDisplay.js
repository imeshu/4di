/**
 * ReportDisplay.js
 * 4Di
 * Created by Mica Lewis
 */
 
 class ReportDisplay {
	
	constructor (canvas, canvoff) {
		this.canvas = canvas;
		this.canvoff = canvoff;
		this.resetCallbacks = [];
	}
	
	onReset(callback) {
		this.resetCallbacks.push(callback)
	}
	
	resetCanvas(){
		
		this.resetCallbacks.forEach()
		
		barWidth   = 20;
		barY       = axisY;
		axisHeight = height - titleHeight - subtitleHeight - 2 * margin;
		barHeight  = axisHeight;
		axisWidth  = width - 2 * margin - barWidth - 70;
		barX       = axisX + axisWidth + margin;
		colors = [];	
		for (let i = rgbGrd.length-1; i >= 0; i--){
			colors.push('rgb(' + rgbGrd[i][0] + ',' + rgbGrd[i][1] + ',' + rgbGrd[i][2] + ')');
		}
		grd = canv.createLinearGradient(0, barY, 0, barY + barHeight);
		for(i = 0; i < colors.length; i++){
			grd.addColorStop((i)/(colors.length -1), colors[i]);
		}
		document.getElementById("point").innerHTML ="Move mouse over scan to see height values.<br>Click to save a point in the form: (ID), Y, X, Z"; 
		canv.clearRect(0,0,width,height);
		canv.fillStyle = "white";
		canv.fillRect(0,0,width,height);
		canv.beginPath();
		enableButtons(true);
	}
 }