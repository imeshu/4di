
            function onComplete (file) {
            	console.log("SUCCESS : ", file.name);
            	var file_type = file.name.split(".");
            	file_type = file_type[file_type.length-1].toLowerCase();
            	console.log( file_type );
            	$("#filename-"+file_type).prop("value", file.name);
            	$.ajax({
            		url: '/uploads3/',
            		data: {filename: file.name},
            		type: 'GET',
			xhrFields: {
				withCredentials: true
			},
            		success: function(response) {
            			console.log("Worked!");
            		},
            		error: function(response) {
            			console.log("Error!");
            		}
            	});
            }
            
            var uploadURI;
            
            Dropzone.autoDiscover = false;
            
            $(document).ready(function(){

		    //Retrieves config info from index.js to construct box sign-in link and the dropzone upload url
		    $.ajax({
			method: "GET",
			url: "/getboxlink",
			xhrFields: {
			    withCredentials: true
			},
			success: function(data){
			    document.getElementById("box_sign_in_option").innerHTML = '<a href="https://api.box.com/oauth2/authorize?client_id='+ data["client"][0] +'&response_type=code" id="box_sign_in" class="login_link">Sign in with Box</a>'
			    uploadURI = data["upload"][0]
			    
			}
		    }).then( function() {
			var drpzn = document.getElementById('drop-upload-dxf');
			drpzn.setAttribute('action', 'uploadURI');
			    
			//Initializes and sets parameters from Dropzone
			$('#drop-upload-dxf').dropzone( {
		    
				init: function () {
					this.on("complete", onComplete);

				},
				
				url: uploadURI,
				paramName: "file",
				maxFiles: 10,
				dictDefaultMessage: "Drag a file here to upload, or click to select one",
				maxFilesize: 50000,
				timeout: 0,
		    
			});
		    });
            	$('#out-images').carousel();
            	$('[data-toggle="popover"]').popover(); 
                
        //Adds search feature to the respective dropdown menus
		$('#e57-dropdown').select2({
			width: '80%'
		});
		$('#dxf-dropdown').select2({
			width: '80%'
		}); 
        $('#pdf-dropdown').select2({
			width: '80%'
		});
            
            });
            
            //Sends a request to the node server to begin the python process to generate a report
            function generate() {
            	
              event.preventDefault();
              var formElement = document.querySelector("#generate");
              var data = new FormData(formElement);
            	
              if(parseFloat($("#threshold").val()) < parseFloat($("#ignore").val())){
                    alert("ERROR: Error Threshold cannot be greater than Plane Threshold. Thresh: " + $("#threshold").val() + "Error: " + $("#ignore").val())
              }else{
                
            	console.log("sending: ");
            	for (var value of data.values()) {
            		console.log(value); 
            	}
            	
            	
            	$.ajax({
            		type: "POST",
            		url: "/node/generate",
            		data: data,
            		processData: false,
            		contentType: false,
            		cache: false,
            		timeout: 0,
                    xhrFields: {
                        withCredentials: true
                        },
            		success: function (data) {
            
            			pollprogress(data)
            
            		},
            		error: function (e) {
            
            			$("#result").text(e.responseText);
            			console.log("ERROR : ", e);
            			
            		}
            	});
              }
            };
        
        //Retrieves the line data of the dxf and calls for it to drawn on the canvas
	    function getdxfjson(filename){ 
		    console.log("getting: " + filename);
		    $.ajax({
			    type:"GET",
			    url: "/canvas/getJSON/",
			    data:"filename="+ filename,
			    xhrFields: {
                    withCredentials: true
                            },
			    success: function (data) {
				console.log("DXF Lines: " + data);
				drawDXFLines(data, ctx);
                drawDXFLines(data, flctx);

			    },
			    error: function (e) {
			    	console.log("ERROR : ", e);
			    }
		    });
	    };
			//Updates a progress bar with the percentage of a file uploaded from box
            function pollboxprogress( runID ) {
            	$.ajax({
            		type:"GET",
            		url: "/node/pollboxprogress/",
            		data: runID,
            		success: function (data) {
            			if (data == "100%") {
                                            $("#boxprog").attr({
                                                    "aria-valuenow" : data.substring(0, data.length-1),
                                                    "style" : "width:" + data
                                            });
                                            $("#boxprog").text(data);
            				console.log("Done! Data at 100%");
            			} else if (data.endsWith("%")) {
            				$("#boxprog").attr({
            					"aria-valuenow" : data.substring(0, data.length-1),
            					"style" : "width:" + data
            				});
            				$("#boxprog").text(data);
            				console.log("Download progress:" + data);
            				setTimeout(function() { pollboxprogress(runID) }, 1000);
            			} else {
            				setTimeout(function() { pollboxprogress(runID) }, 1000);
            			}
            		}		
            	});
            }
            
            
            function pollprogress( resultID ) {
            	
            	$.ajax({
            		type:"GET",
            		url: "/node/pollprogress/",
            		data: resultID,
            		success: function (data) {
            		
            			res = data.split(",");
            			
            			if (res[0] == "") {
            				
            				setTimeout(function() { pollprogress(resultID) }, 1000)
            
            			} else if (res[0] == "NONE") {
            			
            				setTimeout(function(){ pollprogress(resultID) }, 1000);
            			
            			} else if ( res[0] == "ERR" ) {
            				
            				var markup = "<tr style='color:red;'><th></th><th></th><th>" + data.substring(4) + "</th></tr>"
            				$("#out-table-body").prepend(markup);
            				
            			} else if (res[0] != "DONE") {
            			
            				var markup = "<tr><th>" + res[0] + "</th><th>" + res[1] + "</th><th>" + res[2] + "</th></tr>";
            				$("#out-table-body").prepend(markup);
					console.log(res)
            				setTimeout(function(){ pollprogress(resultID) }, 1000);
            				
            			} else {
					data = JSON.parse( data.slice(5, data.length)); 
            				reportList.push(data);
					reportIndex = reportList.length - 1;
            				drawCanvas(data, flctx, flctxOff);
					drawCanvas(data, ctx, ctxOff);
            			}
            		},
            		
            	})
            	
            	
            	
            }
	function disable(r){
	var t=r.form['height'];
	r.value=='auto'?t.setAttribute('disabled','disabled'):t.removeAttribute('disabled');
	}
    
    window.onload = function() {
              fileReload();
         };
    
    
    function fileReload(){
        var reload = setInterval(getList, 5000)
    }
    
    //Retrieves a JSON of all e57 and dxf filenames and populates the lists on the Upload tab of the site
    function getList(){
        $.ajax({
        method: "GET",
        url: "/lists3",
	xhrFields: {
              withCredentials: true
        },
        success: function(jsonx){
            popList("e57-list", jsonx["pcl"]);
            popList("dxf-list", jsonx["dxf"]);
        }
        })
    }
    
    //Populates a list with items given a list name and a json of item names
    function popList(list, items){
        const data = items;
        var id = document.getElementById(list);
        while (id.firstChild) {
            id.removeChild(id.firstChild);
        }
        
        for (var i = 0; i < data.length; i++) {
            elem = document.createElement('LI');
            textElem = document.createTextNode(data[i]);
            elem.appendChild(textElem);
            id.appendChild(elem);;
        }
    }

    function dboxUp(files){
        //var url = $("#dbLink").val();
	//var dbName = $("#dbName").val();
	var dbfiles = JSON.stringify(files);
	alert("File Upload Initiated");
	$.ajax({
        type: "POST",
        url: "/dbupload",
        data: {"uploads":dbfiles},
        success: function (data) {
           // alert("File Uploaded Sucessfully");
        },
        error: function (e) {
            alert("File Upload Failed");
	    console.log("ERROR : ", e);
            			
        }
    });	
    }


function purchReps(){
    var repQuant = $("#repDrop option:selected").val();
    if (confirm("Would you like to purchase " + repQuant + " reports for $" + (repQuant*100) + "?")) {
   	//Run
	$.ajax({
        type: "POST",
        url: "/purchreps",
        data: {"reps":repQuant},
        success: function (data) {
            alert("Report Purchase Sucessful");
	    getRepsLeft();
        },
        error: function (e) {
            alert("Report Purchase Failed");
	    console.log("ERROR : ", e);
            			
        }
    });	
    }else{
  	
    } 
}



function getRepsLeft(){
    $.ajax({
    method: "GET",
    url: "/repsleft",
	xhrFields: {
        withCredentials: true
    },
    success: function(response){
        $("#repsLeft").text(response);
	$("#repsLeft2").text(response);
    },
    error: function(e){
	console.log("Report remaining error: ", e);
    }
    })
}


//Makes call to decrement user's reports by 1
function useReport(type){
    
    $.ajax({
    method: "GET",
    url: "/usereport",
	xhrFields: {
        withCredentials: true
    },
    success: function(response){
        getRepsLeft();
	//return true;
	if(type == 'topo'){saveTopo(true)}
	else{saveFFFL(true)}
    },
    error: function(e){
        //alert(e);
	//return false;
	if(type == 'topo'){saveTopo(false)}
	else{saveFFFL(false)}
    }
    })
   
}
/*
resetFields.addEventListener("click", function(){
	resetFields();
});

function resetFields(){
    $("#outUnits").val("m");
    $("#threshold").val('.075');
    $("#ignore").val('0');
    $("#height").val('');
    $("#auto").prop("checked", true);
    $("#genpts").prop("checked", false);
    $("#scaleh").prop("checked", true);
    $("#samples").val('140');
    $("#culling").val('100');
    $("#resolution").val('500');
}
*/
//var button = Dropbox.createChooseButton(droptions);
//document.getElementById("dropCont").appendChild(button);


droptions = {

    // Required. Called when a user selects an item in the Chooser.
    success: function(files) {
        
	dboxUp(files);
    },
    linkType: "direct",
    multiselect: true,
    extensions: ['.dxf', '.e57', '.rar', '.zip', '.pts']
};


var button = Dropbox.createChooseButton(droptions);
document.getElementById("dropCont").appendChild(button);
