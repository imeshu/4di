//File Dropdowns

//Refreshes the lists of e57 and dxf files in S3 and repopulates the respective dropdowns
function refreshOptions(){
$.ajax({
    method: "GET",
    url: "/lists3",
    xhrFields: {
        withCredentials: true
    },
    success: function(jsonx){
        console.log(jsonx)
        popDrop(e57dropdown, jsonx["pcl"], 'refreshOptions()')
        popDrop(dxfdropdown, jsonx["dxf"], 'refreshOptions()')
    }
})
}

var e57dropdown = document.getElementById('e57-dropdown');
var dxfdropdown = document.getElementById('dxf-dropdown');
e57dropdown.length = 0;
dxfdropdown.length = 0;

option = document.createElement('option');
option.text = "none";
option.value = "none";
dxfdropdown.add(option);


const defaultOption = document.createElement('option');
defaultOption.text = 'Choose a File';

refreshOptions();

//Populates dropdown menus with options
function popDrop(id, choices, func) {
    const data = choices
    
    if(data.length <= 10){
        id.setAttribute('onselect2:opening', ('this.size=' + data.length + '; '+ func +';'))
    }else{
        id.setAttribute('onselect2:opening', ('this.size=' + 10 + '; '+ func +';'))
    }
      
    var option;
    for (var i = 0; i < data.length; i++) {
      var alreadyExist = false;
      //console.log(id.options);
      for (var j = 0; j < id.options.length; j++) {
          
          if (id[j].value == data[i]) {
              alreadyExist = true;
          }
      }
      if (alreadyExist) { continue }
      option = document.createElement('option');
      option.text = data[i];
      option.value = data[i];
      id.add(option);
    }
}



//Unit Dropdown
oldUnits='';

//Updates the labels for certain text fields as well as converts the numbers within to the seleected unit
function unitUpdate(){
    var currUn = $( "#outUnits option:selected" ).text();
    document.getElementById("planeThresh").innerHTML = "Plane Threshold (in " + currUn + ")";
    document.getElementById("errThresh").innerHTML = "Error Threshold (in " + currUn + ")";
    document.getElementById("plnht").innerHTML = "Plane Height (in " + currUn + ")";
    var height = $("#height").val();
    if(height == ''){
       height = 0;
    }
    //console.log($("#threshold").val());
    var convertStr = '0'+ $("#threshold").val() + '--' + $("#outUnits").val() + '--' + oldUnits + '--' + $("#ignore").val() + '--' + height;
    console.log(convertStr);
    oldUnits = $("#outUnits").val();
    $.ajax({
        type: "POST",
        url: "/node/convert",
        data: {"valArr":convertStr},
        success: function (data) {
           console.log(data.toString());
           var newUnits = data.split('--')
           var threshVal = parseFloat(newUnits[0]);
           var ignoreVal = parseFloat(newUnits[1]);
           var heightVal = parseFloat(newUnits[2]);
           $("#threshold").val(threshVal.toFixed(3))
           $("#ignore").val(ignoreVal.toFixed(3))
           $("#height").val(heightVal.toFixed(3))
        },
         
    });
    
}


//PDF Dropdown

var pdfdropdown = document.getElementById('pdf-dropdown');
pdfdropdown.length = 0;
refreshPdf();
var userID;
//Refreshes the list of pdf files in S3 and repopulates the respective dropdown
function refreshPdf(){
    console.log("refreshing pdf");
$.ajax({
    method: "GET",
    url: "/listpdf",
    xhrFields: {
        withCredentials: true
    },
    success: function(jsonx){
        console.log(jsonx)
        popDrop(pdfdropdown, jsonx["pdf"], 'refreshPdf()')
        userID = jsonx["user"][0];
    }
})
}

//Changes the embedded pdf on the Previous Reports page to match what the user has selected
var embedLink;
function pdfChange(){
    var reportID = $( "#pdf-dropdown option:selected" ).text();
    //var embedLink;
    if(reportID.substring(0,4) == "FFFL"){
	embedLink = "https://4dipdfs.s3.amazonaws.com/"+ userID +"/flreports/"+ reportID +".pdf"
    }else{
    	embedLink = "https://4dipdfs.s3.amazonaws.com/"+ userID +"/pdf/"+ reportID +".pdf"
    }
    PDFObject.embed(embedLink, "#pdf-div");
    console.log(embedLink);
}

//Triggers the refresh function for each dropdown when they are opened 
$('#pdf-dropdown').on('select2:opening', function (e) {
    refreshPdf();
});
$('#e57-dropdown').on('select2:opening', function (e) {
    refreshOptions();
});
$('#dxf-dropdown').on('select2:opening', function (e) {
    refreshOptions();
});

