/**
 * pdfHelper.js
 * 4Di
 * Created by Peter Hart and Rich Feimer
 */
var dateTime= (new Date()).toLocaleString('default', { hour12: false});
// Generates a pdf from the canvas
function generatePDF(report,canvOff){
	let fields = reportList[reportIndex];
	let imgData = report.toDataURL("image/jpeg", 1.0);	
    let logo = logo64;//encoded file located in img64.js
	/*
	var imgData = canvas.toDataURL("image/PNG", 1.0);
	var annots = overlay.toDataURL("image/PNG", 1.0);
*/
	var pdf = new jsPDF('l', 'mm',"a4");// [297, 210]);
	pdf.addImage(imgData, 'JPEG', 0, 15);
    pdf.addImage(logo, 'JPEG', 139, 138, 20, 20);
	pdf.setFontSize(15);
	var repTitle = $("#repTitle").text();
	if(repTitle.length > 35){
		pdf.text(2,10, (repTitle.substring(0,40)));
		pdf.text(2,18, (repTitle.substring(40, repTitle.length)));
	}else{pdf.text(2, 10, repTitle);}
//	pdf.addImage(annots, 'PNG', 0, 0);
	pdf.setFontSize(9);  
	
	pointList = [];
    
	for(i = 0; i < annotations.length; i++){
    		if(annotations[i].type == "point"){
        		pointList.push(annotations[i]);
		}
	}
	
	let designName =  fields["design"];
	if(designName.length > 30){
		designName = designName.substring(0,15) + "..." + designName.substring(designName.length - 15, designName.length);
	}
	var revArray = $("#revInfo").text().split(", ");
	var ypos=130;
	console.log(revArray[2]);
	for(var i=revArray.length-1;i>=0;i--){
		pdf.text(5, ypos, revArray[i]);
		ypos -= 5;
	}
	//pdf.text(5, 130, $("#revInfo").text());
	pdf.text(12, 145, "Design file: " + designName);
	pdf.text(12, 150, "Error Threshold: " + fields["errorThreshold"].toFixed(4));
	pdf.text(12, 155, "Plane Height: " + fields["planeHeight"].toFixed(4));
	pdf.text(12, 160, "Plane Threshold: " + fields["planeThreshold"].toFixed(4));
	let notes = document.getElementById("notes").value;
	let charWidth = 80;
	//TODO write or import word wrapping function. 
	//Either use monospace font or use jsPDF measureText function for line width
	for(let row = 0; row < notes.length/charWidth; row ++){
		pdf.text(12, 170 + row * 5, notes.substring(row*charWidth, (row+1)*charWidth));
	}
	pdf.rect(10, 165, 120, 35);
	
	//Prints the date and time at the bottom of the page
	pdf.text(10, 205, dateTime + " " + (Intl.DateTimeFormat().resolvedOptions().timeZone));

	let numRows = 40;//can display up to 80 point coordinates in two columns
	pdf.rect(165, 0, 60, 210); //left points
	pdf.rect(225, 0, 60, 210); //right points
	pdf.rect(0, 0, 159, 132); //report
	pdf.rect(0, 138, 159, 72);//bottom left
	pdf.text(170, 5, "ID, Y, X, Z");
	pdf.rect(10, 140, 80, 22);
	for(var i = 0; i < pointList.length; i++){
		if(i < numRows){
			pdf.text(170, 5*(i+2), i+1 + ", " +  getCoordsStr(pointList[i].shape[0], pointList[i].shape[1],canvOff));
		}
		else if(i < numRows * 2){
			pdf.text(235, 5*(i-numRows + 2), i+1 + ", " +  getCoordsStr(pointList[i].shape[0], pointList[i].shape[1],canvOff));
		}
	}
	return pdf;
}

//Sends pdf to EC2 to be saved on S3. Opens an email link if the user wants to share the pdf via email
function sendPDF(pdf64, sharing){
    var date = dateTime;
    var safeDate = date.replace(/\//g, "-").replace(', ', '_').replace(/:/g, "*");
    var safeName = safeDate + "_" + (reportList[reportIndex].filename)
    console.log("SafeName: " + safeName);
    $.ajax({
        type: "POST",
        url: "/sendpdf",
        data: {"pdfStr":pdf64,
               "pdfName": safeName},
        //data: pdf64,
        success: function (data) {
            if (sharing){
                console.log("PDF sent sucessfully");
                console.log(data);
                window.open("mailto:?subject=Surface Analysis Report " + safeName +"&body=" + encodeURIComponent(data));
            }else{
                alert("Report Saved Successfully");
            }
            
        },
        error: function (e) {
            console.log("ERROR : ", e);
            			
        }
    });
}

save_report.addEventListener("click", function() {
  	//console.log("share clicked");
	  if (confirm("Would you like to use 1 report credit to save this report? Saving reports allows you to share and download as well as view this report in the Previous Reports tab.")) {
 useReport();	
  function useReport(){
    $.ajax({
    method: "GET",
    url: "/usereport",
	xhrFields: {
        withCredentials: true
    },
    success: function(response){
	
	reportList[reportIndex]['purchased'] = true;
	enableButtonsPurch(false);
	let pdf = generatePDF(canvas, ctxOff);
	console.log("pdf created");
    var pdfb = pdf.output('blob');
    var fileReader = new FileReader();
            var base64;
            // On load of file, read the file content
            fileReader.onload = function(fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                //console.log(base64);
                sendPDF(base64, false);
            };
    
  	fileReader.readAsDataURL(pdfb);
	    getRepsLeft();
	},error: function(e){
	   alert("You are out of report credits. You can buy more in the Generate tab.")
        }
    
    })   
  }	}else{}
}, false);


function enableflButtonsPurch(enable){
	buttons = ["shareEml"];
	for(let i = 0; i < buttons.length; i++){
		document.getElementById(buttons[i]).disabled=enable;
	}
}



save_flreport.addEventListener("click", function() {
  	//console.log("share clicked");
	  if (confirm("Would you like to use 1 report credit to save this report? Saving reports allows you to share as well as view this report in the Previous Reports tab whenever you'd like.")) {
 useReport();	
  function useReport(){
    $.ajax({
    method: "GET",
    url: "/usereport",
	xhrFields: {
        withCredentials: true
    },
    success: function(response){
	
	enableflButtonsPurch(false);
     $.ajax({
        type: "POST",
        url: "/ffflpurch",
        data: {"link":flpdf},
        success: function (data) {
           // alert("File Uploaded Sucessfully");
	   alert("Report Saved Sucessfully");
        },
        error: function (e) {
            alert("FFFL Report Purchase Failed. Please contact support.");
	    console.log("ERROR : ", e);
            			
        }
     });
	getRepsLeft();
	},error: function(e){
	   alert("You are out of report credits. You can buy more in the Generate tab.")
        }
    
    })   
  }	}else{}
}, false);



//Allows the user to share a previously generated pdf from the previous reports page
shareReps.addEventListener("click", function() {
    var reportID = $( "#pdf-dropdown option:selected" ).text();
    //var link = "https://4dipdfs.s3.amazonaws.com/"+ userID +"/pdf/"+ reportID +".pdf"
    var link;
    if(reportID.substring(0,4) == "FFFL"){
	link = "https://4dipdfs.s3.amazonaws.com/"+ userID +"/flreports/"+ encodeURIComponent(reportID) +".pdf"
    }else{
    	link = "https://4dipdfs.s3.amazonaws.com/"+ userID +"/pdf/"+ encodeURIComponent(reportID) +".pdf"
    }
    //link = encodeURIComponent(link).replace(' ', '+');
    //console.log(link);
    window.open("mailto:?subject=Surface Analysis Report " + reportID +"&body=" + encodeURIComponent(link));   
}, false);
	
share.addEventListener("click", function() {
	let pdf = generatePDF(canvas, ctxOff);
	console.log("pdf created");
    var pdfb = pdf.output('blob');
    var fileReader = new FileReader();
            var base64;	
	//On load of file, read the file content
            fileReader.onload = function(fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                //console.log(base64);
                sendPDF(base64, true);
            };
    
  	fileReader.readAsDataURL(pdfb);
 
}, false);
