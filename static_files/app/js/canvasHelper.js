/**
 * canvasHelper.js
 * 4Di
 * Created by Peter Hart
 */

const canvas = document.getElementById('report');
const flCanv = document.getElementById('flreport');
const ctx    = canvas.getContext("2d");
const flctx  = flCanv.getContext("2d");
const width  = canvas.width;
const height = canvas.height;
const flwidth  = flCanv.width;
const flheight = flCanv.height;
const off    = document.getElementById('offScreen');
const floff    = document.getElementById('flOffScrn');
const ctxOff = off.getContext("2d");
const flctxOff = floff.getContext("2d");

const titleHeight    = 50;
const subtitleHeight = 50;
const statsWidth     = 172;
const buffer         = 5;
const rowHeight      = 20;
const rgbGrd = [[0, 0, 255], [0, 255, 0], [255, 255, 0], [255, 165, 0], [255, 0, 0]]; 
const margin = 35;

let reportList = [];
let reportIndex = 0;
let reportWidthOrig;
let reportHeightOrig;

let grd;
let colors;
let planeHeight;
let errorThreshold;
let relative;
const axisX=50;
const axisY=35;
let axisWidth;
let axisHeight;
let barX;
let barY;
let barWidth; 
let barHeight;
let designFile = "none";
let fields;
let reportColored;
let savedPoints;
let startX;
let startY;

let toolState   = "none";
let linePath    = [];
let annotations = [];
let pointIndex  = 1;


/**
 * draws a report in the canvas based on the parameters in fields
 */
function drawCanvas(fields, canv, canvOff){
	//TODO Change clearing of raw arrays when canvases become unlinked
	flAnnot = [];
	lnGrp = [];
	rawReg = [];
	rawLn = [];
	console.log(fields);
	resetCanvas(canv);
	scaleReport(fields.bounds.sizex, fields.bounds.sizey, canv)
	console.log(axisY);
	console.log(axisX);
	updateParameters(fields, canv);
	drawReportColored(fields['image_color'], fields['bounds']['units'], canv);
	drawReportEnc(fields['image_enc'], canvOff);
	drawColorBar(canv);
	dxfName = fields["design"];
	if(dxfName != "none"){
		getdxfjson(fields["design"]);
	}
	enableButtons(false);
	if(reportList[reportIndex]['purchased']){enableButtonsPurch(false)}else{enableButtonsPurch(true)};
	canv.stroke();
	console.log(axisY);
	console.log(axisX);
}

/**
 *Rescales and draws the report image in canvas based on a scroll event
 */
function zoom(scroll){
	console.log("scrolling!");
	console.log(scroll);
}



/**
 * clears the canvas and resets dimensions to initial values
 */
function resetCanvas(canv){
	savedPoints = [];
	pointIndex  = 1;
	toolState   = "none";
	linePath    = [];
	annotations = [];
	
	pointIndex = 1;
	//axisX      = margin;
	//axisY      = titleHeight + margin;
	barWidth   = 20;
	barY       = axisY;
	axisHeight = height - titleHeight - subtitleHeight - 2 * margin;
	barHeight  = axisHeight;
	axisWidth  = width - 2 * margin - barWidth - 70;
	barX       = axisX + axisWidth + margin;
	colors = [];	
	for (let i = rgbGrd.length-1; i >= 0; i--){
		colors.push('rgb(' + rgbGrd[i][0] + ',' + rgbGrd[i][1] + ',' + rgbGrd[i][2] + ')');
	}
	grd = canv.createLinearGradient(0, barY, 0, barY + barHeight);
	for(i = 0; i < colors.length; i++){
		grd.addColorStop((i)/(colors.length -1), colors[i]);
	}
  	document.getElementById("point").innerHTML ="Move mouse over scan to see height values.<br>Click to save a point in the form: (ID), Y, X, Z"; 
	canv.clearRect(0,0,width,height);
	canv.fillStyle = "white";
	canv.fillRect(0,0,width,height);
	//canv.stroke();
	canv.beginPath();
	enableButtons(true);	

	//canv.clearRect(0, 0, width, height);
	//canv.fillStyle="white";
	//canv.fillRect(0, 0, width-1, height-1);
	//canv.fillStyle="black";
	//canv.rect(0, 0, width, height);
	//canv.beginPath();

}

/**
 * Stores parameter values from fields in global variables.
 * This is a fairly useless method since 
 * fields easily be acccessed with: 'reportList[reportIndex]'
*/
function updateParameters(fields, canv){
	relative = fields["relative"];
	errorThreshold = fields["errorThreshold"];
	planeHeight = fields["planeHeight"];
	planeThreshold = fields["planeThreshold"];
	drawTitle(fields['filename'], canv);
	designFile = fields['design'];
	drawReviewerInfo(fields["reviewer"], canv);
	units = fields['bounds']['units'];
	xMin = fields['bounds']['minx'];
	yMin = fields['bounds']['miny'];
	zMin = fields['bounds']['minz'];
	xMax = fields['bounds']['maxx'];
	yMax = fields['bounds']['maxy'];
	zMax = fields['bounds']['maxz'];
	let deltaZ = zMax - zMin;
    	
	if(relative){
		zMax = deltaZ/2;
		zMin = -1 * deltaZ/2
	}
}

/**
 * draws the report on the canvas and the axis box around it
 */
function drawReportColored(src, units, canv){
	reportColored = new Image();
	reportColored.crossOrigin = "Anonymous";
	reportColored.src = src;
	reportColored.onload = function(){
		canv.drawImage(reportColored, axisX, axisY, axisWidth, axisHeight);
		//scaleReport(reportColored, canv);
		drawAxis(units, canv);
		canv.stroke();
	}
}

/**
 * draws the RGB encoded image on the offscreen canvas
 * The offscreen encoded image is used by getHeight(x,y)
 */
function drawReportEnc(src, canvOff){
	report = new Image();
 	report.crossOrigin = "Anonymous";
	report.src = src;
	report.onload = function(){
		canvOff.drawImage(report, 0, 0, report.width, report.height);
	}
}

/**
 *enables or disables buttons for the canvas based on the boolean value of 'enable'
 */
function enableButtons(enable){
buttons = ["download_PDF",  "pts_button",/* "PNG_button",*/ "share", "save_report"];
	for(let i = 0; i < buttons.length; i++){
		document.getElementById(buttons[i]).disabled=enable;
	}
}

/*
 * Scales and draws the file name at the top of the canva report
 */
function drawTitle(title, canv) {
	if(canv == flctx){$("#flTitle").text(title);}else{$("#repTitle").text(title)}
	/*canv.fillStyle = "black";
	let fontSize = 30;
	canv.font = '30px serif';
	while(canv.measureText(title).width >= width - 15){
		fontSize -= 2;
		canv.font = fontSize + 'px serif';
	}
	canv.fillText(title, 10, 35);
  	canv.rect(0, 0, width, titleHeight);*/
}

/**
 * Writes the name, company and address of the user at the bottom left of the report canvas
 * TODO only draw the box if the user provides any of the fields
 */
function drawReviewerInfo(reviewer, canv){
	var container;
	if(canv == ctx){container = $("#revInfo")}else{container = $("#flRevInfo");}
	container.empty();	
  	if (reviewer["name"]    != "") container.append("Reviewer: " + reviewer["name"] + ", <br>");
  	if (reviewer["company"] != "") container.append("Company: " + reviewer["company"] + ", <br>");
  	if (reviewer["address"] != "") container.append("Address: " + reviewer["address"] + "<br>");
  	
}
/**
 * draws and labels the XY axis around the scan
 */
function drawAxis(units, canv){
	let ticPix = 30;
	let deltaY = yMax - yMin;
	let deltaX = xMax - xMin;
	canv.font = '10px serif';
	let fontSize = 10;
	while( canv.measureText(xMax.toFixed(1)).width > ticPix-5){
		fontSize -= 1;
		canv.font = fontSize + "px serif";
	}
	let x = axisX;
	let y = axisY + axisHeight;
	scaleText(xMax.toFixed(1), 10, ticPix - 5, canv);

	while(x < axisX + axisWidth){
    		canv.rect(x, axisY+axisHeight, 0,10);
		let label = ((deltaX/axisWidth)* (x - axisX) + xMin).toFixed(1).toString(); 
  		canv.fillText(label, x - canv.measureText(label).width/2, axisY + axisHeight + 20);	
		x += ticPix;
	}
	scaleText(yMax.toFixed(1), 10, 25, canv);

 	while(y > axisY){
		canv.rect(axisX - 10, y, 10, 0);
		let label = (  ( deltaY/axisHeight)*-1* (y - axisY) + yMax).toFixed(1).toString(); 
		canv.fillText(label , axisX - 10 - canv.measureText(label).width, y + 3);
		y -= ticPix;	
	}
	canv.rect(axisX, axisY, axisWidth, axisHeight);
	canv.font = '14px serif';
	let xLabel = "X position (" + units + ")";
	let yLabel = "Y position (" + units + ")";
	
	canv.fillText(xLabel, axisX + axisWidth / 2 - canv.measureText(xLabel).width / 2, axisY + axisHeight + 37);
	canv.rotate(3 * Math.PI / 2);
	canv.fillText(yLabel, -(axisY + axisHeight / 2 + canv.measureText(yLabel).width/2), axisX - 37);
	canv.rotate(Math.PI / 2);
}

function scaleText(text, maxFontSize, maxWidth, canv){
	let fontSize = maxFontSize;
	canv.font = fontSize + "px serif";
	
	while(canv.measureText(text).width > maxWidth){
		fontSize -= 1;
		canv.font = fontSize + "px serif";
	}
}
/**
 * Draws and labels the color bar on the right side of the canvas
 */
function drawColorBar(canv){
	canv.fillStyle = "black";
	let ticPix = barHeight / 6.0 ;
	let deltaZ = zMax - zMin;
	let deltaZTic = (ticPix / barHeight * deltaZ).toFixed(3);
	scaleText(zMax.toFixed(1), 12, 25, canv);

	var z_0 = Math.ceil(zMin / deltaZTic) * deltaZTic;	
	var i = 0;
	let h = 100 + height;
	canv.font = '10px serif';
	fontSize = 10;
	while( canv.measureText(zMax.toFixed(1)).width > 25){
		fontSize -= 1;
		canv.font = fontSize + "px serif";
	}
	for(let height = barY; height <= barY + barHeight + 10; height += ticPix){
		let z = -1.0 * deltaZ / barHeight * (height - barY) + zMax;
		canv.rect(barX + barWidth, height, 10, 0);
		canv.fillText(z.toFixed(5), barX + barWidth + 12, height + 2);
		
	}
	canv.rect(barX, barY, barWidth, barHeight);    
	canv.fillStyle = grd;   
	canv.fillRect(barX, barY, barWidth, barHeight);
	canv.fillStyle = "black";
	if(errorThreshold > 0 && errorThreshold < (zMax-zMin)/2){
		let threshHeight =(errorThreshold * 2) / (zMax - zMin) * barHeight;
		let threshY      = barY + (barHeight - threshHeight)/2;
		canv.rect(barX, threshY , barWidth, threshHeight);
	}
	console.log("Purchased: " + reportList[reportIndex]['purchased']);
}

/**
 * Given the x and y coordinates (relative to the canvas in pixels), 
 * returns the (x, y, z) coordinates in terms of the scan
 */
function getCoords(xx, yy, canvOff){
	let z = getHeight(xx, yy, canvOff);
	let xScan = xMin + (xMax - xMin)/axisWidth*(xx - axisX);
	let yScan = yMin - (yMax - yMin)/axisHeight*(yy - axisY-axisHeight);
	return [xScan, yScan, z];
}

/**
 * Given the x and y coordinates of the scan, 
 * returns the (x, y) coordinates in terms pixels on the canvas
 */
function fromCoords(xScan, yScan, canvOff){
	let xx = axisX + axisWidth*(xScan - xMin)/(xMax - xMin);
	let yy = axisY + axisHeight - axisHeight*(yScan - yMin)/(yMax - yMin);
	return [xx, yy];
}

/**
 * Given the x and y coordinates (relative to the canvas in pixels), 
 * returns the (y, x, z) coordinates in terms of the scan in a formatted string
 */
function getCoordsStr(xx, yy, canvOff){
	coords = getCoords(xx, yy, canvOff);
	if (coords[2])
		return coords[1].toFixed(3) + ", " + coords[0].toFixed(3) + ", " + coords[2].toFixed(5);
	else
		return coords[1].toFixed(3) + ", " + coords[0].toFixed(3);
}

/**
 * Given two points in terms of canvas pixels, 
 * returns the distance between them in the scan units
 */
function getDistance(x1, y1, x2, y2){
	x1 = xMin + (xMax - xMin)/axisWidth*(x1 - axisX);//TODO implement helper method for xCanvas->xScan conversion
	x2 = xMin + (xMax - xMin)/axisWidth*(x2 - axisX);
	y1 = yMin - (yMax - yMin)/axisHeight*(y1 - axisY-axisHeight);
	y2 = yMin - (yMax - yMin)/axisHeight*(y2 - axisY-axisHeight);
	return ((x1-x2)**2 + (y1-y2)**2)**0.5;
}
function scanToCanvas(x, y){
	x = axisWidth/(xMax-xMin)*(x-xMin) + axisX;
	y = axisHeight/(yMin - yMax) * (y-yMin) + axisY;
}

function scanXtoReport(x){
	return axisWidth/(xMax - xMin) * (x - xMin) + axisX;
}
function scanYtoReport(y){
	return axisHeight/(yMin - yMax) * (y - yMax) + axisY;
}

function unitConvert(val, inUnit, outUnit) {
	ratio = {
		"m": 1,
		"in": 39.37007874,
		"ft": 3.28084,
		"16th": 629.92125984,
		"8th": 314.96062992,
		"surft": (3937/1200)
	};
	if (outUnit in ratio && inUnit in ratio) {
		let inMeters = val/ratio[inUnit];
		return inMeters * ratio[outUnit];
	} else {
		return val
	}
}

/**
 * Given a json of pairs of points, draws lines for points that are within the bounds of the scan
 */
function drawDXFLines(DXFjson, canv){
	parsedJSON = JSON.parse(DXFjson);
	lines = parsedJSON["LINES"];
	dxfUnits = parsedJSON["UNITS"];
	for(let i = 0; i < lines.length; i++){
		let x_0 = scanXtoReport(unitConvert(lines[i]['start'][0], dxfUnits, units));
		let y_0 = scanYtoReport(unitConvert(lines[i]['start'][1], dxfUnits, units));
		
		let x_f = scanXtoReport(unitConvert(lines[i]['end'][0], dxfUnits, units));
		let y_f = scanYtoReport(unitConvert(lines[i]['end'][1], dxfUnits, units));
		
		let inRangeX = x_0 > axisX && x_f > axisX && x_0 < axisX + axisWidth && x_f < axisX + axisWidth;
		let inRangeY = y_0 > axisY && y_f > axisY && y_0 < axisY + axisHeight && y_f < axisY + axisHeight;
		
		if (inRangeX && inRangeY){
			canv.moveTo(x_0, y_0);
			canv.lineTo(x_f, y_f);
		}
		canv.stroke();
	}
}

function enableButtonsPurch(enable){
buttons = ["download_PDF",  "pts_button", "share"];
	for(let i = 0; i < buttons.length; i++){
		document.getElementById(buttons[i]).disabled=enable;
	}
}


/**
 * Gets the height at the scan based on the (X,Y) relative to the canvas
 */
function getHeight(x, y, canvOff){
	x = (x-axisX) / axisWidth  * reportWidthOrig;//TODO use helper method
	y = (y-axisY) / axisHeight * reportHeightOrig;	
	let pixel = canvOff.getImageData(Math.floor(x), Math.floor(y), 1, 1);
	let z = 0;
	for(let i = 0; i < 3; i ++){
		z += pixel.data[i] * (2**(8*i));
	}
	if(z === 0){return undefined;}	
	z -= 1;
	return z/(2**24 - 1) * (zMax - zMin) + zMin;
}

/**
 * Calculates the dimensions of the axis that will fit the scan an maintain the ratio
 */
function scaleReport(reportWidth, reportHeight, canv){
	reportWidthOrig = reportWidth;
	reportHeightOrig = reportHeight;
	//let reportWidth  = report.width;
	//let reportHeight = report.height;
	
	if(reportWidth / reportHeight > axisWidth / axisHeight){
		reportHeight *= axisWidth / reportWidth; 
		reportWidth = axisWidth;
	}
	else{
		reportWidth *= (axisHeight / reportHeight);
		reportHeight = axisHeight;
	}
	//axisX= 50 + (axisWidth - reportWidth) / 2;
	//axisY = titleHeight + 25 + (axisHeight - reportHeight) / 2;
	axisWidth  = reportWidth;
	axisHeight = reportHeight;
}

download_img = function(el) {
  	var image = canvas.toDataURL("image/PNG");
  	el.href = image;
};

download_PDF.addEventListener("click", function() {
 	let pdf = generatePDF(canvas, ctxOff);
  	pdf.save("download.pdf");
}, false);
/**
 * returns true iff the (x,y) are within the axis window
 */
function inBounds(x, y){
	return (x >= axisX && x <= axisX + axisWidth && y >= axisY && y <= axisY + axisHeight);
}

//TODO: Make copy for other canvas
/**
 * displays the coordinates of the scan where the users mouse is
 */
function showCoords(event, id, canvOff) {
  	var rect = id.getBoundingClientRect();
		var x = event.clientX - rect.left;
  	var y = event.clientY - rect.top + 0.5;
  	var z = 0;
    document.getElementById("flpoint").innerHTML = document.getElementById("point").innerHTML
  	
  	var coords ="(Y, X, Z): " + getCoordsStr(x, y, canvOff);
  	if(inBounds(x, y)){
        	id.style.cursor = "crosshair";
  		document.getElementById("point").innerHTML = coords;
  	}
  	else{
  		id.style.cursor = "default";
    	document.getElementById("point").innerHTML = "Move mouse over scan to see height values. Click to save a point.";
  	}
}

/**
 * Displays the next report in the list
 */
next.addEventListener("click", function() {
	reportIndex += 1;
	reportIndex %= reportList.length;
	drawCanvas(reportList[reportIndex], ctx, ctxOff);
	drawCanvas(reportList[reportIndex], flctx, flctxOff);
}, false);
prev.addEventListener("click", function(){
	reportIndex += reportList.length-1;
	reportIndex %= reportList.length;
	drawCanvas(reportList[reportIndex], ctx, ctxOff);
	drawCanvas(reportList[reportIndex], flctx, flctxOff);
}, false);
/**
 * downloads the pts file
 */
pts.addEventListener("click", function(){
	src = reportList[reportIndex]["pts"];
	$("#pts").attr("href",src);
});
function hideCanvas(){
	document.getElementById("reportPane").hidden=true;
}
function showCanvas(){
	document.getElementById("reportPane").hidden=false;
	getRepsLeft();
}

