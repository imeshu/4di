   
                  var fileStr = '';
                  var linkArray = [];
                  //Gets user's box token and displays the Box File Picker
                  $.ajax({
                      method: "GET",
                      url: "/token",
                      xhrFields: {
                          withCredentials: true
                      },
                      success: function(data){
                        showPicker(data);
                       /*   var loginDiv = document.getElementById("sign_in_options");
                          if(signedIn == false){
                            loginDiv.style.visibility = "visible";
                            //alert(signedIn);
                          }else{ //if (signedIn == true){
                            loginDiv.style.visibility = "hidden";
                            showPicker(data) 
                            //alert(signedIn);
                          }
                        */      
                      }
                  })
                      
                      var folderId = '0';
                      var filePicker = new Box.FilePicker();
               
            // When a user chooses a file in the picker, this extracts necessary data like the name, size, and download link and sends it to EC2 so the file can be uploaded
            filePicker.addListener('choose', function(items) {
                          //Upon the user choosing files, the names and download links are sent to node
                          console.log('choose');
                   console.log(items);
                   console.log(items.length);
                   var fileStrs = [];
                   var i = 0;
                          for(i = 0; i<items.length; i++){
                  console.log("In loop");
                              fileStr = JSON.stringify(items[i], null, 2);
                  // TO DO: Transfer parsing to JSON for cleanliness
                  console.log("FILE STRING RETURNED")
                  //console.log(fileStr);
                  var fileIDIndx = fileStr.search('"id": "');
                  var endFileIDIndx = fileStr.search('"etag":');
                  var fileID = fileStr.slice(fileIDIndx+7, endFileIDIndx-5);
                              var directDL = "https://api.box.com/2.0/files/" + fileID + "/content";
                              var nameIndx = fileStr.search('"name": "');
                              var sizeIndx = fileStr.search('"size"');
                              var name = fileStr.slice(nameIndx + 9, (sizeIndx - 5));
                  fileStrs.push(name);
                              var endSizeIndx = fileStr.search('"parent"');
                              var size = fileStr.slice(sizeIndx + 8, (endSizeIndx - 4));
                  linkArray[i] = name + "--" + directDL + "--" + size
                              console.log(linkArray[i]);
                          }
                   $("#boxprog").attr({
                              "aria-valuenow" : 0,
                              "style" : "width:0%"
                          });
                          $("#boxprog").text("0%");
                          
                          $.ajax({
                              method: "POST",
                              url: '/download',
                              data: {"strArr": linkArray.join()},
                              async: true,
                              xhrFields: {
                                  withCredentials: true
                              },
                              success: function(data){
                  console.log("Links sent!");
                  fileStrs.forEach(function(fstr) {
                  var splitfile = fstr.split('.');
                  var file_type = splitfile[splitfile.length-1];
                  var filename = fstr;
                  console.log("FILE TYPE:" + file_type);
                        	if (file_type == "e57" || file_type == "dxf") {
                  	$("#filename-"+file_type).prop("value", filename);
                  }
                  });
                              	pollboxprogress(data);
                              }
                          });
            });
                   //Displays the file picker       
                   function showPicker(aT){   
                    	filePicker.show(folderId, aT, {
                          container: '.boxContainer',
                          chooseButtonLabel: 'Choose',
                          logoUrl: 'https://s3.amazonaws.com/4distatic/images/4Dicon.png',
                          extensions: ['e57', 'dxf', 'rar', 'zip', 'pts'],
                      });
                      
                      console.log(aT);
                  }
                      
               
