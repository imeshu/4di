# mathutils.py
# Created by Sean Sullivan
# 4Di
from scipy.optimize import leastsq
from scipy.spatial import Delaunay
import math
import numpy as np

def alpha_shape(points, alpha, only_outer=True):
    """
    Compute the alpha shape (concave hull) of a set of points.
    :param points: np.array of shape (n,2) points.
    :param alpha: alpha value.
    :param only_outer: boolean value to specify if we keep only the outer border
    or also inner edges.
    :return: set of (i,j) pairs representing edges of the alpha-shape. (i,j) are
    the indices in the points array.
    """
    assert points.shape[0] > 3, "Need at least four points"
    
    def add_edge(edges, i, j):
        """
        Add an edge between the i-th and j-th points,
        if not in the list already
        """
        if (i, j) in edges or (j, i) in edges:
            # already added
            assert (j, i) in edges, "Can't go twice over same directed edge right?"
            if only_outer:
                # if both neighboring triangles are in shape, it's not a boundary edge
                edges.remove((j, i))
            return
        edges.add((i, j))

    tri = Delaunay(points)
    edges = set()
    # Loop over triangles:
    # ia, ib, ic = indices of corner points of the triangle
    for ia, ib, ic in tri.vertices:
        pa = points[ia]
        pb = points[ib]
        pc = points[ic]
        # Computing radius of triangle circumcircle
        # www.mathalino.com/reviewer/derivation-of-formulas/derivation-of-formula-for-radius-of-circumcircle
        a = np.sqrt((pa[0] - pb[0]) ** 2 + (pa[1] - pb[1]) ** 2)
        b = np.sqrt((pb[0] - pc[0]) ** 2 + (pb[1] - pc[1]) ** 2)
        c = np.sqrt((pc[0] - pa[0]) ** 2 + (pc[1] - pa[1]) ** 2)
        s = (a + b + c) / 2.0
        area = np.sqrt(s * (s - a) * (s - b) * (s - c))
        circum_r = a * b * c / (4.0 * area)
        if circum_r < alpha:
            add_edge(edges, ia, ib)
            add_edge(edges, ib, ic)
            add_edge(edges, ic, ia)
    
    outlines = outlineList(edges)
    #draw_outlines(edges, outlines, points) 
    return edges

def outlineList(edges):
    """
    Converts a list of edges to a 2D lists, where the inner
    lists contains ordered points that form a polygon outline
    """
    unvisited   = set()  #Point indicies not visited by an outline yet 
    dirEdges = dict() #key, value represents line segment from point index key to value
    outlines = []    

    #Build the adjacency dictionary and a set of unvisited points
    for start, end in edges: #two ends of each line segment
        unvisited.add(start)
        unvisited.add(end)
        dirEdges[start] = end

    #loop until all points have been visited (remove from 'points' once visited):
    while len(unvisited) != 0: 
        outline = []
        point   = unvisited.pop()
        
        while not point in outline: #loop until a point is revisited
            if(point in unvisited): #remove visited points
                unvisited.remove(point)
            outline.append(point)
            point = dirEdges[point]
        outline.append(point) #Close the loop
            
        outlines.append(outline)

    return outlines

def apply_mask(triang, alpha, xvals, yvals):
    # Mask triangles with sidelength bigger some alpha
    triangles = triang.triangles
    # Mask off unwanted triangles.
    xtri = xvals[triangles] - np.roll(xvals[triangles], 1, axis=1)
    ytri = yvals[triangles] - np.roll(yvals[triangles], 1, axis=1)
    maxi = np.max(np.sqrt(xtri**2 + ytri**2), axis=1)
    # apply masking
    triang.set_mask(maxi > alpha)
    
#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def onSegment(px, py, qx, qy, rx, ry):
    return (qx <= max(px, rx)) and (qx >= min(px, rx)) and (qy <= max(py, ry)) and (qy >= min(py, ry))

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def orientation(px, py, qx, qy, rx, ry):
    val = (qy - py) * (rx - qx) - (qx - px) * (ry - qy); 
  
    if (val == 0):
        return 0 #collinear
    else:
        if val > 0:
            return 1
        else:
            return 2 #clock or counterclock wise
        
#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def doIntersect(p1x, p1y, q1x, q1y, p2x, p2y, q2x, q2y):
    o1 = orientation(p1x, p1y, q1x, q1y, p2x, p2y) 
    o2 = orientation(p1x, p1y, q1x, q1y, q2x, q2y)
    o3 = orientation(p2x, p2y, q2x, q2y, p1x, p1y)
    o4 = orientation(p2x, p2y, q2x, q2y, q1x, q1y) 
  
    if (o1 != o2) and (o3 != o4):
        return True
  
    # Special Cases 
    # p1, q1 and p2 are colinear and p2 lies on segment p1q1 
    if (o1 == 0) and (onSegment(p1x, p1y, p2x, p2y, q1x, q1y)):
        return True
  
    # p1, q1 and p2 are colinear and q2 lies on segment p1q1 
    if (o2 == 0) and (onSegment(p1x, p1y, q2x, q2y, q1x, q1y)):
        return True
  
    # p2, q2 and p1 are colinear and p1 lies on segment p2q2 
    if (o3 == 0) and (onSegment(p2x, p2y, p1x, p1y, q2x, q2y)):
        return True
  
    # p2, q2 and q1 are colinear and q1 lies on segment p2q2 
    if (o4 == 0) and (onSegment(p2x, p2y, q1x, q1y, q2x, q2y)):
        return True
  
    return False # Doesn't fall in any of the above cases

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def isInside(polygon, px, py):
    n = len(polygon)
    if (n < 3):
        return False
  
    # Create a point for line segment from p to infinite 
    ex = 10000000
    ey = py
  
    # Count intersections of the above line with sides of polygon 
    count = 0
    i = 0
    notDone = True
    while notDone:
        #print(i)
        nxt = (i+1)%n
        #print(nxt)
        #print(len(polygon))
        #print(polygon[0])
        # Check if the line segment from 'p' to 'extreme' intersects 
        # with the line segment from 'polygon[i]' to 'polygon[next]' 
        if (doIntersect(polygon[i][0], polygon[i][1], polygon[nxt][0], polygon[nxt][1], px, py, ex, ey)):
            # If the point 'p' is colinear with line segment 'i-next', 
            # then check if it lies on segment. If it lies, return true, 
            # otherwise false 
            if (orientation(polygon[i][0], polygon[i][1], px, py, polygon[nxt][0], polygon[nxt][1]) == 0):
               return onSegment(polygon[i][0], polygon[i][1], px, py, polygon[nxt][0], polygon[nxt][1])
  
            count = count + 1 
        i = nxt
        if (i == 0):
            notDone = False
  
    # Return true if count is odd, false otherwise 
    return count%2 == 1  # Same as (count%2 == 1)

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def f_min(X,p):
    plane_xyz = p[0:3]
    distance = (plane_xyz*X.T).sum(axis=1) + p[3]
    return distance / np.linalg.norm(plane_xyz)

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def residuals(params, signal, X):
    return f_min(X, params)

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def filterBoundingDWG(vals, polyVals):
    nxvals = []
    nyvals = []
    nzvals = []
    i = 0
    for i in range(0, vals.size):
        if isInside(polyVals, vals[i][0], vals[i][1]):
            nxvals.append(vals[i][0])
            nyvals.append(vals[i][1])
            nzvals.append(vals[i][2])
    return nxvals, nyvals, nzvals

#@jit(parallel=True, nopython=True, fastmath=True, nogil=True)
def getDistVals(xvals, yvals, zvals, sol):
    diffs = []
    for i in range(0, len(xvals)):
        dist = (sol[0]*xvals[i]+sol[1]*yvals[i]+sol[2]*zvals[i]+sol[3])/np.sqrt(np.square(sol[0]) + np.square(sol[1]) + np.square(sol[2]))
        diffs.append(dist)
    return diffs
