# afDebug.py
# Created by Mica Lewis
# 4Di
# A means of testing memory useage and speed of an arrayfire function

import arrayfire as af
import numpy as np
import matplotlib.pyplot as plt
import time

class Run:
    def __init__(self):
        self.buffers = []
        self.alloc = []
        self.times = []
        self.messages = []
        self.running = False

    def start(self):
        self.running = True
        self.prev = time.time()

    def step(self, message):
        if not self.running:
            return
        step_time = time.time() - self.prev
        self.buffers.append(af.device_mem_info()["alloc"]["buffers"])
        self.alloc.append(af.device_mem_info()["alloc"]["bytes"])
        self.times.append(step_time)
        self.messages.append(message)
        self.prev = time.time()

    def showplot(self):
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
        ax1.plot(self.buffers)
        ax1.label("buffers")
        ax2.plot(self.alloc)
        ax2.label("alloc")
        ax3.plot(self.times)
        ax3.label("times")
        fig.showplot()

    def showtext(self):
        print("times\tbuffers\talloc")
        for i in range(self.times):
            print(str(self.times[i]) + "\t" + str(self.buffers[i]) + "\t" + str(self.alloc[i]))


def max_points (func):
    high = 10**8
    low = 0
    success = True
    
    while success:
        high = high*2
        a = af.randu(int(high),3)
        try:
            func(a)
        except Exception as e:
            success = False

    while (high - low > 100):
        success = True
        n = int((high + low)/2)

        a = af.randu(int(n),3)
        try:
            func(a)
        except Exception as e:
            success = False

        del a
        af.device_gc()

        if success:
            low = n

        else:
            high = n

    return low

