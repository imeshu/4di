# Histogram.py
# Created by Mica Lewis
# 4Di

import arrayfire as af
import numpy as np
import math
from AF_Analysis import afDebug

MAX_POINTS = int(293750000/3 - 100)

# depricated
# An experimental fuction to sort 1 dimensional values into a two dimensional array
# with each value sorted into evenly spaced 'bucket' based on it's value and backfilled with 0's
# a: input array
# size: number of buckets
# min_val: beginning of lowest bucket
# max_val: end of highest bucket
def hist1d(a, size, min_val, max_val):

    hist = af.histogram(a, size, min_val, max_val)

    anti = af.max(hist)-hist

    fill = af.constant(0, int(af.sum(anti)))

    fill_index = af.scan(anti)
    fill_index = af.shift(fill_index, 0)[1:]

    fill[af.flip(fill_index)] = af.flip(1 + af.range(size-1))
    fill = af.scan(fill, op = af.BINARYOP.MAX)

    if anti[-1] == 0:
        fill[-1] = fill[-2]

    vals = af.constant(0,int(size*af.max(hist)))
    keys = af.constant(0,int(size*af.max(hist)))

    vals[:a.dims()[0]] = a
    keys[:a.dims()[0]] = af.floor( (a-min_val)/(max_val-min_val) * size )
    keys[a.dims()[0]:] = fill
    
    result = af.sort(af.moddims(out[0], int(af.max(hist)), size))

    return result, hist

# depricated
# An experimental fuction to sort an array of n>2 dimensional point sinto a three dimensional array
# with each value sorted into a 'bucket' based on the first two dimensions of the points and backfilled with 0's
def hist2d(a, size_x, size_y, min_x, min_y, max_x, max_y):

    size_flat = size_x*size_y

    keys_x = af.floor( ( a[:,0] - min_x )/( max_x - min_x ) * size_x ) * size_y
    keys_y = af.floor( ( a[:,1] - min_y )/( max_y - min_y ) * size_y )
    data_keys = keys_x + keys_y

    hist = af.histogram(data_keys, size_flat, 0, size_flat)
    
    anti = af.max(hist)-hist

    vals = af.constant(0, int(size_flat*af.max(hist)), a.dims()[1])
    keys = af.constant(0, int(size_flat*af.max(hist)))

    vals[:a.dims()[0]] = a
    keys[:a.dims()[0]] = data_keys

    if af.sum(anti) != 0:

        fill = af.constant(0, int(af.sum(anti)))

        # Sums all elements of the list, this can cause rounding errors
        fill_index = af.scan(anti)
        fill_index = fill_index[:-1]

        fill[af.flip(fill_index)] = af.flip(1+af.range(fill_index.dims()[0]))
        fill = af.scan(fill, op = af.BINARYOP.MAX)

        if anti[-1] == 0:
            fill[-1] = fill[-2]
    
        keys[a.dims()[0]:] = fill

    out = af.sort_by_key(af.tile(keys, 1, a.dims()[1]), vals)
    # out = af.sort_by_key(keys, vals[:,-1])
    # out = af.sort_index(keys)

    result = af.reorder(af.moddims(out[0], int(af.max(hist)), size_flat, a.dims()[1]), 0, 2, 1)

    return result, hist

# Similar to the above function restricted to 3 dimensional points. Performs an averaging operation on 
# each bucket that would be generated. Uses arrayfire which can crash using large numbers of points
def af_avg_grid(a, size_x, size_y, min_x, max_x, min_y, max_y):

    size_flat = size_x*size_y

    keys_x = af.floor( ( a[:,0] - min_x )/( max_x - min_x ) * size_x ) * size_y
    keys_y = af.floor( ( a[:,1] - min_y )/( max_y - min_y ) * size_y )
    data_keys = keys_x + keys_y

    z_vals, data_keys = af.sort_by_key(data_keys, a[:,2])
    z_vals = z_vals.as_type(af.Dtype.f64)
    data_keys = data_keys.as_type(af.Dtype.u64)

    hist = af.histogram(data_keys, size_flat, 0, size_flat)

    aug = af.constant(0, size_flat + 1, dtype=af.Dtype.f64)
    aug[1:] = af.scan(z_vals)[af.scan(hist)-1]

    sums = af.diff1( aug )

    sums = af.moddims(
        sums,
        size_y,
        size_x
        ).as_type(af.Dtype.f32)

    hist = af.moddims(
        hist,
        size_y,
        size_x
        ).as_type(af.Dtype.f32)

    return (sums, hist)

# A numpy based wrapper around the af_avg_grid function. Runs the function in subsets of a safe size,
# then combines the returned grids from each block. Also uses the bounds function to get min, max and size values.
def avg_grid(a, b=None):

    if b == None:
        b = get_bounds(a)
    grid_count = af.constant(0, b["sizey"], b["sizex"])
    grid_sum = af.constant(0, b["sizey"], b["sizex"])

    idx = 0
    while idx < a.shape[0]:
        upper = min(idx + MAX_POINTS, a.shape[0])
        sums, hist = af_avg_grid(
            af.from_ndarray(a[idx:upper]),
            b["sizex"], b["sizey"], b["minx"],
            b["maxx"], b["miny"], b["maxy"])

        grid_count += hist
        grid_sum += sums

        del(sums)
        del(hist)

        idx = upper
        af.device_gc()

    return af.flip(grid_sum/grid_count)

# Merges two image outputs. Could be used for combining multiple executions of af_avg_grid across multiple GPUS
def af_merge_grids(imga, imgb):
    assert(imga.dims() == imgb.dims())
    af.replace(imga, af.isinf(imga), 0)
    af.replace(imgb, af.isinf(imgb), 0)
    return (imga + imgb)/((imga>0) + (imgb>0))

# encodes the float32 height values in each position in the numpy array 'a' into RGB values (unsigned int48) scaled between
# the minimum and maximum value provided in min_val and max_val
def encodeRGB(a, min_val, max_val):

    out = af.constant(0, a.dims()[0], a.dims()[1], 3, dtype=af.Dtype.u8)

    out32 = a.as_type(af.Dtype.f64)
    out32 = (a - min_val)/(max_val - min_val)*(2**24-2)+1

    af.replace(out32, af.isinf(a) + af.isnan(a) < 1, 0)

    out[:,:,0] = ( out32%(2**8) ).as_type(af.Dtype.u8)
    out[:,:,1] = ( out32%(2**8)/(2**8) ).as_type(af.Dtype.u8)
    out[:,:,2] = ( out32/(2**16) ).as_type(af.Dtype.u8)

    return out

# encodes the float32 height values in each position of array 'a' into a gradient of RGB values (a heatmap) 
def colorRGB(a, min_val, max_val):

    out = af.constant(255, a.dims()[0], a.dims()[1], 3, dtype=af.Dtype.u8)

    inrange = (a>min_val)*(a<(max_val-min_val)/3 + min_val)
    norm = (a - min_val)/(max_val - min_val)*3
    out[:,:,0] = af.select(inrange, 0, out[:,:,0])
    out[:,:,1] = af.select(inrange, (norm*230).as_type(af.Dtype.u8), out[:,:,1])
    out[:,:,2] = af.select(inrange, ((1 - norm)*255).as_type(af.Dtype.u8), out[:,:,2])
    del inrange, norm
    af.device_gc()

    inrange = (a > (max_val - min_val)/3 + min_val)*(a < (max_val - min_val)*2/3 + min_val)
    norm = (a - min_val - (max_val-min_val)/3)/(max_val - min_val)*3
    out[:,:,0] = af.select(inrange, ((norm)*255).as_type(af.Dtype.u8), out[:,:,0])
    out[:,:,1] = af.select(inrange, 230, out[:,:,1])
    out[:,:,2] = af.select(inrange, 0, out[:,:,2])
    del inrange, norm
    af.device_gc()

    inrange = (a > (max_val - min_val)*2/3 + min_val)*(a < max_val)
    norm = (a - min_val - (max_val - min_val)*2/3)/(max_val - min_val)*3
    out[:,:,0] = af.select(inrange, 255, out[:,:,0])
    out[:,:,1] = af.select(inrange, ((1-norm)*230).as_type(af.Dtype.u8), out[:,:,1])
    out[:,:,2] = af.select(inrange, 0, out[:,:,2])
    del inrange, norm
    af.device_gc()

    return out

# returns a JSON ready dictionary of information about an array of 3 dimensional points.
# The contents of the dictionary are as follows:
'''
{
    maxx : *the maximum x value*
    minx : *the minmum x value*
    maxy : *the maximum y value*
    miny : *the minmum y value*
    maxz : *the maximum z value*
    minz : *the minmum z value*
    sizex : *the reccomended width of an 2d array representing this point cloud*
    sizey : *the reccomended height of an 2d array representing this point cloud*
} 
''' 
def get_bounds(a, resolution=500):

    b = {}
    b['maxz'] = np.max(a[:,2])
    b['minz'] = np.min(a[:,2])

    min_x = float(np.min(a[:,0]))
    min_y = float(np.min(a[:,1]))
    max_x = np.max(a[:,0])
    max_y = np.max(a[:,1])

    tile = max( (max_x - min_x)/resolution, (max_y - min_y)/resolution )

    size_x = int( math.ceil( (max_x - min_x)/tile ) )
    max_x = tile*size_x + min_x
    size_y = int( math.ceil( (max_y - min_y)/tile ) )
    max_y = tile*size_y + min_y

    b['maxx'] = max_x
    b['minx'] = min_x
    b['maxy'] = max_y
    b['miny'] = min_y
    b['sizex'] = size_x
    b['sizey'] = size_y

    return b
