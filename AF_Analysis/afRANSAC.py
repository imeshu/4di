# afRANSAC.py
# Created by Mica Lewis
# 4Di

import arrayfire as af
import numpy as np
import time
from AF_Analysis import Histogram as hst
import traceback
from collections import namedtuple
import sys

MAX_POINTS = int(370466803/3 - 100)

test_data = af.np_to_af_array(
    np.array([[1, 1, 0], [0, 1, 0], [1, 0, 0], [0, 0, 1]],
             order='F',
             dtype='f'))
Plane = namedtuple("Plane", "coeffs inliers inlier_data")
Plane.__new__.__defaults__ = (None, )

STEP_TOTAL = 4
def printToSite(start, step, output):
    elapsed = time.time() - start
    print("{:.9f}".format(elapsed) + "s," + step + "/" + str(STEP_TOTAL) + "," + output)
    sys.stdout.flush()

# loads a structured csv faster than default numpy csv loading
def iter_loadtxt(filename,
        delimiter=',',
                 skiprows=0,
                 dtype=float,
                 out_every=2**31):
    def iter_func():
        with open(filename, 'r') as infile:
            i = 0
            for _ in range(skiprows):
                next(infile)
                i += 1
            for line in infile:
                if i % out_every == 0:
                    print("Loaded " + str(i) + " rows")
                line = line.rstrip().split(delimiter)
                line = line[1:]
                for item in line:
                    yield dtype(item)
                i += 1
        iter_loadtxt.rowlength = len(line)

    data = np.fromiter(iter_func(), dtype=dtype)
    data = data.reshape((-1, iter_loadtxt.rowlength))
    return data

# load npy array
def load_npy(filename):
    af.np_to_af_array(np.load(filename))

# write AF array to csv, writing is slow, files are large
def write_csv(data, outfile):
    if outfile[:-4] != ".csv":
        outfile += ".csv"
    np_data = data.to_ndarray()
    np.savetxt(outfile, np_data, delimiter=",")

# write AF array to numpy file, writing is fast, file is small
def write_npy(data, outfile):
    if outfile[:-4] != ".npy":
        outfile += ".npy"
    np_data = data.to_ndarray()
    np.save(outfile, np_data)

# Augments an nx3 matrix with a new column full of the value in const
def augment(data, const):
    aug = af.constant(const, d0=data.dims()[0], d1=data.dims()[1] + 1)
    aug[:, :-1] = data
    return aug

# Generate coefficients based on point normal form
def point_normal(point, normal):
    coeff = af.constant(0, 4)
    coeff[:3] = normal
    coeff[3] = -1 * af.dot(point, normal)
    return coeff

# least squares fit. Fits a plane by solving the equation Ax=B where A and B are over
# fit to x. All x coordinats, y coordinates, and an augmented row of 1s, in
# place of a d coefficient, form the matrix A. B is the negative of all Z
# coordinates. This equation solves for the coeffcients a, b, and d, assuming c is
# 1.
#
# Optional Arguments,
# transform (NOT IMPLEMENTED):
# If the plane is known to have a norm with 0 as the C coeffcient, a tranformation
# matrix can be applied before and after the calculation to prevent division by zero
# or rounding errors.
def plane_fit_lstsq(data, transform=af.identity(3, 3)):
    a0 = augment(data[:, :2], 1)
    a = af.matmul(a0.T, a0)
    b = -1 * af.matmul(a0.T, data[:, -1])
    out = af.constant(1, 3)
    abd = af.solve(a, b)
    abc = augment(abd[:2].T, 1).T
    out = af.constant(1, 4)
    out[:3] = abc
    out[3] = abd[2]
    return out

# Fit a plane with a fixed normal. Finds the average position of each point along a normal.
def plane_fit_fixed(data, norm):
    d = np.sum(-1 * np.dot(data, norm))
    if data.shape[1] == 3:
        d = d/data.shape[0]
    out = np.full(4, d, dtype=np.float32)
    out[:3] = norm
    return out

# NOT IMPLEMENTED
# Fit to any plane tangent to the z-axis
def plane_fit_vert(data):

    return

# Fits a plane using svd and a dataset augmented with a 4th column of 1s
# The plane fit will be the minimum orthogonal distance of each point to
# the plane.
def plane_fit_svd(data):
    return af.svd(augment(data, 1))[-1][-1, :].T

# Counts all datapoints within a certain threshold of a plane by calculating
# a list of all distances to the plane and counting the number of distances
# below the threshold
def inlier_count(data, coeffs, threshold):
    d = af.sqrt(af.sum(coeffs[:3]**2, dim=0))
    dists = af.abs(af.matmul(augment(data, 1), coeffs)) / d.scalar()
    return af.sum(dists < threshold)

# runs an arrayfire function on a list that is too large to fit into memory
# by splitting it into chunks and running those through
def mem_safe_lambda(data, func):

    idx = 0
    results = []
    ptct = 0

    while idx < data.shape[0]:

        upper = min(MAX_POINTS + idx, data.shape[0])

        af_out = func(af.from_ndarray(data[idx:upper]))
        if not af_out.is_empty():
            results.append( af_out.to_ndarray() )
            ptct += results[-1].shape[0]
            
        af.device_gc()
        idx = upper

    out = np.full((ptct,3), 0, 'F')
    idx = 0
    for result in results:
        out[idx:idx+result.shape[0]] = result
        idx += result.shape[0]

    return out.astype(np.float32)

# Gets all points that lie within a given threshold of a given plane using arrayfire (limited possible size of data)
def af_get_inliers(data, coeffs, threshold):
    d = af.sqrt(af.sum(coeffs[:3]**2, dim=0))
    dists = af.abs(af.matmul(augment(data, 1), coeffs)) / d.scalar()
    return af.lookup(data, af.where(dists < threshold))

# A wrapper around af_get_inliers that uses the mem_safe_lambda function to avoid overloading memory
def get_inliers(data, coeffs, threshold):
    return mem_safe_lambda(
            data,
            lambda a: af_get_inliers(
                a,
                af.from_ndarray(coeffs),
                threshold))

# Gets all points that lie outside of a given threshold of a given plane using arrayfire (limited possible size of data)
def af_get_outliers(data, coeffs, threshold):
    d = af.sqrt(af.sum(coeffs[:3]**2, dim=0))
    dists = af.abs(af.matmul(augment(data, 1), coeffs)) / d.scalar()
    return af.lookup(data, af.where(dists > threshold))

# A wrapper around af_get_outliers that uses the mem_safe_lambda function to avoid overloading memory
def get_outliers(data, coeffs, threshold):
    return mem_safe_lambda(
            data,
            lambda a: af_get_outliers(
                a,
                af.from_ndarray(coeffs),
                threshold))

# Generates a random sample of a dataset with no repetition of datapoints.
# Used only for large sets of data where repetition could be a problem.
def random_sample_lg(data, sample_size):
    rand_key = af.randu(data.dims()[0])
    return af.sort_by_key(af.tile(rand_key, 1, d1=data.dims()[1]), data,
                          dim=0)[0][:sample_size]


# Generates a random sample of a dataset with possible repetition of datapoints.
# Used for small samples of large datasets where repetition is not a problem.
def random_sample_sm(data, sample_size):
    rand_index = np.floor(np.random.rand(sample_size) *
                          data.shape[0]).astype(int)
    return data[rand_index]

def random_sample_fst(data, sample_size):
    return data[::int(data.shape[0]/sample_size)]

# Samples data from a sphere of a given radius around a random point
def random_sample_lcl(data, sample_radius):
    rand_point = random_sample_sm(data, 1)
    centered_data = data - af.tile(rand_point, data.dims()[0])
    dists = af.sqrt(af.sum(centered_data**2, dim=1))
    return af.lookup(data, af.where(dists < sample_radius))

#get center of mass
def get_com(data):
    return np.sum(data, 0) / data.shape[0]

#get center of mass using GPU
def af_get_com(data):
    return af.sum(data, 0) / data.dims()[0]

def plane_dist1(coeffs1, coeffs2):
    vec31 = coeffs1[:3] / coeffs1[3]
    vec32 = coeffs2[:3] / coeffs2[3]
    return af.sqrt(af.sum((vec31 - vec32)**2))

def plane_dist2(plane1, plane2):
    norm1 = plane1.coeffs[:3] / np.sqrt(af.sum(plane1.coeffs[:3]**2))
    norm2 = plane2.coeffs[:3] / np.sqrt(af.sum(plane2.coeffs[:3]**2))
    com_dist = np.sqrt(af.sum((plane1.com - plane2.com)**2))
    return np.sqrt(com_dist**2 + np.arccos(af.dot(norm1, norm2).scalar())**2)

# Runs a RANSAC algorithm to fit a plane with the highest number of inliers out of a
# series of iterations.
def RANSAC(data, threshold, iter_max, sampler, fitter):

    start = time.time()

    best_coeffs = np.full(4,0)
    best_inliers = 0
    reduced_data = random_sample_fst(data, min(50000, data.shape[0]))

    #possibly run in parallel with CPU
    for i in range(iter_max):

        sample = sampler(reduced_data)
        estimate = fitter(sample)
        inliers = inlier_count(
            af.from_ndarray(reduced_data),
            af.from_ndarray(estimate),
            threshold)

        if inliers > best_inliers:
            best_coeffs = estimate
            best_inliers = inliers

        af.device_gc()

    best_inlier_data = get_inliers(data, best_coeffs, threshold)
    best_plane = Plane(best_coeffs, best_inlier_data.shape[0], best_inlier_data)

    af.device_gc()
    return best_plane


def multiRANSAC(data, plane_threshold, iter_max, sampler, fitter, min_inliers,
                is_overlap):

    best_models = []

    for i in range(iter_max):

        sample = sampler(data)

        estimate = fitter(sample)

        inliers = get_inliers(data, estimate, plane_threshold)

        unique = True
        if not inliers.is_empty():

            plane = Plane(estimate, get_com(inliers), inliers.dims()[0])

            for i in range(len(best_models)):

                unique = unique and not is_overlap(best_models[i], plane)
                if not unique and inliers.dims()[0] > best_models[i].inliers:
                    del best_models[i]
                    best_models.append(plane)
                    break

            if unique:
                best_models.append(plane)

        inliners = None
        sample = None

        af.device_gc()

    return best_models

# wrapper around RANSAC to find only ground plane
def ground_plane(data,
                 threshold,
                 iter_max,
                 min_inliers=0,
                 overlap_threshold=0,
                 culling=100):

    if culling < 100:
        data = random_sample_fst(data, data.shape[0] * (culling / 100))

    if min_inliers < 0:
        result = RANSAC(
            data, threshold, iter_max, lambda data: random_sample_sm(data, 1),
            lambda data: plane_fit_fixed(data, np.array([0, 0, 1])))

    else:
        print("Warning - using multiransac")
        result = multiRANSAC(
            data, threshold, iter_max, lambda data: random_sample_sm(data, 1),
            lambda data: plane_fit_fixed(data, np.array([0, 0, 1])),
            min_inliers, lambda p1, p2: plane_dist2(p1, p2
                                                    ) < overlap_threshold)

    return result

# finds the inliers in the data for a user defined horizontal (z = height) plane
def user_input_plane(data, threshold, height, culling=100):

    coeffs = np.array([0, 0, 1, -height], dtype=np.float32)
    if culling < 100:
        data = random_sample_fst(data, data.shape[0] * (culling/100))
    inliers = get_inliers(data, coeffs, threshold)
    
    if inliers.shape == ():
        inliers = np.full((1,3), 0)

    return Plane(coeffs, inliers.shape[0], inliers)
