# RANSAC

Requires that arrayfire (http://arrayfire.org/arrayfire-python/) and numpy (http://www.numpy.org/) have been installed

afRANSAC.py and afRANSAC_test.py hold the primary functionality, other files kept for posterity

afRANSAC.py contains all functions required for RANSAC and the main RANSAC function

afRANSAC_test.py tests afRANSAC on two fixed datasets then asks for a .csv file to run RANSAC against
