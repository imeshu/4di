# file-read.py
# Created by Sean Sullivan
# 4Di
import numpy as np
import pye57
import pandas as pd
from sys import argv

# Combine e57 scans
def combineScanData(e57, numScans):
    dframe = pd.DataFrame({'x' : [], 'y' : [], 'z' : []}, dtype=np.float32)
    for i in range(0, numScans):
        print("On scan " + str(i))
        data = e57.read_scan(i, intensity=False, colors=False, row_column=False)
        dframe = dframe.append(pd.DataFrame({'x' : data["cartesianX"], 'y' : data["cartesianY"], 'z' : data["cartesianZ"]}, dtype=np.float32), ignore_index=True)  
    print("Dropping duplicates...")
    dframe.drop_duplicates()
    return dframe

file = argv[1]
frame = pd.DataFrame({'x' : [], 'y' : [], 'z' : []}, dtype=np.float32)
print(file)

if file[-4:] == ".e57":
    e57 = pye57.E57(file)
    imf = e57.image_file
    root = imf.root()
    data3d = root["data3D"]
    frame = frame.append(combineScanData(e57, len(data3d)))
    frame.to_csv('points2.csv')
    print("Wrote to points.csv - done")
