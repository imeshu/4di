# ForgeModelExtraction.py
# Created by Mica Lewis
# 4Di

# Uses the forge RestAPI on a test bed app to unpack .objs from .dwgs
# Produces unusable .obj files when Civil3D is used.

# Currently unused


import requests #http://requests.readthedocs.org/en/latest/
import argparse
import json
import base64
import time
from urllib import parse

client_id = '53XEB4aEvB0EAJXFOVASHXgfRRsMYasV'

BASE_URL = 'https://developer.api.autodesk.com/'
BUCKET_KEY = 'dwgs_' + client_id.lower()



def getToken(client_id, client_secret):
    req = { 'client_id' : client_id,
            'client_secret': client_secret,
            'grant_type' : 'client_credentials',
            'scope' : 'data:read data:write bucket:create bucket:read'}
    resp = requests.post(BASE_URL+'authentication/v1/authenticate', req ).json();
    return resp['token_type'] + " " + resp['access_token']

def createBucket(bucketKey, token):
    
    header = {
        'Authorization' : token
    }
    res1 = requests.get(BASE_URL+'oss/v2/buckets/'+bucketKey+'/details', headers=header)
    
    if res1.status_code != 200 :
        res2 = requests.post(BASE_URL+'oss/v2/buckets',
                             headers={
                                 'Content-Type' : 'application/json',
                                 'Authorization' : token
                                 },
                             data=json.dumps({
                                 'bucketKey' : BUCKET_KEY,
                                 'policyKey' : 'transient'
                                 })
                             )
        return res2
    else:
        return res1

def uploadFile(filename, bucketKey, token):

    res1 = requests.get(BASE_URL+'oss/v2/buckets/'+bucketKey+'/objects/'+filename+'/details',
                         headers={
                             'Authorization' : token
                             }
                        )

    if res1.status_code == 404:
        with open(filename, 'rb') as f:

            res2 = requests.put(
                BASE_URL+'oss/v2/buckets/'+bucketKey+'/objects/'+filename,
                headers={
                    'Content-Type' : 'application/octet-stream',
                    'Authorization' : token
                    },
                data=f
                )

            return res2.json()['objectId']

    else:
        return res1.json()['objectId']

def clearManifest(urn, token):
    return requests.delete(
        BASE_URL+
            'modelderivative/v2/designdata/'+
            base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
            '/manifest',
            headers={
                'Authorization' : token
                }
            )

def getManifest(urn, token):
    return requests.get(
        BASE_URL+
        'modelderivative/v2/designdata/'+
        base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
        '/manifest',
        headers={
            'Authorization' : token
            }
        )

def postJob(filename, urn, token, formats=[{ 'type' : 'svf', 'views' : [ '2d', '3d' ]}] ):
    return requests.post(
        BASE_URL+'modelderivative/v2/designdata/job',
        headers={
            'Content-Type' : 'application/json',
            'Authorization' : token
            },
        data=json.dumps({
            "input" : {
                "urn" : base64.b64encode(urn.encode()).decode().strip('='),
                #"compressedUrn" : True,
                #"rootFilename" : filename
                },
            'output' : {
                'destination' : {
                    'region' : 'us'
                    },
                'formats' : formats
                }
            })
        )

def waitOnJob(urn, token):

    res_manifest = getManifest(urn, token)
    
    manifest = 'pending'
    while manifest == 'pending' or manifest == 'inprogress':
        
        res_manifest = getManifest(urn, token)
        
        if res_manifest.status_code == 200:
            manifest = res_manifest.json()['status']
            print(res_manifest.json()['progress'])
            
        else:
            print(res_manifest.text)
            manifest = 'failed'
            #clearManifest(urn, token)
            return res_manifest
        
        time.sleep(3)

    return res_manifest

def getMetadata(urn, token):
    return requests.get(
        BASE_URL+
        'modelderivative/v2/designdata/'+
        base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
        '/metadata',
        headers={
            'Authorization' : token
            }
        )

def getObjectMetadata(guid, urn, token):
    return requests.get(
        BASE_URL+
        'modelderivative/v2/designdata/'+
        base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
        '/metadata/'+
        guid,
        headers={
            'Authorization' : token
            }
        )

def getObjectMetadataProps(guid, urn, token):
    return requests.get(
        BASE_URL+
        'modelderivative/v2/designdata/'+
        base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
        '/metadata/'+
        guid+
        '/properties',
        headers={
            'Authorization' : token
            }
        )

def downloadDerivative(outfile, urn, derivativeurn, token):
    
    with requests.get(
        BASE_URL+
        'modelderivative/v2/designdata/'+
        base64.urlsafe_b64encode(urn.encode()).decode().strip('=')+
        '/manifest/'+
        parse.quote(derivativeurn),
        stream=True,
        headers={
            'Authorization' : token
            }) as r:
        
        r.raise_for_status()
        with open(outfile, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()

def getObj(filename, bucketKey, urn, token):

    res_metadata = getMetadata(urn, token)

    if(res_metadata.status_code != 200):


        print("running post job for svf, cloud credits down the drain")
        res_job = postJob(filename, urn, token)

        if res_job.status_code != 200:
            print(res_job.text)
            return res_job

        with open(filename[:-4]+'_svf_manifest.json', 'w') as outfile:
            json.dump( waitOnJob(urn, token).json(), outfile, indent=4 )
        
        res_metadata = getMetadata(urn, token)
        
        if res_metadata.status_code != 200:
            print(res_metadata.text)
            return res_metadata

    with open(filename[:-4]+'_metadata.json', 'w') as outfile:
        json.dump(res_metadata.json(), outfile, indent=4)

    views = list(filter( lambda view: view['role'] == '3d', res_metadata.json()['data']['metadata']))
    if len(views) > 1:
        raise Exception('multiple 3d views in ' + filename + ":\n" + str(views))
    guid = views[0]['guid']

    with open(filename[:-4]+'_3D_metadata.json', 'w') as outfile:
        json.dump(getObjectMetadata(guid, urn, token).json(), outfile, indent=4)

    with open(filename[:-4]+'_3D_properties.json', 'w') as outfile:
        json.dump(getObjectMetadataProps(guid, urn, token).json(), outfile, indent=4)

    res_manifest = getManifest(urn, token)

    with open(filename[:-4]+'_obj_manifest.json', 'w') as outfile:
        json.dump(res_manifest.json(), outfile, indent=4)

    if not any( view['outputType'] == 'obj' for view in res_manifest.json()['derivatives']):

        print("running post job for obj, cloud credits down the drain")
        res_job = postJob(filename, urn, token, formats=[{
            'type':'obj',
            'advanced':{
                'modelGuid':guid,
                'objectIds':[-1]}
            }])

        if res_job.status_code != 200:
            print(res_job.url)
            print(res_job.text)
            return res_job
        
        res_manifest = waitOnJob(urn, token)

    derivative = list(filter( lambda view: view['outputType'] == 'obj',
                              res_manifest.json()['derivatives']))
    if len(derivative) > 1:
        raise Exception('multiple obj derivatives in ' + filename + ":\n" + str(views))

    derivativeurn = list(filter( lambda child: child['urn'][-3:] == 'obj',
                                 derivative[0]['children']))[0]['urn']

    downloadDerivative( filename[:-4]+'.obj', urn, derivativeurn, token )

f = "RVA1_2_IMP_Walls-Bldg1.dwg"

token = getToken( '53XEB4aEvB0EAJXFOVASHXgfRRsMYasV', '9mRP4vuMbxpnngzB' )

urn = uploadFile( f, BUCKET_KEY, token )
# res = createBucket(BUCKET_KEY, token)

r = getObj( f, BUCKET_KEY, urn, token )
#r = clearManifest(urn, token)
