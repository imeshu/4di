import numpy as np
import time

def generate_text_file(length=1e6, ncols=20):
    data = np.random.random((length, ncols))
    np.savetxt('large_text_file.csv', data, delimiter=',')

def iter_loadtxt(filename, delimiter=',', skiprows=0, dtype=float):
    def iter_func():
        with open(filename, 'r') as infile:
            i = 0
            for _ in range(skiprows):
                next(infile)
                i += 1
            for line in infile:
                if i%1e6 == 0:
                    print(i)
                line = line.rstrip().split(delimiter)
                for item in line:
                    yield dtype(item) 
                i += 1
        iter_loadtxt.rowlength = len(line)

    data = np.fromiter(iter_func(), dtype=dtype)
    data = data.reshape((-1, iter_loadtxt.rowlength))
    return data


print("stackOverflow guy:")
start = time.time()
data1 = iter_loadtxt('points.csv', skiprows=1, delimiter=',')
end = time.time()
print("runtime: " + str(end-start))

print("default loadtxt:")
start = time.time()
data2 = np.loadtxt('points.csv', skiprows=1, delimiter=',')
end = time.time()
print("runtime: " + str(end-start))

print(data1[:3])
print(data2[:3])

print(data1[:-3])
print(data2[:-3])

print(data1.shape())
print(data2.shape())
