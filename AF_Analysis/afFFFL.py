from AF_Analysis import afRANSAC as afr
from converter import unitConvert
from scipy.ndimage import filters
from matplotlib import collections  as mc
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import arrayfire as af
import math
import os.path
import datetime

'''
LINE format:
{
    "start":(x0,y0),
    "end":(x1,y1)
}

REGION format:
{
    "origin":(x0,y0),
    "wh":(w,h),
    "angle":theta
}
'''

WITHIN_SPEC = (0,0,0,1)
BELOW_SPEC = (1,.75,0,1)
BELOW_MIN = (.75,0,0,1)

LOGOPATH = "static_files/public/images/4Dicon.png"
if os.path.isfile(LOGOPATH):
    LOGO4DI = plt.imread(LOGOPATH)
else:
    LOGO4DI = plt.imread("../" + LOGOPATH)

class flreport:

    def __init__(self, plnHt, plnThr, errThr, sections, img, bounds, units, design={}, filename="", spacing=3,
    specff=0, specfl=0, minff=0, minfl=0, user=""):

        self.img = img
        self.bounds = bounds
        self.filename = filename
        self.specff = specff
        self.specfl = specfl
        self.minff = minff
        self.minfl = minfl
        self.design = design
        self.user = user
        self.plnHt = plnHt
        self.plnThr = plnThr
        self.errThr = errThr

        self.flsections = [ flsection(
            section, img, bounds, units, design=design,
            filename=filename, spacing=spacing,
            specff=specff, specfl=specfl,
            minff=minff, minfl=minfl) for section in sections ]
        '''
        self.ff = self.getff()
        self.fl = self.getfl()
        self.ffCI = getCI(self.ff, sum([s.getTotalLen() for s in self.flsections]))
        self.flCI = getCI(self.fl, sum([s.getZCount() for s in self.flsections]))
        '''

    # ff and fl for a total report are currently unusable. To calculate these
    # numbers each section must be weighted by its area and there is no area
    # available for a section defined only by lines
    def getff(self):
        return sum([s.getTotalLen()*s.ff for s in self.flsections])/sum([s.getTotalLen() for s in self.flsections])
    def getfl(self):
        return sum([s.getZCount()*s.fl for s in self.flsections])/sum([s.getZCount() for s in self.flsections])
            
    def plotSections(self, pdf):
        
        fig = Figure()
        self.plotHeader(fig, pageNo=pdf.get_pagecount())
        ax = fig.add_axes([.07,.05,.9,.8])

        sectionName = "Section Name:\n\n\n"
        ffs = "Flatness:\n\n\n"
        qreadings = "q Readings:\n\n\n"
        exceedminFF = "Percent below\nminimum ff:\n\n"
        fls = "Levelness:\n\n\n"
        zreadings = "z Readings:\n\n\n"
        exceedminFL = "Percent below\nminimum fl:\n\n"

        i = 0
        for section in self.flsections:
            i += 1
            sectionName += "Section " + str(i) + "\n"
            ffs += str(round(section.ff,2)) + " <" + str(round(section.ffCI[0], 2)) + ", " + str(round(section.ffCI[1], 2)) + ">\n"
            qreadings += str(section.getTotalLen()) + "\n"
            fls += str(round(section.fl,2)) + " <" + str(round(section.flCI[0], 2)) + ", " + str(round(section.flCI[1], 2)) + ">\n"
            zreadings += str(section.getZCount()) + "\n"
            if len(section.flsamples) > 0:
                exceedminFF += str(round(section.exceedMinFFCount()/len(section.flsamples)*100, 2)) + "%\n"
                exceedminFL += str(round(section.exceedMinFLCount()/len(section.flsamples)*100, 2)) + "%\n"
            else:
                exceedminFF += "No samples\n"
                exceedminFL += "No samples\n"

        ax.axis("off")
        fs = 6
        height = .95
        ax.text(0, height, sectionName, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.13, height, ffs, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.32, height, qreadings, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.42, height, exceedminFF, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.55, height, fls, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.73, height, zreadings, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.85, height, exceedminFL, horizontalalignment='left', verticalalignment='top', fontsize=fs)

        ax.text(
            0, 1,
            "Minimum Floor Flatness: {0:>5}\nSpecified Floor Flatness: {1:>5}\nPlane Height: {2:>5}".format(
            self.minff, self.specff, round(self.plnHt, 4)),
            fontsize=fs)
        ax.text(
            .3, 1,
            "Minimum Floor Levelness: {0:>5}\nSpecified Floor Levelness: {1:>5}\nPlane Threshold: {2:>5}".format(
            self.minfl, self.specfl, round(self.plnThr, 4)),
            fontsize=fs)
        now = datetime.datetime.now()
        ax.text(
            .6, 1,
            "Created: " + str(now.strftime("%Y-%m-%d %H:%M")) + (" UTC\nby: " + self.user if self.user else "") + ("\nError Threshold: {0:>5}".format(
            round(self.errThr, 4))),
            fontsize=fs)
        pdf.savefig(fig)

        i = 0
        for section in self.flsections:
            i += 1
            section.plotLines(pdf, "Section " + str(i))
            section.plotSamples(pdf, "Section " + str(i))
            section.plotNumbers(pdf, "Section " + str(i))
        
        labels = ['Within Specification', 'Below Specification', 'Below Minimum']
        colors = [WITHIN_SPEC, BELOW_SPEC, BELOW_MIN]
        fig = plt.figure()
        fig_legend = plt.figure(figsize=(2, 1.25))
        ax = fig.add_subplot(111)
        lines = ax.plot(range(2), range(2), range(2), range(2), range(2), range(2), range(2), range(2))
        fig_legend.legend(lines, labels, loc='center', frameon=False)
        plt.show()
        #pdf.savefig(fig_legend)

#    def plotHeader(self, fig, pageNo=-1, extra=""):
 #       ax = fig.add_axes([0,.9,.1,.1])
  #      ax.axis("off")
   #     ax.imshow(LOGO4DI, interpolation="bilinear")
    #    ax.text(70, 30, self.filename + "    " + extra, fontsize = 8)
     #   ax.text(70, 45, "teeeeeeeeeeeeeesssssttttt", fontsize = 8)
      #  if pageNo >= 0:
       #     ax.text(850, 45, pageNo+1, horizontalalignment='right')
        

    def plotHeader(self, fig, pageNo=-1, extra=""):
        ax = fig.add_axes([0,.9,.1,.1])
        ax.axis("off")
        ax.imshow(LOGO4DI, interpolation="bilinear")
        strn = self.filename + "    " + extra
        if len(strn) > 86:
            ax.text(70, 30, strn[:86], fontsize = 8)
            ax.text(70, 47, strn[86:], fontsize = 8)
        elif len(strn) > 70:
            ax.text(70, 30, strn, fontsize = 8)
        else:
            ax.text(70, 45, strn, fontsize = 10)
        if pageNo >= 0:
            ax.text(850, 45, pageNo+1, horizontalalignment='right')

class flsection:
    
    def __init__(self, section, img, bounds, units, design={}, filename="", spacing=3,
    specff=0, specfl=0, minff=0, minfl=0 ):

        self.lines = []
        self.img = np.copy(img)
        self.bounds = bounds.copy()
        self.filename = filename
        self.specff = specff
        self.specfl = specfl
        self.minff = minff
        self.minfl = minfl
        self.design = design

        convertBounds(self.bounds, section, self.img, units)

        if("origin" in section):
            assert "wh" in section
            if not "angle" in section:
                section["angle"] = 0
            else:
                section["angle"] *= math.pi / 180
            self.lines = filter_lines(self.img, self.bounds, linify_region(section, spacing))
            self.type = "region"
        else:
            assert "start" in section[0] and "end" in section[0]
            self.lines = filter_lines(self.img, self.bounds, section)
            self.type = "lines"

        self.flsamples = get_samples(self.img, self.bounds, self.lines)

        self.ff = 0
        self.fl = 0
        self.ffCI = (0,0)
        self.flCI = (0,0)
        if len(self.flsamples) != 0:
            self.ff = self.combinenf([sample.ff for sample in self.flsamples], [len(sample.sample) for sample in self.flsamples])
            self.fl = self.combinenf([sample.fl for sample in self.flsamples], [len(sample.sample) for sample in self.flsamples])
            self.ffCI = getCI(self.ff, self.getTotalLen())
            self.flCI = getCI(self.fl, self.getZCount())

    def combine2f(self, f1, f2, r1, r2):
        return f1 * f2 * math.sqrt((r1 + r2)/(r2*f1**2 + r1*f2**2))

    def combinenf(self, fls, rs):
        flprev = fls[0]
        rprev = rs[0]
        for i in range(1,len(fls)):
            flprev = self.combine2f(flprev, fls[i], rprev, rs[i])
            rprev += rs[i]
        return flprev

    def getTotalLen(self):
        return sum([len(fls.sample) for fls in self.flsamples])

    def getZCount(self):
        return sum([len(fls.sample)-10 for fls in self.flsamples])

    def exceedMinFFCount(self):
        count = 0
        for s in self.flsamples:
            if s.ff < self.minff:
                count += 1
        return count

    def exceedMinFLCount(self):
        count = 0
        for s in self.flsamples:
            if s.fl < self.minfl:
                count += 1
        return count

    def plotLines(self, pdf, extra=""):

        if len(self.flsamples) == 0:
            return

        fig = Figure()
        self.plotHeader(fig, pdf.get_pagecount(), extra)
        ax = fig.add_axes([.1,.1, .9, .8])
        ax.set_xlabel("Feet")
        ax.set_ylabel("Feet")

        img_plotted = ax.imshow(
            self.img,
            vmin=self.bounds["minz"],
            vmax=self.bounds["maxz"],
            extent=(self.bounds["minx"], self.bounds["maxx"], self.bounds["miny"], self.bounds["maxy"])
        )
        if self.design:
            #print([ [line["start"],line["end"]] for line in self.design["LINES"] ][0])
            #sys.stdout.flush()
            ax.add_collection(mc.LineCollection(
                [ [line["start"][:2], line["end"][:2]] for line in self.design["LINES"] ],
                colors=[(.5,.5,.5)]*len(self.design["LINES"])))

        fig.colorbar(img_plotted).set_label("Inches")

        for sample in self.flsamples:
            ax.arrow(
                    sample.start[0], sample.start[1],
                    sample.end[0]-sample.start[0], sample.end[1]-sample.start[1],
                    color=sample.getColor(self.minff, self.minfl, self.specff, self.specfl),
                    head_starts_at_zero=False, head_width=.4, head_length=.6)
            ax.text(
                    sample.start[0], sample.start[1], sample.name, fontsize=3,
                    horizontalalignment="right" if sample.start[0]<sample.end[0] else "left",
                    verticalalignment="top" if sample.start[1]<sample.end[1] else "bottom")

        self.plotLegend(ax)

        pdf.savefig(fig)

    def plotSamples(self, pdf, extra=""):
        
        if len(self.flsamples) == 0:
            return

        fig1 = Figure()
        self.plotHeader(fig1, pdf.get_pagecount(), "south to north " + extra)
        ax1 = fig1.add_axes([.1,.1, .8, .8])
        ax1.set_xlabel("Feet")
        ax1.set_ylabel("Inches")
        fig2 = Figure()
        self.plotHeader(fig2, pdf.get_pagecount()+1, "north to south " + extra)
        ax2 = fig2.add_axes([.1,.1, .8, .8])
        ax2.set_xlabel("Feet")
        ax2.set_ylabel("Inches")

        for sample in self.flsamples:
            ax = ax1 if sample.sampley[-1] > sample.sampley[0] else ax2
            ax.plot(sample.sample, label="heck", c=sample.getColor(self.minff, self.minfl, self.specff, self.specfl))
            ax.text(0, sample.sample[0], sample.name + " ", horizontalalignment="right", verticalalignment="center", fontsize=6)

        self.plotLegend(ax1)
        self.plotLegend(ax2)

        pdf.savefig(fig1)
        pdf.savefig(fig2)

    def fill_rows(self, ax, index, rows, offset, spacing=.033):
        fs = 6
        args = {
            'horizontalalignment':'left',
            'verticalalignment':'top',
            'fontsize':fs
        }
        for i, sample in enumerate(self.flsamples[index:index+rows]):
            yy = 1 - spacing*i - offset
            ax.text(.0, yy, sample.name + "\n", **args)
            ax.text(.13, yy, "({0:.6},{1:.6})".format(*sample.start), **args)
            ax.text(.32, yy, "({0:.6},{1:.6})".format(*sample.end), **args)

            bgc = (0,0,0,0)
            if sample.ff < self.minff:
                bgc = BELOW_MIN
            elif sample.ff < self.specff:
                bgc = BELOW_SPEC
            ax.text(.5, yy, "{0:.3} <{1:.4},{2:.4}>".format(sample.ff, *sample.ffCI), backgroundcolor=bgc, **args)

            bgc = (0,0,0,0)
            if sample.fl < self.minfl:
                bgc = BELOW_MIN
            elif sample.fl < self.specfl:
                bgc = BELOW_SPEC
            ax.text(.7, yy, "{0:.3} <{1:.4},{2:.4}>".format(sample.fl, *sample.flCI), backgroundcolor=bgc, **args)

            ax.text(.9, yy, str(len(sample.sample)), **args)

    def plotNumbers(self, pdf, extra=""):
        
        if len(self.flsamples) == 0:
            return

        fig = Figure()
        self.plotHeader(fig, pdf.get_pagecount(), extra=extra)
        ax = fig.add_axes([.1,.1, .8, .8])

        textName = "\n\n\nLine Name:\n"
        textStart = "\n\nStart\nCoordinates (x,y):\n"
        textEnd = "\n\nEnd\nCoordinates (x,y):\n"
        textff = "Total Flatness:\n" + str(round(self.ff, 2)) + " <"
        textff += str(round(self.ffCI[0], 2)) + ", " + str(round(self.ffCI[1], 2)) + ">\n\n"
        textff += "Per Line Flatness:\n"
        textfl = "Total Levelness:\n" + str(round(self.fl, 2)) + " <"
        textfl += str(round(self.flCI[0], 2)) + ", " + str(round(self.flCI[1], 2)) + ">\n\n"
        textfl += "Per Line Levelness:\n"
        textCount = "Total Readings:\n" + str(self.getTotalLen()) + "\n\nPer Line Readings:\n"

        ax.axis("off")
        fs = 6
        ax.text(.0, 1, textName, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.13, 1, textStart, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.32, 1, textEnd, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.5, 1, textff, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.7, 1, textfl, horizontalalignment='left', verticalalignment='top', fontsize=fs)
        ax.text(.9, 1, textCount, horizontalalignment='left', verticalalignment='top', fontsize=fs)

        FIRSTPAGESAMPLES = 27
        self.fill_rows(ax, 0, FIRSTPAGESAMPLES, .13)

        pdf.savefig(fig)

        SAMPLESPERPAGE = 30
        for i in range(FIRSTPAGESAMPLES,len(self.flsamples),SAMPLESPERPAGE):
            figof = Figure()
            self.plotHeader(figof, pdf.get_pagecount())
            axof = figof.add_axes([.1, .1, .8, .8])
            axof.axis("off")
            self.fill_rows(axof, i, SAMPLESPERPAGE, 0)
            pdf.savefig(figof)
                
#    def plotHeader(self, fig, pageNo=-1, extra=""):
#        ax = fig.add_axes([0,.9,.1,.1])
#        ax.axis("off")
#        ax.imshow(LOGO4DI, interpolation="bilinear")
#        ax.text(70, 45, self.filename + "    " + extra)
#        if pageNo > 0:
#            ax.text(850, 45, pageNo+1, horizontalalignment='right')


    def plotHeader(self, fig, pageNo=-1, extra=""):
        ax = fig.add_axes([0,.9,.1,.1])
        ax.axis("off")
        ax.imshow(LOGO4DI, interpolation="bilinear")
        str = self.filename + "    " + extra
        if len(str) > 86:
            ax.text(70, 30, str[:86], fontsize = 8)
            ax.text(70, 47, str[86:], fontsize = 8)
        elif len(str) > 70:
            ax.text(70, 30, str, fontsize = 8)
        else:
            ax.text(70, 45, str, fontsize = 10)
        if pageNo >= 0:
            ax.text(850, 45, pageNo+1, horizontalalignment='right')


    def plotLegend(self, ax):
        ax.legend(
            handles=[ mlines.Line2D([],[], color=c, label=l) for l, c in {
                "Within Specification":WITHIN_SPEC,
                "Below Specification":BELOW_SPEC,
                "Below Minimum":BELOW_MIN}.items()
            ],
            fontsize=6
        )
        #pdf.savefig(ax.legend) 


class flsample:
    def __init__(self, _sample, name="", _start=None, _end=None, _samplex=None, _sampley=None):

        self.samplex = _samplex
        self.sampley = _sampley
        self.start = _start
        self.end = _end
        self.name = name

        self.sample = _sample

        if len(self.sample) >= 11:
            self.ff = self.getff(self.sample)
            self.fl = self.getfl(self.sample)
            self.ffCI = getCI(self.ff, len(self.sample))
            self.flCI = getCI(self.fl, len(self.sample) - 10)
            
    def getff(self, h):
        q = []
        for i in range(2,len(h)):
            q.append(h[i] - 2*h[i-1] + h[i-2])
        qj = float(np.mean(q))
        sqj = float(np.std(q))
        return 4.57 / (3*sqj + abs(qj))
    
    def getfl(self, h):
        z = []
        for i in range(10,len(h)):
            z.append(h[i] - h[i-10])
        zj = float(np.mean(z))
        szj = float(np.std(z))
        return 12.5 / (3*szj + abs(zj))

    def getColor(self, minff, minfl, specff, specfl):
        c = WITHIN_SPEC
        if self.ff < minff or self.fl < minfl:
            c = BELOW_MIN
        elif self.ff < specff or self.fl < specfl:
            c = BELOW_SPEC
        return c

def getCI(f, n):
    CI90 = -1.82*(math.log10(n))**3 + 19.4*(math.log10(n))**2 - 71.69*math.log10(n) + 92.62
    return (100 - CI90)*f/100, (100+CI90)*f/100

def to_pixel(x, y, b):
    return int((x-b["minx"])/(b["maxx"]-b["minx"])*b["sizex"]), int((y-b["miny"])/(b["maxy"]-b["miny"])*b["sizey"])

def get_samples(img, bounds, lines):

    samples = []

    lineID = 1
    for line in lines:

        start = line["start"]
        end = line["end"]
        mx, my = normFromLine(start, end)
        length = math.sqrt((start[1]-end[1])**2+(start[0]-end[0])**2)
        
        dist = 0
        sample = []
        samplex = []
        sampley = []

        while dist <= length:
            coords = (start[0]+dist*mx, start[1]+dist*my)
            if coords[0] < bounds["minx"] or coords[1] < bounds["miny"] or coords[0] >= bounds["maxx"] or coords[1] >= bounds["maxy"]:
                sample.append(np.nan)
            else:
                sample.append(img[
                    -int((coords[1]-bounds["miny"])/(bounds["maxy"]-bounds["miny"])*bounds["sizey"]),
                    int((coords[0]-bounds["minx"])/(bounds["maxx"]-bounds["minx"])*bounds["sizex"])
                ])
            samplex.append(start[0]+dist*mx)
            sampley.append(start[1]+dist*my)
            dist += 1
        
        samples.append(flsample(sample, "L"+str(lineID), start, end, samplex, sampley))
        lineID += 1
    
    return samples

def filter_lines(img, bounds, lines):

    img_gauss = filters.gaussian_filter(img, .125+.25*math.ceil((bounds["maxx"] - bounds["minx"])/bounds["sizex"]*2))

    newlines = []

    for sample in get_samples(img_gauss, bounds, lines):
        start = 0
        inLine = not math.isnan(sample.sample[0])

        for i, h in enumerate(sample.sample):
            if math.isnan(h) and inLine:
                inLine = False
                if i - 1 - start >= 11:
                    newlines.append({
                        "start": (sample.samplex[start], sample.sampley[start]),
                        "end": (sample.samplex[i-1], sample.sampley[i-1])
                    })
            if not math.isnan(h) and not inLine:
                inLine = True
                start = i

        if inLine and len(sample.sample) - 1 - start >= 11:
            newlines.append({
                "start": (sample.samplex[start], sample.sampley[start]),
                "end": (sample.samplex[-1], sample.sampley[-1])
            })


    #return [line for line in newlines if (line["end"][0] - line["start"][0])**2 + (line["end"][1] - line["start"][1])**2 > 121]
    return newlines

def linify_region(region, spacing):
    nwlines = diagify_region(region, spacing)

    newRegion = {}
    newRegion["origin"] = (
        region["origin"][0] + region["wh"][0] * math.cos(region["angle"]),
        region["origin"][1] + region["wh"][0] * math.sin(region["angle"])
    )
    newRegion["wh"] = (region["wh"][1], region["wh"][0])
    newRegion["angle"] = region["angle"]+math.pi/2
    nelines = diagify_region(newRegion, spacing)

    return nelines + nwlines

def diagify_region(region, spacing):

    lines = []

    # convert spacing between lines to spacing between intersections
    spacing = math.sqrt(.5)*spacing
    mx = math.cos(region["angle"])
    my = math.sin(region["angle"])
    abswh = (abs(region["wh"][0]), abs(region["wh"][1]))
    total_dist = spacing
    
    while total_dist < sum(abswh):
        line = {}

        # starting points for all lines go along the bottom edge, then the right edge
        if total_dist < abswh[0]:
            line["end"] = (
                region["origin"][0] + total_dist * mx,
                region["origin"][1] + total_dist * my
            )
        else:
            line["end"] = (
                region["origin"][0] + region["wh"][0] * mx - (total_dist - region["wh"][0]) * my,
                region["origin"][1] + region["wh"][0] * my + (total_dist - region["wh"][0]) * mx
            )

        # ending points for all lines go along the left edge, then the top edge
        if total_dist < abswh[1]:
            line["start"] = (
                region["origin"][0] - total_dist * my,
                region["origin"][1] + total_dist * mx
            )
        else:
            line["start"] = (
                region["origin"][0] - region["wh"][1] * my + (total_dist - region["wh"][1]) * mx,
                region["origin"][1] + region["wh"][1] * mx + (total_dist - region["wh"][1]) * my
            )
        
        total_dist += spacing
        lines.append(line)
    return lines

def convertBounds(bounds, section, img, units):

    img -= (unitConvert(bounds["maxz"], 'm', units) + unitConvert(bounds["minz"], 'm', units))/2
    img *= 39.37008

    plane_height = (bounds["maxz"] + bounds["minz"])/2
    for key in bounds:
        if key == "maxz" or key == "minz":
            bounds[key] = unitConvert(bounds[key] - plane_height, 'in', units)
        elif "max" in key or "min" in key:
            bounds[key] = unitConvert(bounds[key], 'ft', units)

    if "wh" in section:
        for key in section:
            if key != "angle":
                section[key] = [ unitConvert(x, 'ft', units) for x in section[key] ]
    else:
        for line in section:
            for key in line:
                #for x in line[key]:
                line[key] = [ unitConvert(x, 'ft', units) for x in line[key] ]
                    #print("Python: " + str(x))

def normFromLine(start, end):
    mx = 0
    my = 0
    if end[1]-start[1] == 0:
        mx = math.copysign(1, end[0]-start[0])
    elif end[0]-start[0] == 0:
        my = math.copysign(1, end[1]-start[1])
    else:
        slope = (end[1]-start[1])/(end[0]-start[0])
        my = math.copysign(slope/math.sqrt(1+slope*slope), (end[1]-start[1]))
        slope = 1/slope
        mx = math.copysign(slope/math.sqrt(1+slope*slope), end[0]-start[0])
    return mx, my


