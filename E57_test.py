import subprocess
import numpy as np
from AF_Analysis import Histogram as hst
import time
import boto3
import botocore


s3 = boto3.resource('s3')
bucket = s3.Bucket('4diuploads')
ups3 = boto3.client('s3')

fname = "../../D4A1980113ARE_3.e57" 
googleid = "117916171233309199726"

start = time.time()
subprocess.call(["./a.out", fname, fname + ".npy"])
print("generated pts file in " + str(time.time()-start) + " seconds")
pts = np.load(fname + ".npy")
print(hst.get_bounds(pts))


send2s3 = input("send to s3? (y/n)")


if send2s3 == 'y':
    s3.meta.client.upload_file(
        fname + ".npy",
        '4diuploads',
        str(googleid) + "/pcl/" + fname + ".npy")

